package com.softech.ccconsultant

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.softech.ccconsultant.adapters.DocumentAdapter
import com.softech.ccconsultant.apiconnections.ApiConnection
import com.softech.ccconsultant.apiconnections.RetrofitApi
import com.softech.ccconsultant.data.models.models.DocumentFolder.Response
import com.softech.ccconsultant.data.models.models.GeneralResponse
import com.softech.ccconsultant.fragments.FragmentMyProfile
import com.softech.ccconsultant.interfaces.IWebListener
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.utils.AESEncryption
import com.softech.ccconsultant.utils.Const
import com.softech.ccconsultant.utils.FileChooser
import com.softech.ccconsultant.utils.Logs
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_upload_document.*
import org.json.JSONException
import org.json.JSONObject
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import retrofit2.Call
import retrofit2.Callback
import java.io.*
import java.util.HashMap
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody


class UploadDocumentActivity : AppCompatActivity() {

    private val TAG = "UploadDocumentActivity"

    internal var document: MutableList<Response>? = null
    internal var documentAdapter: DocumentAdapter? = null
    internal var REQUEST_IMAGE_CAPTURE = 0x1001

    internal var outputFile: File? = null
    private var pDialog: ProgressDialog? = null
    var mimetype: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_document)

        recyclerviewDoc.layoutManager = LinearLayoutManager(this)
        recyclerviewDoc.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerviewDoc.setHasFixedSize(true)
        callDocumentService()
        pDialog = ProgressDialog(this)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)

        selectFileBtn?.setOnClickListener()
        {
            selectDocumentType()

        }
        uploadFileBtn?.setOnClickListener()
        {

            uploadDocumentService()
        }
    }

    private fun uploadDocumentService() {
        if (!pDialog?.isShowing!!) {
            pDialog?.show()
        }
        val consultantID = FetchData.getData(this, "userId")
        val access_token = FetchData.getData(this, "access_token")
        val patientID = FetchData.getData(this, "patientID")
        val folderID = FetchData.getData(this, "folderID")
        val params = HashMap<String, String>()
        params["action"] = "Document Upload"
        params["consultantId"] = consultantID
        params["access_token"] = access_token
        params["patientId"] = patientID
        params["folderId"] = folderID
        params["fileName"] = outputFile!!.name
        params["fileType"] = mimetype
        Log.e(TAG, "Encrypted Object Parameters: $params")

        val jsonObject = JSONObject(params as Map<String, String>)
        val encryptString = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)
        Log.e(TAG, "Encrypted Object: $encryptString")

        try {

            val requestBodyEncryptString: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), encryptString)
            val requestBodyFile: MultipartBody.Part
            val requestFile: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), outputFile!!)
            requestBodyFile = MultipartBody.Part.createFormData("File", outputFile!!.name, requestFile)

            RetrofitApi.getService(Const.BASE_URL)
                .uploadDocument(requestBodyEncryptString, requestBodyFile)
                .enqueue(object : Callback<GeneralResponse?> {
                    override fun onResponse(
                        call: Call<GeneralResponse?>,
                        response: retrofit2.Response<GeneralResponse?>
                    ) {
                        if (pDialog!!.isShowing) {
                            pDialog!!.dismiss()
                        }
                        Log.e(TAG, "onResponse: ")
                        if (response.isSuccessful && response.body() != null) {
                            if (response.body()!!.meta.code == 200) {
                                Toast.makeText(this@UploadDocumentActivity, "File uploaded...", Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }

                    override fun onFailure(call: Call<GeneralResponse?>, t: Throwable) {
                        if (pDialog!!.isShowing) {
                            pDialog!!.dismiss()
                        }
                        Log.e(TAG, "onFailure: " + t.message)
                    }
                })
        }catch (ex:Exception){

        }



//        Ion.with(this).load(Const.BASE_URL + Const.UPLOAD_DOCUMENT)
//            .setMultipartParameter("encryptObj", encryptString)
//            .setMultipartFile("File", mimetype, outputFile)
//            .asString()
//            .setCallback(FutureCallback<String> { e, result ->
//                try {
//                    if (pDialog?.isShowing()!!) {
//                        pDialog?.dismiss()
//                    }
//                    if(result!=null){
//                        val jsonObject = JSONObject(result)
//                        val meta = jsonObject.getJSONObject("meta")
//                        val code = meta.getString("code")
//                        if (Integer.valueOf(code) == 200) {
//                            val message = meta.getString("message")
//                            Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
//                            this.finish()
//                        } else if (Integer.valueOf(code) == 404) {
//                            val message = jsonObject.getString("error")
//                            Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
//                        //  recyclViewSearch.setVisibility(View.GONE);
//
//                        }
//                    }
//                    else{
//                        Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show();
//                    }
//
//                } catch (e1: JSONException) {
//                    e1.printStackTrace()
//                }
//            })



    }

    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "application/pdf"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        val i = Intent.createChooser(intent, "File")
        startActivityForResult(i, REQUEST_IMAGE_CAPTURE)
    }

    private fun selectFile() {
        if (FetchData.CheckPermission("Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
            showFileChooser()

        }
    }

    private fun selectDocumentType() {
        val items = arrayOf<CharSequence>( "Gallery", "PDF Document")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Select Document Type")
        builder.setItems(items) { dialog, item ->
//            if (items[item] == "Select Camera") {
//                if (FetchData.CheckPermission("Camera", Manifest.permission.CAMERA, this, 100)) {
//                    EasyImage.openCameraForImage(this, 100)
//                }
//            }
             if (items[item] == "Gallery") {
                if (FetchData.CheckPermission( "Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
                    EasyImage.openGallery(this, 101)
                }
            }
             else if(items[item]=="PDF Document")
             {
                 if (FetchData.CheckPermission( "Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
                    selectFile()
                 }

             }
             else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            val selectedUri = data?.getData()
            if (selectedUri !== null) {
                val path = FileChooser.getFilePath(this@UploadDocumentActivity, selectedUri)
                val inputfile = File(path!!)
                outputFile = File(path)
               // AESEncryption.encryptFile(1, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputFile)
                outputFile!!.exists()
                mimetype = "application/pdf"
            }
        }
        else
        {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
                override fun onImagesPicked(list: List<File>, imageSource: EasyImage.ImageSource, i: Int) {

                    val inputfile = list[0]
                    var compressedImageFile: File? = null

                    try {
                        compressedImageFile = Compressor(this@UploadDocumentActivity).compressToFile(inputfile)
                        val file_size = Integer.parseInt((compressedImageFile!!.length() / 1024).toString())
                         //  Toast.makeText(this@UploadDocumentActivity, ""+file_size, Toast.LENGTH_SHORT).show();
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    if (compressedImageFile!!.length()<400000) {
                        outputFile = compressedImageFile
                        Log.d("sdasdasd", outputFile!!.length().toString())
                        val inputfile = list[0]
                       // AESEncryption.encryptFile(1, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputFile)
                        outputFile!!.exists()
                      //  Toast.makeText(this@UploadDocumentActivity, outputFile?.name + " selected", Toast.LENGTH_LONG).show()
                        mimetype = FetchData.getMimeType(outputFile!!.path)
                    }
                    else
                    {
                        Toast.makeText(this@UploadDocumentActivity,"File size is greater than 4 mb", Toast.LENGTH_LONG).show()
                    }
                }
            })

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (FetchData.CheckPermission("Storage", Manifest.permission.READ_EXTERNAL_STORAGE, this, 101)) {
                    selectFile()
                }

            }
        }

    }

    fun callDocumentService() {
        val consultantID = FetchData.getData(this, "userId")
        val access_token = FetchData.getData(this, "access_token")
        val patientID = FetchData.getData(this, "patientID")
        val params = HashMap<String, String>()
        params["action"] = "Load Folders"
        params["consultantId"] = consultantID
        params["access_token"] = access_token
        params["patientId"] = patientID
        val jsonObject = JSONObject(params as Map<String, String>)
        val finalParams = HashMap<String, String>()
        try {
            val encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)

            finalParams["encryptObj"] = encrypt
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                if (response != null) {

                    try {

                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val message = meta.getString("message")
                            val decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV)
                            document = Gson().fromJson(decryptedString, Array<Response>::class.java).toMutableList()
                            //                    linearLayout1.setVisibility(View.VISIBLE);
                            documentAdapter = DocumentAdapter(this@UploadDocumentActivity, document, true)
                            val mLayoutManager = LinearLayoutManager(this@UploadDocumentActivity)
                            recyclerviewDoc.setLayoutManager(mLayoutManager)
                            recyclerviewDoc.setItemAnimator(DefaultItemAnimator())
                            recyclerviewDoc.setAdapter(documentAdapter)
                            // mAdapter.notifyDataSetChanged();
                            recyclerviewDoc.addItemDecoration(DividerItemDecoration(this@UploadDocumentActivity, DividerItemDecoration.VERTICAL))

                        } else if (Integer.valueOf(code) == 504) {
                            document?.clear()
                            val message = meta.getString("message")

                            Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
                            //  recyclViewSearch.setVisibility(View.GONE);

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String) {
                Log.v("Error Add Appointment", "")

            }
        }, this, finalParams,
                Const.Documents)
        apiConnection.makeStringReq()
    }

}
