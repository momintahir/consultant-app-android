package com.softech.ccconsultant;

import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.adapters.CalendarAdapter;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.models.CalendarByDate.CalendarByDate;
import com.softech.ccconsultant.data.models.models.CalendarByDate.DayInterval;
import com.softech.ccconsultant.data.models.models.CalendarByDate.Response;
import com.softech.ccconsultant.data.models.models.CalendarByDate.SlotsDatum;
import com.softech.ccconsultant.data.models.models.CliniclistModel;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.vo.DateData;

public class DateSelectionActivity extends AppCompatActivity implements IWebListener {
    @BindView(R.id.calenderview)
    MCalendarView calandarView;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.spinnerClinicList)
    Spinner spinnerClinicList;
    String currentDate, selectedDate, hospitalId;
    int date, month, year;
    CalendarByDate calendarByDate;
    Date startDate, ourEndDate, endDate, currentDated;
    Map<String, List<SlotsDatum>> hashMap = new HashMap<String, List<SlotsDatum>>();
    List<SlotsDatum> slotsData, arrangedSlotsData;
    List newArray = new ArrayList();
    List<Date> dates = new ArrayList<Date>();
    List<Date> firsttwentyDates = new ArrayList<>();
    List<Response> responseArrayList = new ArrayList<Response>();
    ArrayList<Date> eventsArray = new ArrayList<Date>();
    CalendarAdapter calendarAdapter;
    int spinnerPosition = 0;
    ArrayList<String> clinics = new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_selection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Add New");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateSelectionActivity.this.finish();
            }
        });

        ButterKnife.bind(this, this);
        getCurrentDate(Calendar.getInstance().getTime());

        calendarAdapter = new CalendarAdapter(DateSelectionActivity.this, newArray, selectedDate, hospitalId);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DateSelectionActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(calendarAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(DateSelectionActivity.this, DividerItemDecoration.VERTICAL));

        callingDaysSlotService();
        calandarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                newArray.clear();
                selectedDate = String.format("%s/%s/%d", date.getDayString(), date.getMonthString(), date.getYear());
                Log.d("check values" + date.getDayString(), date.getMonthString());
                if (hashMap.get(selectedDate) != null) {
                    slotsData = hashMap.get(selectedDate);
                    for (int i = 0; i < slotsData.size(); i += 3) {
                        int j = i + 3;
                        int Size = slotsData.size() - i;
                        if (Size < 3)
                            j = i + Size;
                        arrangedSlotsData = slotsData.subList(i, j);
                        newArray.add(arrangedSlotsData);
                        calendarAdapter.notifyDataSetChanged();
                        calendarAdapter = new CalendarAdapter(DateSelectionActivity.this, newArray, selectedDate, hospitalId);
                        recyclerView.setAdapter(calendarAdapter);

                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    Log.d("newArray", newArray.toString());
                } else {
                    recyclerView.setVisibility(View.GONE);
                }
            }
        });
        calandarView.setOnMonthChangeListener(new OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month) {
                recyclerView.setVisibility(View.GONE);
                Calendar cal = Calendar.getInstance();
                int mon = cal.get(Calendar.MONTH) + 1;
                if (mon == month) {
                    Log.d("Month", "is Current");
                    getCurrentDate(cal.getTime());
                } else {
                    cal.set(year, month - 1, 1);
                    Log.d("Month", "is not Current");
                    Log.d("custom date", cal.getTime().toString());
                    getCurrentDate(cal.getTime());
                }
                callingDaysSlotService();
            }
        });

        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, clinics);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerClinicList.setAdapter(adapter);


    }

    public void getCurrentDate(Date date) {
        System.out.println("Current time => " + date);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = df.format(date);
        currentDated = dateFromString(currentDate);

    }

    public void callingDaysSlotService() {

        String consultantID = FetchData.getData(this, "userId");
        String accesstoken = FetchData.getData(this, "access_token");
        String ClinicList = FetchData.getData(this, "cliniclist");

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
        }.getType();

        final ArrayList<CliniclistModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);
        String clinicID=connectedClinicsModelArrayList.get(0).getOID();
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Consultant Calendars");
        params.put("consultantId", consultantID);
        params.put("date", currentDate);
        params.put("clinicid", clinicID);
        params.put("access_token",accesstoken);
        ApiConnection apiConnection = new ApiConnection(this,
                this, params, Const.ConsultantCalendars);
        apiConnection.makeStringReq();
    }

    public Date dateFromString(String string) {
        Date mDate = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mDate = format.parse(string);
            System.out.println(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }


    public void setDotsOnEvents() {
        for (int i = 0; i < eventsArray.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(eventsArray.get(i));
            date = calendar.get(Calendar.DATE);
            month = calendar.get(Calendar.MONTH) + 1;
            year = calendar.get(Calendar.YEAR);
            calandarView.markDate(
                    new DateData(year, month, date).setMarkStyle(new MarkStyle(MarkStyle.DOT, Color.RED)
                    ));
        }
    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
                    spinnerClinicList.setEnabled(true);

                    eventsArray.clear();
                    dates.clear();
                    hashMap.clear();
                    String message = meta.getString("message");
                    calendarByDate = new Gson().fromJson(response, CalendarByDate.class);
//                    responseArrayList.add(calendarByDate.getResponse().get(0));

                    spinnerClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            SlotsData(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    populdateSpinner();
                } else if (Integer.valueOf(code) == 504) {
                    String message = meta.getString("message");

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                } else if (Integer.valueOf(code) == 499) {
                    String message = meta.getString("message");
                    spinnerClinicList.setEnabled(false);
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            recyclerView.setVisibility(View.GONE);
            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }

    private void populdateSpinner() {
        clinics.clear();
        responseArrayList = calendarByDate.getResponse();
        for (int i = 0; i < responseArrayList.size(); i++) {
            clinics.add(responseArrayList.get(i).getClinicName());
        }

        adapter.notifyDataSetChanged();
    }

    private void SlotsData(int position) {


        responseArrayList.add(calendarByDate.getResponse().get(position));
        hospitalId = responseArrayList.get(position).getHospitalId();

        for (int i = 0; i < calendarByDate.getResponse().size(); i++) {
            startDate = dateFromString(calendarByDate.getResponse().get(i).getStartDate());
            endDate = dateFromString(calendarByDate.getResponse().get(i).getEndDate());
            if (currentDated.after(startDate)) {
                startDate = currentDated;


            } else {
            }
            Calendar cac = Calendar.getInstance();
            cac.setTime(currentDated);
            cac.add(Calendar.DAY_OF_MONTH, 20);
            ourEndDate = cac.getTime();
            if (ourEndDate.after(endDate)) {

            } else {
                endDate = ourEndDate;

            }
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startDate);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(endDate);

            while (!cal1.after(cal2)) {
                dates.add(cal1.getTime());
                cal1.add(Calendar.DATE, 1);
            }
            Log.d("Dates Array", dates.toString());

            for (DayInterval dayinterval : calendarByDate.getResponse().get(position).getDayIntervals()
                    ) {
                for (int j = 0; j < dates.size(); j++) {
                    if (dates.get(j).toString().contains(dayinterval.getVDay())) {
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                        String eventkeyDate = df.format(dates.get(j));
                        if (dayinterval.getSlotsData().size() > 0) {
                            hashMap.put(eventkeyDate, dayinterval.getSlotsData());
                            eventsArray.add(dates.get(j));
                        }


                    }
                }
                setDotsOnEvents();

            }
        }

    }
}

