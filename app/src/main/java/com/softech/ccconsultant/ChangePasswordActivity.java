package com.softech.ccconsultant;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends AppCompatActivity implements IWebListener {

    private final String TAG = "ChangePasswordActivity";

    @BindView(R.id.editTextCurrentPassword)
    EditText editTextCurrentPassword;
    @BindView(R.id.editTextNewPassword)
    EditText editTextNewPassword;
    @BindView(R.id.editTextConfirmPassword)
    EditText editTextConfirmPassword;

    @BindView(R.id.buttonChangePassword)
    Button buttonChangePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordActivity.this.finish();
            }
        });

        ButterKnife.bind(this);


        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editTextCurrentPassword.getText().toString().length()>0 &&
                        editTextNewPassword.getText().toString().length()>0 &&
                        editTextConfirmPassword.getText().toString().length()>0 ) {
                    if(editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())){

                        if (editTextNewPassword.getText().toString().length()>6 &&
                                editTextConfirmPassword.getText().toString().length()>6)
                        {
                            changePassword();

                        }
                        else {
                            Toast.makeText(ChangePasswordActivity.this, "Please enter at least 7 characters password", Toast.LENGTH_SHORT).show();

                        }

                    }
                    else{
                        Toast.makeText(ChangePasswordActivity.this, "New Password Doesn't match.", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, "Please fill all of the fields", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    private void changePassword() {

        String consultantID = FetchData.getData(ChangePasswordActivity.this, "userId");
        String access_token = FetchData.getData(ChangePasswordActivity.this, "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Consultant Password");
        params.put("consultantId", consultantID);
        params.put("token", access_token);
        params.put("newPassword", editTextNewPassword.getText().toString());


        ApiConnection apiConnection = new ApiConnection(ChangePasswordActivity.this, ChangePasswordActivity.this, params,
                Const.CHANGEPASSWORD);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response) {
        if(response !=null) {

            try {
                Log.e(TAG, "Change Password Response: "+response );
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.parseInt(code)== 200)
                {
                    Toast.makeText(this, "You have changed your password.", Toast.LENGTH_SHORT).show();
                    ChangePasswordActivity.this.finish();


                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {
        Toast.makeText(this, "You have changed your password.", Toast.LENGTH_SHORT).show();

    }
}
