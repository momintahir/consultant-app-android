package com.softech.ccconsultant;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.softech.ccconsultant.sharedpreferences.FetchData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.textViewWelcome)
    TextView textViewWelcome;

    String IsFirstLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("SETUP YOUR CLINIC");

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MainActivity.this.finish();
//            }
//        });

        ButterKnife.bind(this);

        String Name = FetchData.getData(MainActivity.this,"Name");
        textViewWelcome.setText("Hi, " + Name);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            IsFirstLog = intent.getString("IsFirstLog", "");

        }

//        if(IsFirstLog !=null)
//        {
//        if (IsFirstLog.equals("0"))
//        {
//            button1.setVisibility(View.VISIBLE);
//        }}

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, AddNewClinicActivity.class));


            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, ClinicListActivity.class));

            }
        });

    }
}
