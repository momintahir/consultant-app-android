package com.softech.ccconsultant;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.adapters.DocumentAdapter;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.models.DocumentFolder.Response;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DocumentActivity extends AppCompatActivity implements IWebListener {

    private final String TAG = "DocumentActivity";

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.calendarBtn)
    ImageView calendarBtn;

    @BindView(R.id.toolbar)
    androidx.appcompat.widget.Toolbar toolbar;

    FragmentManager fragmentManager;
    List<Response> documentList;
    DocumentAdapter documentAdapter;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ButterKnife.bind(this, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        callDocumentService();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Documents");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocumentActivity.this.finish();
            }
        });
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DocumentActivity.this, UploadDocumentActivity.class));
            }
        });

    }

    public void callDocumentService() {
        String consultantID = FetchData.getData(this, "userId");
        String access_token = FetchData.getData(this, "access_token");
        String patientID = FetchData.getData(this, "patientID");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Load Folders");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId", patientID);

        Log.e(TAG, "Actual Params: "+ params);
        JSONObject jsonObject = new JSONObject(params);
        try {
            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
            Log.e(TAG, "Encrypted String: "+ encrypt);

            Map<String, String> finalParams = new HashMap<String, String>();
            finalParams.put("encryptObj", encrypt);
            ApiConnection apiConnection = new ApiConnection(this, this, finalParams, Const.Documents);
            apiConnection.makeStringReq();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {

                Log.e(TAG, "Load Folder Response: "+response );
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.parseInt(code) == 200) {
                    String decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV);

                    String message = meta.getString("message");
                    documentList = new Gson().fromJson(decryptedString, new TypeToken<List<Response>>(){}.getType());

//                    linearLayout1.setVisibility(View.VISIBLE);
                    documentAdapter = new DocumentAdapter(this, documentList, false);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(documentAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

                } else if (Integer.valueOf(code) == 504) {
                    documentList.clear();
                    String message = meta.getString("message");

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }
}
