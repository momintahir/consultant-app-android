package com.softech.ccconsultant.utils

object Util {

    fun checkDigit(number: Int): String {
        return if (number <= 9) "0$number" else number.toString()
    }

}