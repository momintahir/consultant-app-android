package com.softech.ccconsultant.utils;

import com.softech.ccconsultant.BuildConfig;

public class Const {

    //public static final String BASE_URL = "https://www.healthcarepakistan.pk/ClinicConnectServiceStage/";
   //  public static final String BASE_URL = "https://www.healthcarepakistan.pk/ClinicConnectService/";
  // public static final String BASE_URL = "https://www.healthcarepakistan.pk/ClinicConnectServiceStageSecure/";
    public static final String BASE_URL = BuildConfig.HOST;


    //Done
    public static final String LOGIN = "SignIn/SignInConsultantMobile";
    public static final String GETSPECIALITIES = "SignUp/GetSpecialities";
    public static final String GETLABANDPHARMACYLIST = "consultant/GetLabAndPharmacyListForConsultantProfile";
    public static final String VIEWAPPOINTMENTS = "Consultant/ViewAppointments";
    public static final String SEARCHPatientByClinic = "Consultant/PatientsListByClinicId";
    public static final String VIEWSCHEDULE = "Consultant/ViewConsultantSchedule";
    public static final String LOGOUT = "SignIn/SignOutMobile";
    public static final String FORGOTPASSWORD = "SignUp/ForgetPasswordRequest";
    public static final String CHANGEPASSWORD = "SignIn/ChangeConsultantPassword";
    public static final String GETPROFILe = "Consultant/GetConsultantProfile";
    public static final String UPDATEPROFILE = "Consultant/UpdateConsultantProfile";
    public static final String UPDATEPROFILEIMAGE = "Consultant/UpdateConsultantProfilePicture";
    public static final String GET_ONLINE_STATUS = "Consultant/GetOnlineStatus";
    public static final String Documents = "Consultant/LoadFolders";
    public static final String GetConsultantPreferences = "Consultant/GetConsultantPreferences";
    public static final String SEARCH = "Consultant/SearchPatientsOnly";
    public static final String SelectedDocuments = "Consultant/LoadSelectedFolder";
    public static final String ConsultantCalendars = "Consultant/GetCurrentDateCalendarSlots";
    public static final String UPDATE_SCHEDULE = "Consultant/UpdateConsultantSchedule";
    public static final String UPDATETOKEN = "Consultant/UpdateDeviceId";
    public static final String ACTION_GETPATIENTPROFILE = "Consultant/GetPatientProfileForConsultant";
    public static final String UPLOAD_DOCUMENT = "Consultant/UploadDocument";
    public static final String UPLOAD_PRESCRIPTION = "Consultant/UploadPrescriptionTagLab";
    public static final String ACTION_BOOK_APPOINTMENT = "Appointment/AddAppointment";
    public static final String ACTION_DELETE_APPOINTMENT = "Consultant/DeleteAppointment";
    public static final String SIGNUP = "SignUp/ConsultantSignUp";
    public static final String SETUPPROFILE = "Consultant/SetupConsultantProfile";


    //Pending

    public static final String ADDNEWCLINIC = "SignUp/AddClinic";
    public static final String CLINCSLIST = "SignUp/ViewClinics";
    public static final String CLINICREQUEST = "Consultant/ConnectClinicRequest";
    public static final String VIEWCALENDER = "Consultant/ViewConsultantCalendar";
    public static final String ADDCALANDER = "Consultant/ConsultantCalendar";
    public static final String SELECT_CLINIC = "Consultant/SelectClinic";

    //Unused
    public static final String VIEWAPPOINTMENTDETAIL = "Consultant/ViewConsultantCalendar";
    public static final String VIEW_CLINICS = "Consultant/SelectClinic";
    public static final String SEARCH_PATIENT = "Consultant/SearchPatient";


    public static final String Encryption_Key = "S*FTECHCL!N!CC*NNECTCRYPT*GRAPHY";
    public static final String Encryption_IV = "1234567890123456";



}
