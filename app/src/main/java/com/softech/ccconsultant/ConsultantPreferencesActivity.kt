package com.softech.ccconsultant

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.*
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.softech.ccconsultant.adapters.PrescriptionAdapter
import com.softech.ccconsultant.apiconnections.ApiConnection
import com.softech.ccconsultant.data.models.ConsultantPreference
import com.softech.ccconsultant.interfaces.IWebListener
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.utils.Const
import com.softech.ccconsultant.utils.FileChooser
import com.softech.ccconsultant.utils.Logs
import kotlinx.android.synthetic.main.activity_consultant_preferences.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import android.net.Uri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.zelory.compressor.Compressor
import java.io.IOException
import java.util.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage

class ConsultantPreferencesActivity : AppCompatActivity(), PrescriptionAdapter.OnPrescriptionClickListener {

    private val TAG = "ConsultantPreferencesAc"

    internal var REQUEST_IMAGE_CAPTURE = 0x1001
    internal var outputFile: File? = null
    var mimetype: String = ""
    var labId: String = "0"
    var pharmId: String = "0"
    var radId: String = "0"
    var isCamera: Boolean = false
    var filePath: String = ""
    internal var prescList: MutableList<ConsultantPreference>? = null
    var prescriptionAdapter: PrescriptionAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consultant_preferences)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Upload Prescription"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        if (intent.extras != null) {
            isCamera = intent.getBooleanExtra("isCamera", false)
        }
        toolbar.setNavigationOnClickListener { this@ConsultantPreferencesActivity.finish() }
        selectFile(isCamera)

        nextBtn.setOnClickListener()
        {
            if (labId != "" || radId != "" || pharmId != "") {
                val intent = Intent(this, BookAppointmentActivity::class.java)
                intent.putExtra("labId", labId)
                intent.putExtra("pharId", pharmId)
                intent.putExtra("radId", radId)
                intent.putExtra("filePath", filePath)
                startActivity(intent)
            } else
                Toast.makeText(this, "Select Preference First.", Toast.LENGTH_SHORT).show()
        }
        getMyConsultantPreferenceList()
    }

    @SuppressLint("CheckResult")
    private fun showCamera() {
//        RxImagePicker.with(getFragmentManager()).requestImage(Sources.CAMERA).subscribe(object : Consumer<Uri> {
//            @Throws(Exception::class)
//            override fun accept(@NonNull uri: Uri) {
//                Log.d("fileUri", uri.toString())
//                filePath = FileChooser.getFilePath(this@ConsultantPreferencesActivity, uri)
//
//                //   if (Const.imagePickerListener != null) run { Const.imagePickerListener.onImagePic(uri) }
//            }
//        })

    }

    override fun OnPrescriptionClick(prescriptionId: Int, position: Int) {
        when (position) {
            0 -> {
                pharmId = if (pharmId == "0") {
                    prescriptionId.toString()
                } else {
                    "0"
                }
            }
            1 -> {
                labId = if (labId == "0") {
                    prescriptionId.toString()
                } else {
                    "0"
                }
            }
            else -> {
                radId = if (radId == "0")
                    prescriptionId.toString()
                else {
                    "0"
                }
            }
        }

    }

    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        val i = Intent.createChooser(intent, "File")
        startActivityForResult(i, REQUEST_IMAGE_CAPTURE)
    }

    private fun selectFile(isCamera: Boolean) {
        if (!isCamera) {
            if (FetchData.CheckPermission("Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
                Log.e(TAG, "Opening Gallery" )
                EasyImage.openGallery(this, 101)
            }
        } else {
            if (FetchData.CheckPermission("Camera", Manifest.permission.CAMERA, this, 102)) {
                Log.e(TAG, "Opening Camera" )
                EasyImage.openCameraForImage(this, 102)
            }
        }
    }

    private fun getMyConsultantPreferenceList() {
        val params = HashMap<String, String>()
        val consultantID = FetchData.getData(this, "userId")
        params["action"] = "Get Consultant Preferences"
        params["ConID"] = consultantID
        // params["patientId"] = patientID
        // params["patientId"] = patientID
        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                if (response != null) {

                    try {

                        Log.e(TAG, "GetConsultantPreferences Response: $response")
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val message = meta.getString("message")
                            // val decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV)
                            //      document=Gson().fromJson(decryptedString, Array<Response>::class.java).toMutableList()
                            //                    linearLayout1.setVisibility(View.VISIBLE);
                            val responseString = jsonObject.getString("response")
                            prescList = Gson().fromJson(responseString, Array<ConsultantPreference>::class.java).toMutableList()
                            prescriptionAdapter = PrescriptionAdapter(prescList, this@ConsultantPreferencesActivity, this@ConsultantPreferencesActivity)
                            preferences_recyclerView?.layoutManager = LinearLayoutManager(this@ConsultantPreferencesActivity, RecyclerView.VERTICAL, false)
                            preferences_recyclerView?.adapter = prescriptionAdapter

                        } else if (Integer.valueOf(code) == 504||Integer.valueOf(code) == 499) {
                            prescList?.clear()
                            val message = meta.getString("message")
                            Toast.makeText(this@ConsultantPreferencesActivity, message, Toast.LENGTH_LONG).show()
                            //  recyclViewSearch.setVisibility(View.GONE);

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }
            override fun error(response: String) {
                Log.v("Error Add Appointment", "")
            }
        }, this, params, Const.GetConsultantPreferences)
        apiConnection.makeStringReq()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagesPicked(list: List<File>, imageSource: EasyImage.ImageSource, i: Int) {
                Log.e(TAG, "onImagesPicked: " )

                val inputfile = list[0]
                var compressedImageFile: File? = null

                try {
                    compressedImageFile = Compressor(this@ConsultantPreferencesActivity).compressToFile(inputfile)
                    val file_size = Integer.parseInt((compressedImageFile!!.length() / 1024).toString())
                  //   Toast.makeText(this@ConsultantPreferencesActivity, ""+file_size, Toast.LENGTH_SHORT).show();
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                if (compressedImageFile!!.length()<400000) {
                    outputFile = compressedImageFile

                    filePath = outputFile!!.path
                    //  AESEncryption.encryptFile(1, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputFile)
                    outputFile!!.exists()
                  //  Toast.makeText(this@ConsultantPreferencesActivity, outputFile?.name + " selected", Toast.LENGTH_LONG).show()
                    mimetype = FetchData.getMimeType(outputFile!!.path)
                } else {
                    Toast.makeText(this@ConsultantPreferencesActivity,"File size is greater than 4 mb", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        })
//        if (requestCode == REQUEST_IMAGE_CAPTURE) {
//            val selectedUri = data?.getData()
//            if (selectedUri !== null) {
//                val path = FileChooser.getFilePath(this@ConsultantPreferencesActivity, selectedUri)
//                val inputfile = File(path!!)
//                outputFile = File(path)
//              //  AESEncryption.encryptFile(1, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputFile)
//                // AESEncryption.encryptFile(2,inputfile,Const.Encryption_Key,Const.Encryption_IV,outputFile)
//                //  writeToSDFile(outputFile?.path!!)
//
//                outputFile!!.exists()
//                filePath=path
//                mimetype = FetchData.getMimeType(path)
//                getMyConsultantPreferenceList()
//            }
//        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (FetchData.CheckPermission("Storage", Manifest.permission.READ_EXTERNAL_STORAGE, this, 101)) {
                    // showFileChooser()
                    EasyImage.openGallery(this, 101)
                }
            }
        } else {
            if (FetchData.CheckPermission("Camera", Manifest.permission.CAMERA, this, 102)) {
                //showCamera()
                EasyImage.openCameraForImage(this, 102)
            }
        }

    }


}
