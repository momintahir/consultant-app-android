package com.softech.ccconsultant;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebviewwActivity extends AppCompatActivity {

    WebView webview;
    String url;
    private Dialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webvieww);

        webview=findViewById(R.id.postWebView_activity);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {

            url = intent.getString("url", "");

        }

        webview.getSettings().setJavaScriptEnabled(true); // enable javascript
        webview.getSettings().setAllowContentAccess(true);;
        webview.setNetworkAvailable(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webview.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                pDialog = new ProgressDialog(WebviewwActivity.this);
                pDialog.setTitle("Loading...");
                pDialog.setCancelable(false);
                showProgressDialog();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgressDialog();

            }
        });



        webview.loadUrl(url);
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
