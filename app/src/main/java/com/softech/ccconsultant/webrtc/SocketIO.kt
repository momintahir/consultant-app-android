package com.softech.ccconsultant.webrtc

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import com.github.nkzawa.socketio.client.Ack
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.JsonObject
import com.softech.ccconsultant.webrtc.callbacks.SocketCallback
import com.softech.ccconsultant.webrtc.callbacks.WebSocketCallback
import com.softech.ccconsultant.webrtc.callbacks.WebSocketNewCallListener
import com.softech.ccconsultant.webrtc.services.WebrtcService
import com.softech.ccconsultant.webrtc.util.CallsGlobal
import com.softech.ccconsultant.webrtc.util.Constant
import com.softech.ccconsultant.webrtc.util.D
import org.json.JSONObject
import org.webrtc.SessionDescription

@SuppressLint("StaticFieldLeak")
object SocketIO {

    private val TAG = this.javaClass.simpleName
    var mSocket: Socket? = null
    var socketCallback: SocketCallback? = null
    private var webSocketCallback: WebSocketCallback? = null
    private var webSocketNewCallListener: WebSocketNewCallListener? = null
    private var dummyWebSocketNewCallListener: WebSocketNewCallListener? = null
    private var isFromPush = false
    private var connectedUserId = ""
    private var name = ""
    private var callId = ""
    private var chatId = ""
    private var socketId = ""
    private var sessionId = ""
    private var isVideo: Boolean = false
    private var profileImage = ""

    private var isAlreadyConnected: Boolean = false
    var context: Context? = null


    fun connectSocket(userId: String, userName: String, accessToken: String, context: Context) {
        SocketIO.context = context
        val opts = IO.Options()
        opts.forceNew = true
        opts.query = "user_id=$userId&user_name=$userName&device_type=android&user_type=consultant&token=$accessToken"
//        mSocket = IO.socket("http://184.169.185.200:9090/", opts)
        mSocket = IO.socket("https://healthcarepakistan.pk:9091/", opts)
        mSocket?.connect()
        mSocket?.on(Socket.EVENT_CONNECT) {
            connectListeners()
            D.e(TAG, "==============CONNECTED==============")
            if (isFromPush) {
                if (!CallsGlobal.isMyServiceRunning(SocketIO.context!!, WebrtcService::class.java)) {
                    newCallReceived(chatId, name, callId, connectedUserId, socketId, sessionId)
                    isFromPush = false
                    startCallService()
                } else {
                    onHangout(chatId, callId)
                }
            }
        }
        D.e("Socket", "user_id=$userId")
        D.e("Socket", "user_name=$userName")
        D.e("Socket", "access_token=$accessToken")
    }

    private fun setListenersOff() {
        mSocket?.off(Constant.NEW_CALL)
        mSocket?.off(Constant.OFFER)
        mSocket?.off(Constant.CANDIDATE)
        mSocket?.off(Constant.ANSWER)
        mSocket?.off(Constant.REJECT)
        mSocket?.off(Constant.READY_FOR_CALL)
        mSocket?.off(Constant.SWITCH_VIDEO)
    }

    private fun connectListeners() {
        mSocket?.on(Socket.EVENT_DISCONNECT) {
            setListenersOff()
            D.e(TAG, "===============OFF===============")

        }?.on(Constant.NEW_CALL) {
            val json = it[0] as JSONObject
            newCallReceived(
                    json.getString("chatId"),
                    json.getString("name"),
                    json.getString("callId"),
                    json.getString("connectedUserId"),
                    json.getString("socketId"),
                    json.getString("sessionId")
            )
            webSocketNewCallListener?.newCallListener(json)

        }?.on(Constant.OFFER) {
            val json = it[0] as JSONObject
            webSocketNewCallListener?.newCallListener(json)

        }?.on(Constant.CANDIDATE) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }?.on(Constant.ANSWER) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }?.on(Constant.REJECT) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)
//            rejectCallback?.onReject(json)

        }?.on(Constant.READY_FOR_CALL) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }?.on(Constant.CALL_ACCEPTED) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)
//            rejectCallback?.onReject(json)

        }?.on(Constant.NEW_CALL_RECEIVED) {
            val json = it[0] as JSONObject
            webSocketNewCallListener?.newCallListener(json)

        }?.on(Constant.SWITCH_VIDEO) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }
    }

    private fun startCallService() {
        val intent = Intent(context, WebrtcService::class.java)
        intent.putExtra(Constant.CHAT_USER_NAME, name)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_ID, chatId)
        intent.putExtra(Constant.CALL_ID, callId)
        intent.putExtra(Constant.CONNECTED_USER_ID, connectedUserId)
        intent.putExtra(Constant.IS_FROM_PUSH, true)
        intent.putExtra(Constant.CHAT_USER_PICTURE, profileImage)
        intent.putExtra(Constant.SOCKET_ID, socketId)
        intent.putExtra(Constant.SESSION_ID, sessionId)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context?.startForegroundService(intent)
        } else {
            context?.startService(intent)
        }
    }

    fun disconnectSocket() {
        if (!CallsGlobal.isMyServiceRunning(context!!, WebrtcService::class.java)) {
            mSocket?.disconnect()
            mSocket?.off(Constant.NEW_CALL)
            mSocket?.off(Constant.OFFER)
            mSocket?.off(Constant.CANDIDATE)
            mSocket?.off(Constant.ANSWER)
            mSocket?.off(Constant.REJECT)
            mSocket?.off(Constant.READY_FOR_CALL)
            mSocket?.close()
            D.e("SocketIO", "Socket disconnectSocket")
        }
    }

    fun setSocketCallbackListener(socketCallback: SocketCallback) {
        SocketIO.socketCallback = socketCallback
    }

    // Call Functions

    fun setSocketCallback(webSocketCallback: WebSocketCallback?) {
        SocketIO.webSocketCallback = webSocketCallback
    }

    fun setOfferListener(
            webSocketNewCallListener: WebSocketNewCallListener,
            isMainOfferCallback: Boolean
    ) {
        if (isMainOfferCallback) {
            SocketIO.webSocketNewCallListener = webSocketNewCallListener
        } else {
            dummyWebSocketNewCallListener = SocketIO.webSocketNewCallListener
            SocketIO.webSocketNewCallListener = webSocketNewCallListener
        }
    }

    fun reAssignOfferListenerToMain() {
        webSocketNewCallListener = dummyWebSocketNewCallListener
    }

    fun setPushData(
            chatId: String,
            connectedUserId: String,
            isFromPush: Boolean,
            name: String,
            callId: String,
            isVideo: String?,
            isAlreadyConnected: Boolean,
            socketId: String,
            sessionId: String,
            profileImage: String
    ) {
        SocketIO.chatId = chatId
        SocketIO.connectedUserId = connectedUserId
        SocketIO.isFromPush = isFromPush
        SocketIO.name = name
        SocketIO.callId = callId
        SocketIO.isVideo = isVideo != "0"
        SocketIO.isAlreadyConnected = isAlreadyConnected
        SocketIO.socketId = socketId
        SocketIO.sessionId = sessionId
        SocketIO.profileImage = profileImage
        logE("isFromPush :$isFromPush")
    }

    fun onReadyForCall(
            chatId: String,
            name: String,
            callId: String,
            connectedUserId: String,
            socketId: String,
            sessionId: String
    ) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("type", Constant.READY_FOR_CALL)
        jsonObject.addProperty("chatId", chatId)
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("callId", callId)
        jsonObject.addProperty("connectedUserId", connectedUserId)
        jsonObject.addProperty("socketId", socketId)
        jsonObject.addProperty("sessionId", sessionId)
        logE("readyForCall sent $jsonObject")
        mSocket?.emit(Constant.READY_FOR_CALL, jsonObject)
    }


    fun sendIceCandidates(
            iceCandidate: JSONObject,
            callerID: String,
            chatId: String,
            callId: String?,
            socketId: String,
            sessionId: String
    ) {
        val jsonObject = JSONObject()
        jsonObject.put("type", Constant.CANDIDATE)
        jsonObject.put("candidate", iceCandidate)
        jsonObject.put("connectedUserId", callerID)
        jsonObject.put("chatId", chatId)
        jsonObject.put("callId", callId)
        jsonObject.put("socketId", socketId)
        jsonObject.put("sessionId", sessionId)
        logE("send IceCandidate $jsonObject")
        mSocket?.emit(Constant.CANDIDATE, jsonObject.toString())
    }


    fun createOffer(
            sessionDescription: SessionDescription,
            offer: JSONObject,
            callerID: String,
            isVideo: Boolean?,
            name: String,
            callId: String,
            chatId: String,
            socketId: String,
            sessionId: String
    ) {
        val obj = JSONObject()
        obj.put("type", sessionDescription.type.canonicalForm())
        obj.put("offer", offer)
        obj.put("connectedUserId", callerID)
        obj.put("isVideo", isVideo)
        obj.put("name", name)
        obj.put("callId", callId)
        obj.put("chatId", chatId)
        obj.put("socketId", socketId)
        obj.put("sessionId", sessionId)
        logE("createOffer $obj")
        mSocket?.emit(Constant.OFFER, obj.toString())
    }

    fun doAnswer(
            sessionDescription: SessionDescription,
            offer: JSONObject,
            userId: String,
            callId: String,
            chatId: String,
            socketId: String,
            sessionId: String
    ) {
        val obj = JSONObject()
        obj.put("type", sessionDescription.type.canonicalForm())
        obj.put("offer", offer)
        obj.put("connectedUserId", userId)
        obj.put("callId", callId)
        obj.put("chatId", chatId)
        obj.put("socketId", socketId)
        obj.put("sessionId", sessionId)
        logE("doAnswer :$obj")
        mSocket?.emit(Constant.ANSWER, obj.toString())
    }

    fun onHangout(chatId: String, callId: String) {
        val obj = JsonObject()
        obj.addProperty("type", Constant.REJECT)
        obj.addProperty("chatId", chatId)
        obj.addProperty("callId", callId)
        logE("OnHangout :$obj")
        mSocket?.emit(Constant.REJECT, obj)
    }

    fun didEnterBackground(id: String) {
        val obj = JsonObject()
        obj.addProperty("type", "didEnterBackground")
        obj.addProperty("userId", id)
        mSocket?.emit("didEnterBackground", obj)
    }

    fun didEnterForeground(id: String) {
        val obj = JsonObject()
        obj.addProperty("type", "didEnterForeground")
        obj.addProperty("userId", id)
        mSocket?.emit("didEnterForeground", obj)
    }


    fun checkCallStatus(chatId: String) {
        val obj = JSONObject()
        obj.put("chatId", chatId)
        logE("checkCallStatus :$obj")
        mSocket?.emit(Constant.CHECK_CALL_STATUS, obj.toString(), Ack {
            val json = it[0] as JSONObject
            logE("callProgress :$json")
            socketCallback?.socketResponse(json, Constant.CHECK_CALL_STATUS)
        })
    }

    fun addToCallSocket(chatId: String, callId: String, userId: String) {
        val obj = JSONObject()
        obj.put("chatId", chatId)
        obj.put("callId", callId)
        obj.put("userId", userId)
        logE("checkCallStatus :$obj")
        mSocket?.emit(Constant.ADD_TO_CALL, obj.toString())
    }

    fun switchToVideo(chatId: String, enableVideo: Boolean) {
        val obj = JSONObject()
        obj.put("chatId", chatId)
        obj.put("enableVideo", enableVideo)
        logE("checkCallStatus :$obj")
        mSocket?.emit(Constant.SWITCH_VIDEO, obj.toString())
    }

    fun isSocketActive(): Boolean {
        return mSocket != null
    }

    // Call New Code

    private fun newCallReceived(
            chatId: String,
            name: String,
            callId: String,
            connectedUserId: String,
            socketId: String,
            sessionId: String
    ) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("type", Constant.READY_FOR_CALL)
        jsonObject.addProperty("chatId", chatId)
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("callId", callId)
        jsonObject.addProperty("connectedUserId", connectedUserId)
        jsonObject.addProperty("socketId", socketId)
        jsonObject.addProperty("sessionId", sessionId)
        logE("newCallReceived sent $jsonObject")
        mSocket?.emit(Constant.NEW_CALL_RECEIVED, jsonObject)
    }

    fun updatePushToken(token: String) {
        val obj = JsonObject()
        obj.addProperty("push_token", token)
        logE("updatePushToken sent $obj")
        mSocket?.emit(Constant.UPDATE_PUSH_TOKEN, obj)
    }

    private fun logE(msg: String) {
        D.e(TAG, msg)
    }

}