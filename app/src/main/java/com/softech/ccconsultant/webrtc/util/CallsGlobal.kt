package com.softech.ccconsultant.webrtc.util

import android.app.ActionBar
import android.app.ActivityManager
import android.content.Context
import android.graphics.Typeface
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.softech.ccconsultant.R


object CallsGlobal {
    var currentChatID = -1
    var ringtune: Ringtone? = null
    var isOngoingCall = false
    var calleeName = ""
    var callTimer: Long = 0
    var handler: Handler? = null
    var callTimeRunnable: Runnable? = null
    public var isAppInBackground = false
    var keepSocketConnected = false

    fun setColor(context: Context, color: Int): Int {
        return ContextCompat.getColor(
                context,
                color
        )
    }

    fun startTune(context: Context) {
        D.e("startTune", "Tune is started")
        ringtune = RingtoneManager.getRingtone(
                context,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        )
        ringtune?.play()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ringtune?.isLooping = true
        }
    }

    fun stopCallTune() {
        if (ringtune?.isPlaying == true) {
            D.e("startTune", "Stopping Call Tune")
            ringtune?.stop()
            ringtune = null
        }
    }

    fun changeText(context: Context, font: Int): Typeface {
        var typeface: Typeface = Typeface.DEFAULT
        when (font) {
            0 -> {
                typeface = Typeface.createFromAsset(context.assets, "fonts/amiri_regular.ttf")
            }
            1 -> {
                typeface = Typeface.createFromAsset(context.assets, "fonts/roboto_regular.ttf")
            }
            2 -> {
                typeface = Typeface.createFromAsset(context.assets, "fonts/roboto_medium.ttf")
            }
        }
        return typeface
    }

    fun expand(v: View, speed: Int) {
        val matchParentMeasureSpec: Int = View.MeasureSpec.makeMeasureSpec(
                (v.parent as View).width,
                View.MeasureSpec.EXACTLY
        )
        val wrapContentMeasureSpec: Int =
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight: Int = v.measuredHeight
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation?
            ) {
                v.layoutParams.height =
                        if (interpolatedTime == 1f) ActionBar.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        // Expansion speed of 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density * speed).toLong()
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation
            ) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                            initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        // Collapse speed of 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density * 6).toLong()
        v.startAnimation(a)
    }

    fun showKeyBoard(context: Context, editText: EditText) {
        val imm =
                context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        setFocusCursor(editText)
    }

    private fun setFocusCursor(editText: EditText) {
        editText.isFocusable = true
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
    }

    fun getNotificationIcon(): Int {
        val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        return if (useWhiteIcon) R.mipmap.ic_launcher else R.mipmap.ic_launcher
    }

    fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}