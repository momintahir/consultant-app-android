package com.softech.ccconsultant.webrtc.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.ToneGenerator
import android.media.projection.MediaProjection
import android.os.*
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.github.nkzawa.socketio.client.Ack
import com.google.gson.JsonObject
import com.softech.ccconsultant.R
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.webrtc.CallActivity
import com.softech.ccconsultant.webrtc.CustomPeerConnectionObserver
import com.softech.ccconsultant.webrtc.CustomSdpObserver
import com.softech.ccconsultant.webrtc.SocketIO
import com.softech.ccconsultant.webrtc.callbacks.WebSocketCallback
import com.softech.ccconsultant.webrtc.callbacks.WebSocketNewCallListener
import com.softech.ccconsultant.webrtc.model.*
import com.softech.ccconsultant.webrtc.util.CallsGlobal
import com.softech.ccconsultant.webrtc.util.Constant
import com.softech.ccconsultant.webrtc.util.D
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import org.webrtc.*
import java.util.*
import kotlin.collections.ArrayList

class WebrtcService : Service(), WebSocketCallback, WebSocketNewCallListener {

    private val TAG = this.javaClass.simpleName
    private var videoCapture: VideoCapturer? = null
    private var videoSource: VideoSource? = null
    private var peerConnectionFactory: PeerConnectionFactory? = null
    private var localVideoTrack: VideoTrack? = null
    private var localScreenVideoTrack: VideoTrack? = null
    private var rootEglBase: EglBase? = null
    private var mMediaProjectionPermissionResultData: Intent? = null
    private var mMediaProjectionPermissionResultCode = 0
    private var peerIceServers: MutableList<PeerConnection.IceServer> = ArrayList()
    private var audioConstraints: MediaConstraints? = null
    private var audioSource: AudioSource? = null
    private var localAudioTrack: AudioTrack? = null
    private var handler: Handler? = null
    private var callTimeRunnable: Runnable? = null
    private var localPeerArray: ArrayList<PeerConnection?>? = ArrayList()
    private var peerMediaList: ArrayList<PeerMedia> = ArrayList()
    private var toneGenerator: ToneGenerator? = null
    private var dialTuneHandler: Handler? = null
    private var dialtuneRunnable: Runnable? = null
    private lateinit var audioManager: AudioManager
    private var isFromPush: Boolean? = false
    lateinit var peerData: PeerData
    private var isFromReconnecting = false
    private var isCallConnected = false
    private var isTimerStarted = false

    //Data to Handle
    var chatId: String? = null
    private var connectedUserId: String? = null
    private var callId: String? = null
    private var callImage: String? = null
    private var initiator: Boolean? = false
    private var isFirstCall: Boolean? = false
    private var calleeName: String? = null
    private var isVideo: Boolean? = false
    private val notificationId = (System.currentTimeMillis() / 1000L).toInt()
    private var isMuted = false
    private var isSpeakerEnabled = false
    private var isNewCallReceived = false
    var timeWhenCallStarted = 0L
    var socketId: String? = null
    var sessionId: String? = null

    var intent: Intent? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        EventBus.getDefault().register(this)
        this.intent = intent
        initiator = intent?.getBooleanExtra(Constant.INITIATOR, false)
        calleeName = intent?.getStringExtra(Constant.CHAT_USER_NAME)
        isVideo = intent?.getBooleanExtra(Constant.IS_VIDEO_CALL, false)
        chatId = intent?.getStringExtra(Constant.CHAT_ID)
        callImage = intent?.getStringExtra(Constant.CHAT_USER_PICTURE)
        isFirstCall = intent?.getBooleanExtra(Constant.IS_CALL_ACCEPTED, false)
        connectedUserId = intent?.getStringExtra(Constant.CONNECTED_USER_ID)
        callId = intent?.getStringExtra(Constant.CALL_ID)
        isFromPush = intent?.getBooleanExtra(Constant.IS_FROM_PUSH, false)
        socketId = intent?.getStringExtra(Constant.SOCKET_ID)
        sessionId = intent?.getStringExtra(Constant.SESSION_ID)
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        SocketIO.setSocketCallback(this)
        SocketIO.setOfferListener(this, false)
        if (initiator == true) {
            startCallingActivity()
            showOutgoingNotification()
        } else {
            logE("IsFrom Push :$isFromPush")
            if (CallsGlobal.ringtune == null)
                CallsGlobal.startTune(this)
            if (isFromPush == false) {
                showIncomingNotification()
            } else {
                showIncomingNotification()
            }
        }
        return START_STICKY
    }

    private fun startCallingActivity() {
        val intent = Intent(this, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Constant.CHAT_USER_NAME, calleeName)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_USER_PICTURE, callImage)
        intent.putExtra(Constant.INITIATOR, initiator)
        intent.putExtra(Constant.IS_CALL_ACCEPTED, isFirstCall)
        startActivity(intent)
    }

    fun init() {
        rootEglBase = EglBase.create()
        CallsGlobal.keepSocketConnected = true
        Handler().postDelayed(
            { EventBus.getDefault().post(EglBaseModel(rootEglBase)) },
            500
        )
    }

    private fun initVideoCapturer(isScreenCapturer: Boolean) {
        if (videoCapture != null) {
            videoCapture?.stopCapture()
            videoCapture = null
        }

        videoCapture = if (isScreenCapturer) {
            createScreenCapturer()
        } else {
            if (Camera2Enumerator.isSupported(this)) {
                createCameraCapturer(Camera2Enumerator(this))
            } else {
                createCameraCapturer(Camera1Enumerator(false))
            }
        }
        videoCapture?.let {
            val surfaceTextureHelper =
                SurfaceTextureHelper.create("CaptureThread", rootEglBase?.eglBaseContext)
            videoSource =
                peerConnectionFactory?.createVideoSource(it.isScreencast)
            it.initialize(
                surfaceTextureHelper,
                applicationContext,
                videoSource?.capturerObserver
            )
        }

        if (isScreenCapturer) {
            logE("screen capturer track has been initialized")
            localScreenVideoTrack = peerConnectionFactory?.createVideoTrack("100", videoSource)
        } else {
            logE("Local camera track has been initialized")
            localVideoTrack = peerConnectionFactory?.createVideoTrack("105", videoSource)
        }
        videoCapture?.startCapture(1280, 720, 22)
    }

    // Needs to be test out

    private fun createScreenCapturer(): VideoCapturer? {
        if (mMediaProjectionPermissionResultCode != Activity.RESULT_OK) {
            logE("User didn't give permission to capture the screen.")
            return null
        }
        return ScreenCapturerAndroid(
            mMediaProjectionPermissionResultData, object : MediaProjection.Callback() {
                override fun onStop() {
                    logE("User revoked permission to capture the screen.")
                }
            })
    }

    private fun createCameraCapturer(enumerator: CameraEnumerator): VideoCapturer? {
        val deviceNames = enumerator.deviceNames
        for (deviceName in deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                val videoCapturer: VideoCapturer? = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }

        for (deviceName in deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                val videoCapturer = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }
        return null
    }

    // Starting Webrtc

    private fun startWebrtc(intent: Intent) {
        init()
        val stunIceServer = PeerConnection.IceServer
            .builder("stun:stun.worldnoordev.com:3478")
            .createIceServer()
        peerIceServers.add(stunIceServer)
        val turnIceServer =
            PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:3478?transport=udp")
                .setUsername("softech")
                .setPassword("Kalaam2020")
                .createIceServer()
        peerIceServers.add(turnIceServer)

        val turnIceServer2 =
            PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:3478?transport=tcp")
                .setUsername("softech")
                .setPassword("Kalaam2020")
                .createIceServer()
        peerIceServers.add(turnIceServer2)

        val turnIceServer3 =
            PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:443?transport=tcp")
                .setUsername("softech")
                .setPassword("Kalaam2020")
                .createIceServer()
        peerIceServers.add(turnIceServer3)

        val initializationOptions = PeerConnectionFactory.InitializationOptions.builder(this)
            .createInitializationOptions()
        PeerConnectionFactory.initialize(initializationOptions)

        val options = PeerConnectionFactory.Options()
        val defaultVideoEncoderFactory = DefaultVideoEncoderFactory(
            rootEglBase?.eglBaseContext,
            true,
            true
        )
        val defaultVideoDecoderFactory = DefaultVideoDecoderFactory(rootEglBase?.eglBaseContext)
        peerConnectionFactory = PeerConnectionFactory.builder()
            .setOptions(options)
            .setVideoEncoderFactory(defaultVideoEncoderFactory)
            .setVideoDecoderFactory(defaultVideoDecoderFactory)
            .createPeerConnectionFactory()

        audioConstraints = MediaConstraints()
        initVideoCapturer(false)

        audioSource = peerConnectionFactory?.createAudioSource(audioConstraints)
        localAudioTrack = peerConnectionFactory?.createAudioTrack("101", audioSource)
        handler = Handler()
        callTimeRunnable = Runnable {
            logE("callTimeRunnable called")
            SocketIO.onHangout(chatId.toString(), callId.toString())
            logE("Hangup called 1")
            hangup()
        }
        handler?.postDelayed(callTimeRunnable!!, 60 * 1000)
        if (initiator == true) {
            addMyView()
            val obj = JsonObject()
            obj.addProperty("type", Constant.NEW_CALL)
            obj.addProperty("chatId", "")
            obj.addProperty("receiverId", chatId)
            obj.addProperty("isVideo", isVideo)
            logE("onNewCall sent $obj")
            SocketIO.mSocket?.emit(Constant.NEW_CALL, obj, Ack {
                val json = it[0] as JSONObject
                logE("new call ack :$json")
                chatId = json.getString("chatId")
                if (json.getBoolean("callStarted")) {
                    isFirstCall = false
                    callId = json.getString("callId")
                    logE("call id is $callId")
                } else {
                    isFirstCall = true
                }
            })
        } else {
            addMyView()
            logE("call accepted :$isFirstCall")
            callId = intent.getStringExtra(Constant.CALL_ID)
        }
    }

    private fun addMyView() {
        val itemList = peerMediaList.filter { it.user.id == FetchData.getData(this, "userId").toInt() }
        if (itemList.isEmpty()) {
            logE("Adding My View")
            localPeerArray?.add(null)
            val member =
                GroupMembers(
                    FetchData.getData(this, "firstName"),
                    FetchData.getData(this, "lastName"),
                    FetchData.getData(this, "userId").toInt(),
                    FetchData.getData(this, "profileImage"),
                    ""
                )
            val track: VideoTrack? = localVideoTrack
            peerMediaList.add(
                PeerMedia(
                    member,
                    Constant.CALLING,
                    isVideo,
                    track,
                    false
                )
            )
//            EventBus.getDefault().post(PeerMediaEvent(peerMediaList, -1))
        } else {
            logE("My View is already present")
        }
    }

    private fun newCallReceived(data: JSONObject, isFirstCall: Boolean) {
        logE("newCallReceived :$data")
        val offer = JSONObject(data.getString("offer"))
        val userId = data.getString(Constant.CONNECTED_USER_ID)
        val mSessionId = data.getString("sessionId")
        if (isFirstCall) {
            calleeName = data.getString("name")
            chatId = data.getString("chatId")
            callId = data.getString("callId")
        }
        val user = GroupMembers(
            data.getString("name"),
            "",
            userId.toInt(),
            data.getString("photoUrl"),
            mSessionId
        )
        val peerMedia = PeerMedia(
            user,
            Constant.CONNECTING,
            data.getBoolean("isVideo"),
            null,
            false
        )
        logE("UserId in readyforcall: $userId")

        val item = peerMediaList.filter { it.user.sessionId == mSessionId }
        logE("Users id is matched :$item")

        if (item.isEmpty()) {
            peerMediaList.add(peerMedia)
            localPeerArray?.add(null)
            EventBus.getDefault().post(PeerMediaEvent(peerMediaList, -1))
            consumeNewCall(peerMediaList.size - 1, userId, data, offer)
        } else {
            val index = peerMediaList.indexOf(item[0])
            if (localPeerArray?.get(index)
                    ?.iceConnectionState() == PeerConnection.IceConnectionState.FAILED || localPeerArray?.get(
                    index
                )
                    ?.iceConnectionState() == PeerConnection.IceConnectionState.CLOSED || localPeerArray?.get(
                    index
                )?.iceConnectionState() == PeerConnection.IceConnectionState.DISCONNECTED
            ) {
                localPeerArray?.get(index)?.close()
                localPeerArray?.set(index, null)
                consumeNewCall(index, userId, data, offer)
            }
            logE("localPeerArray?.get(index) != null")
        }
    }


    private fun consumeNewCall(index: Int, userId: String, data: JSONObject, offer: JSONObject) {
        peerData = PeerData(
            index,
            userId,
            data.getString("chatId"),
            data.getString("callId"),
            data.getBoolean("isVideo"),
            data.getString("sessionId"),
            data.getString("socketId")
        )
        logE("Index found in users array :$index")
        createPeerConnection(peerData)
        localPeerArray?.get(index)?.setRemoteDescription(
            CustomSdpObserver("localSetRemote$index"),
            SessionDescription(SessionDescription.Type.OFFER, offer.getString("sdp"))
        )
    }


    private fun createPeerConnection(peerData: PeerData) {
        logE("Creating peer connections")
        val rtcConfig = PeerConnection.RTCConfiguration(peerIceServers)
        rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy =
            PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        rtcConfig.keyType = PeerConnection.KeyType.ECDSA
        localPeerArray!![peerData.position] = peerConnectionFactory?.createPeerConnection(
            rtcConfig,
            object : CustomPeerConnectionObserver("localPeerCreation") {
                override fun onIceCandidate(iceCandidate: IceCandidate) {
                    super.onIceCandidate(iceCandidate)
                    val json = JSONObject()
                    json.put("sdp", iceCandidate.sdp)
                    json.put("sdpMLineIndex", iceCandidate.sdpMLineIndex)
                    json.put("sdpMid", iceCandidate.sdpMid)
                    SocketIO.sendIceCandidates(
                        json,
                        peerData.userId,
                        peerData.chatId,
                        peerData.callId,
                        peerData.socketId,
                        peerData.sessionId
                    )
                }

                override fun onAddStream(mediaStream: MediaStream) {
                    super.onAddStream(mediaStream)
                    try {
                        val item =
                            peerMediaList.single { it.user.id == peerData.userId.toInt() }
                        val index = peerMediaList.indexOf(item)
                        item.state = Constant.CONNECTING
                        if (mediaStream.videoTracks.isNotEmpty()) {
                            logE("mediaStream.videoTracks is not empty")
                            item.track = mediaStream.videoTracks[0]
                        }
                        item.isVideo = false
                        EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                    } catch (e: NoSuchElementException) {
                        logE("onAddStream such element found")
                    }
                }

                override fun onIceConnectionChange(iceConnectionState: PeerConnection.IceConnectionState) {
                    when (iceConnectionState) {
                        PeerConnection.IceConnectionState.CONNECTED -> {
                            if (isFromReconnecting) {
                                EventBus.getDefault()
                                    .post(ConnectionStatusEvent(Constant.CONNECTED, true))
                                stopDialTune()
                            } else {
                                isCallConnected = true
                                timeWhenCallStarted = SystemClock.elapsedRealtime()
                                logE("Peer Connection CONNECTED :${peerData.userId}")
                                handler?.removeCallbacks(callTimeRunnable!!)
                                if (!isTimerStarted) {
                                    isTimerStarted = true
                                    EventBus.getDefault()
                                        .post(
                                            ConnectionStatusEvent(
                                                Constant.CONNECTED,
                                                false,
                                                timeWhenCallStarted
                                            )
                                        )
                                    showCallAcceptedNotification()
                                }
                            }
                            updateStatus(Constant.CONNECTED, peerData)
                        }
                        PeerConnection.IceConnectionState.DISCONNECTED -> {
                            logE("Peer Connection DISCONNECTED")
                            updateStatus(Constant.RECONNECTING, peerData)
                            if (isCallConnected && localPeerArray?.size == 2) {
                                EventBus.getDefault()
                                    .post(ConnectionStatusEvent(Constant.RECONNECTING))
                                startReconnectingTune()
                                isFromReconnecting = true
                                handler?.postDelayed(callTimeRunnable!!, 40 * 1000)
                            }
                        }
                        PeerConnection.IceConnectionState.FAILED -> {
                            logE("Peer Connection FAILED")
                            updateStatus(Constant.FAILED, peerData)
                        }
                        PeerConnection.IceConnectionState.CHECKING -> {
                            logE("Peer Connection CHECKING")
                        }
                        PeerConnection.IceConnectionState.CLOSED -> {
                            updateStatus(Constant.CLOSED, peerData)
                            logE("Peer Connection CLOSED")
                        }
                        PeerConnection.IceConnectionState.NEW -> {
                            logE("Peer Connection NEW")
                        }
                        PeerConnection.IceConnectionState.COMPLETED -> {
                            logE("Peer Connection COMPLETED")
                        }
                    }
                }
//                }
            })
        addStreamToLocalPeer(peerData.position)
    }

    fun updateStatus(status: String, peerData: PeerData) {
        val item =
            peerMediaList.filter { it.user.id == peerData.userId.toInt() }
        logE("--------------Update Status---------------")
        logE("Item :$item")
        if (item.isNotEmpty()) {
            val index = peerMediaList.indexOf(item[0])
            item[0].state = status
            when (status) {
                Constant.CONNECTED -> {
                    logE("Item.track == ${item[0].track}")
                    item[0].isVideo = peerData.isVideo
                    EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                }

                Constant.CLOSED -> {
                    logE("Trying to disconnect :$peerData")
//                    Handler(Looper.getMainLooper()).post {
//                        closeConnection(index)
//                    }
                }
                else -> {
                    item[0].isVideo = false
                    EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                }
            }
        }
    }

    private fun closeConnection(index: Int) {
//        try {
        localPeerArray?.let {
            if (index < it.size) {
                logE("------------closeConnection------------")
                logE("localPeerArray :$localPeerArray")
                logE("position :$index")
                localPeerArray?.get(index)?.close()
                localPeerArray?.removeAt(index)
                peerMediaList.removeAt(index)
                EventBus.getDefault().post(PeerMediaEvent(peerMediaList, -1))
            }
        }
        if (peerMediaList.size == 1) {
            logE("Hangup called 2")
            hangup()
        }
    }
//    } catch (e: Exception)
//    {
//        logE("User not present at $index")
//    }

    private fun startReconnectingTune() {
        try {
            toneGenerator = ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100)
            dialTuneHandler = Handler(Looper.getMainLooper())
            dialtuneRunnable = object : Runnable {
                override fun run() {
                    toneGenerator?.startTone(
                        ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK, 500
                    )
                    dialTuneHandler?.postDelayed(this, 1000)
                }
            }
            dialTuneHandler?.post(dialtuneRunnable as Runnable)
        } catch (e: Exception) {
            logE("Exception while playing dial Tune :${e.message}")
        }
    }

    private fun addStreamToLocalPeer(pos: Int) {
        val stream = peerConnectionFactory?.createLocalMediaStream("102")
        stream?.addTrack(localAudioTrack)
        stream?.addTrack(localVideoTrack)
        localPeerArray?.get(pos)?.addStream(stream)
    }

    private fun doCall(name: String, peerData: PeerData) {
        val sdpConstraints = MediaConstraints()
        sdpConstraints.mandatory?.add(
            MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true")
        )
        sdpConstraints.mandatory?.add(
            MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true")
        )
        localPeerArray?.get(peerData.position)
            ?.createOffer(object : CustomSdpObserver("localCreateOffer") {
                override fun onCreateSuccess(sessionDescription: SessionDescription) {
                    super.onCreateSuccess(sessionDescription)
                    localPeerArray?.get(peerData.position)?.setLocalDescription(
                        CustomSdpObserver("localSetLocalDesc${peerData.position}"),
                        sessionDescription
                    )
                    val jsonObject = JSONObject()
                    jsonObject.put("sdp", sessionDescription.description)
                    SocketIO.createOffer(
                        sessionDescription,
                        jsonObject,
                        peerData.userId,
                        isVideo,
                        name,
                        peerData.callId,
                        peerData.chatId,
                        peerData.socketId,
                        peerData.sessionId
                    )
                }
            }, sdpConstraints)
    }

    private fun answerCall(peerData: PeerData) {
        EventBus.getDefault().post(CallStatus(Constant.CONNECTING))
        doAnswer(peerData)
        if (isVideo == true) {
            isSpeakerEnabled = true
            turnOnSpeakers()
        }
    }

    private fun doAnswer(peerData: PeerData) {
        val sdpConstraints = MediaConstraints()
        localPeerArray?.get(peerData.position)
            ?.createAnswer(object : CustomSdpObserver("localCreateAnswer") {
                override fun onCreateSuccess(sessionDescription: SessionDescription) {
                    super.onCreateSuccess(sessionDescription)
                    logE("doAnswer : onCreateSuccess $sessionDescription")
                    localPeerArray?.get(peerData.position)?.setLocalDescription(
                        CustomSdpObserver("localSetLocal${peerData.position}"),
                        sessionDescription
                    )
                    val json = JSONObject()
                    json.put("sdp", sessionDescription.description)
                    SocketIO.doAnswer(
                        sessionDescription,
                        json,
                        peerData.userId,
                        peerData.callId,
                        peerData.chatId,
                        peerData.socketId,
                        peerData.sessionId
                    )
                }
            }, sdpConstraints)
    }

    private fun saveIceCandidates(data: JSONObject, pos: Int) {
        try {
            logE("onIceCandidateReceived : $data")
            val candidates = JSONObject(data.getString("candidate"))
            localPeerArray?.get(pos)?.addIceCandidate(
                IceCandidate(
                    candidates.getString("sdpMid"),
                    candidates.getInt("sdpMLineIndex"),
                    candidates.getString("sdp")
                )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun onAnswerSave(data: JSONObject, pos: Int) {
        val answer = JSONObject(data.getString("offer"))
        logE("onAnswerSave : $answer")
        localPeerArray?.get(pos)?.setRemoteDescription(
            CustomSdpObserver("localSetRemote"),
            SessionDescription(
                SessionDescription.Type.fromCanonicalForm(data.getString("type").toLowerCase()),
                answer.getString("sdp")
            )
        )
        logE("answer saved successfully")
    }

    override fun webSocketCallback(jsonObject: JSONObject) {
        try {
            when (jsonObject.getString(Constant.TYPE)) {
                Constant.CANDIDATE -> {
                    logE("iceCandidates Received :$jsonObject")
                    val userId = jsonObject.getString(Constant.CONNECTED_USER_ID).toInt()
                    val item = peerMediaList.single { it.user.id == userId }
                    val index = peerMediaList.indexOf(item)
                    saveIceCandidates(jsonObject, index)
                }
                Constant.ANSWER -> {
                    stopDialTune()
                    logE("answer Received :$jsonObject")

                    val userId = jsonObject.getString(Constant.CONNECTED_USER_ID).toInt()
                    val item = peerMediaList.single { it.user.id == userId }
                    val index = peerMediaList.indexOf(item)
                    onAnswerSave(jsonObject, index)
                    EventBus.getDefault()
                        .post(CallStatus(Constant.CONNECTING))
                }
                Constant.REJECT -> {
                    logE("Reject Received in listener :$jsonObject")
                    if (chatId == jsonObject.getString("chatId")) {
                        val userId = jsonObject.getString(Constant.CONNECTED_USER_ID)
                        val callType = jsonObject.getString("callType")
                        if (callType == "single") {
                            logE("Hangup called 3")
                            Handler(Looper.getMainLooper()).post {
                                hangup()
                            }
                        } else {
                            val item = peerMediaList.filter { it.user.id == userId.toInt() }
                            if (item.isNotEmpty()) {
                                val index = peerMediaList.indexOf(item[0])
                                logE("Disconnecting :$item")
                                closeConnection(index)
                            }
                        }
                    }
                }
                Constant.READY_FOR_CALL -> {
                    val userId = jsonObject.getString(Constant.CONNECTED_USER_ID)
                    callId = jsonObject.getString("callId")
                    chatId = jsonObject.getString("chatId")
//                        socketId = jsonObject.getString("socketId")
                    val sessionId = jsonObject.getString("sessionId")
                    logE("call id is $callId")
                    logE("readyforcall received: $jsonObject")
                    val user = GroupMembers(
                        jsonObject.getString("name"),
                        "",
                        userId.toInt(),
                        jsonObject.getString("photoUrl"),
                        sessionId
                    )
                    val peerMedia = PeerMedia(
                        user,
                        Constant.CONNECTING,
                        false,
                        null,
                        false
                    )
                    val item = peerMediaList.filter { it.user.id == userId.toInt() }
                    if (item.isEmpty()) {
                        peerMediaList.add(peerMedia)
                        localPeerArray?.add(null)
                        EventBus.getDefault().post(PeerMediaEvent(peerMediaList, -1))
                        consumeReadyForCall(userId, peerMediaList.size - 1, jsonObject)
                        if (initiator == true) {
                            initiator = false
                            startDialTune()
                        }
                    } else {
                        val index = peerMediaList.indexOf(item[0])
                        if (localPeerArray?.get(index)
                                ?.iceConnectionState() == PeerConnection.IceConnectionState.FAILED
                            || localPeerArray?.get(index)
                                ?.iceConnectionState() == PeerConnection.IceConnectionState.CLOSED
                            || localPeerArray?.get(index)
                                ?.iceConnectionState() == PeerConnection.IceConnectionState.DISCONNECTED
                        ) {
                            localPeerArray?.get(index)?.close()
                            localPeerArray?.set(index, null)
                            consumeReadyForCall(userId, index, jsonObject)
                        } else
                            D.e("TestingReadyforcall", "ready for call ignored :$userId")
                    }
                }
                Constant.SWITCH_VIDEO -> {
                    val userId = jsonObject.getString("senderId")
                    val enableVideo = jsonObject.getBoolean("enableVideo")
                    val item = peerMediaList.single { it.user.id == userId.toInt() }
                    val index = peerMediaList.indexOf(item)
                    item.isVideo = enableVideo
                    EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                }
                Constant.CALL_ACCEPTED -> {
                    hangup()
                }
            }

        } catch (e: NoSuchElementException) {
            logE("no such element found :${e.message}")
        }
//        }
    }

    private fun consumeReadyForCall(userId: String, index: Int, jsonObject: JSONObject) {
        val peerData = PeerData(
            index,
            userId,
            jsonObject.getString("chatId"),
            jsonObject.getString("callId"),
            jsonObject.getBoolean("isVideo"),
            jsonObject.getString("sessionId"),
            jsonObject.getString("socketId")
        )
        createPeerConnection(peerData)
        doCall(jsonObject.getString("name"), peerData)
        EventBus.getDefault().post(CallStatus("Ringing"))
    }

    private fun startDialTune() {
        try {
            toneGenerator = ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100)
            dialTuneHandler = Handler(Looper.getMainLooper())
            dialtuneRunnable = object : Runnable {
                override fun run() {
                    toneGenerator?.startTone(
                        ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK, 1500
                    )
                    dialTuneHandler?.postDelayed(this, 4000)
                }
            }
            dialTuneHandler?.post(dialtuneRunnable as Runnable)
        } catch (e: Exception) {
            logE("Exception while playing dial Tune :${e.message}")
        }
    }

    override fun newCallListener(jsonObject: JSONObject) {
        when (jsonObject.getString(Constant.TYPE)) {
            Constant.NEW_CALL -> {
                if (jsonObject.getString("chatId") == chatId && jsonObject.getString("callId") == callId) {
                    logE("newCallListener :$jsonObject")
                    SocketIO.onReadyForCall(
                        jsonObject.getString("chatId"),
                        jsonObject.getString("name"),
                        jsonObject.getString("callId"),
                        jsonObject.getString("connectedUserId"),
                        jsonObject.getString("socketId"),
                        jsonObject.getString("sessionId")
                    )
                } else {
                    SocketIO.onHangout(
                        jsonObject.getString("chatId"),
                        jsonObject.getString("callId")
                    )
                }
            }
            Constant.OFFER -> {
                logE("newCallListener offer :$jsonObject")
                if (isFirstCall == true) {
                    isFirstCall = false
                    newCallReceived(jsonObject, true)
                } else {
                    newCallReceived(jsonObject, false)
                }
                answerCall(peerData)
            }
            Constant.NEW_CALL_RECEIVED -> {
                isNewCallReceived = true
                logE("New Call Received :$jsonObject")
                if (initiator == true) {
                    initiator = false
                    startDialTune()
                }
                EventBus.getDefault().post(CallStatus("Ringing..."))
                if (isVideo == true) {
                    turnOnSpeakers()
                }
            }
        }
    }


    private fun hangup() {
        CallsGlobal.stopCallTune()
        logE("PeerConnection array :$localPeerArray")
        peerConnectionFactory?.stopAecDump()
        if (videoCapture != null) {
            videoCapture?.stopCapture()
            videoCapture?.dispose()
            videoCapture = null
        }
        if (!localPeerArray.isNullOrEmpty()) {
            localPeerArray?.let {
                for (x in it.indices) {
                    it[x]?.close()
                }
            }
        }
        stopDialTune()
        turnOFFSpeakers()
        muteMicroPhone(false)
        CallsGlobal.keepSocketConnected = false
        handler?.removeCallbacks(callTimeRunnable!!)
        SocketIO.setSocketCallback(null)
        SocketIO.reAssignOfferListenerToMain()
        stopSelf()
        if (CallsGlobal.isAppInBackground) {
            CallsGlobal.keepSocketConnected = false
            SocketIO.disconnectSocket()
        }
        EventBus.getDefault().post(HangupCallScreen(false))
    }

    private fun stopDialTune() {
        toneGenerator?.stopTone()
        dialTuneHandler?.removeCallbacks(dialtuneRunnable!!)
    }

    private fun turnOnSpeakers() {
        if (!audioManager.isSpeakerphoneOn) {
            logE("TURNING ON SPEAKERS")
            audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            audioManager.isSpeakerphoneOn = true
        }
    }

    private fun turnOFFSpeakers() {
        if (audioManager.isSpeakerphoneOn) {
            logE("TURNING OFF SPEAKERS")
            audioManager.mode = AudioManager.MODE_NORMAL
            audioManager.isSpeakerphoneOn = false
        }
    }

    private fun muteMicroPhone(mic: Boolean) {
        if (mic) {
            if (!audioManager.isMicrophoneMute)
                audioManager.isMicrophoneMute = true
        } else {
            if (audioManager.isMicrophoneMute)
                audioManager.isMicrophoneMute = false
        }
    }

    private fun switchCamera() {
        if (videoCapture != null) {
            if (videoCapture is CameraVideoCapturer) {
                val cameraVideoCapturer = videoCapture as CameraVideoCapturer
                cameraVideoCapturer.switchCamera(null)
            } else {
                logE("Camera switch is not working its not a VideoCapturur")
                // Will not switch camera, video capturer is not a camera
            }
        }
    }

    private fun logE(msg: String) {
        D.e(TAG, msg)
    }

    // CallNotification
    private fun showIncomingNotification() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

        val channelId: String = ("incoming_id")
        val channelName: String = ("Incoming Call Notification")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var mChannel = notificationManager?.getNotificationChannel(channelId)
            if (mChannel == null) {
                mChannel = NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_HIGH
                )
                notificationManager?.createNotificationChannel(mChannel)
            }
        }
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val description: String = if (isVideo == true) {
            "Incoming video call from $calleeName"
        } else {
            "Incoming audio call from $calleeName"
        }
        notificationBuilder?.setSmallIcon(CallsGlobal.getNotificationIcon())
            ?.setContentTitle(getString(R.string.app_name))
            ?.setContentText(description)
            ?.setPriority(NotificationCompat.PRIORITY_HIGH)
            ?.setCategory(NotificationCompat.CATEGORY_CALL)
            ?.setColor(Color.parseColor("#179a63"))
            ?.setFullScreenIntent(fullScreenIntent(notificationId), true)
            ?.addAction(
                R.drawable.call_cancel,
                "Reject",
                rejectCallIntent(notificationId)
            )
            ?.addAction(
                R.drawable.call_receive,
                "Answer",
                acceptCallIntent(notificationId)
            )
            ?.setAutoCancel(true)
            ?.setOngoing(true)
        with(NotificationManagerCompat.from(this)) {
            notificationBuilder?.build()?.let { notify(notificationId, it) }
        }
        startForeground(notificationId, notificationBuilder?.build())
    }

    // CallNotification
    private fun showOutgoingNotification() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        val channelId: String = ("outgoing_id")
        val channelName: String = ("Outgoing Call Notification")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var mChannel = notificationManager?.getNotificationChannel(channelId)
            if (mChannel == null) {
                mChannel = NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_LOW
                )
                notificationManager?.createNotificationChannel(mChannel)
            }
        }

        val intent = Intent(this, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Constant.CHAT_USER_NAME, calleeName)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_USER_PICTURE, callImage)
        intent.putExtra(Constant.INITIATOR, initiator)
        intent.putExtra(Constant.NOTIFICATION_CLICK_TYPE, "show_screen")

        val pendingIntent =
            PendingIntent.getActivity(
                this,
                notificationId + 4,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val description = "Calling"
        notificationBuilder?.setSmallIcon(CallsGlobal.getNotificationIcon())
            ?.setContentTitle(calleeName)
            ?.setContentText(description)
            ?.setPriority(NotificationCompat.PRIORITY_LOW)
            ?.setContentIntent(pendingIntent)
            ?.setCategory(NotificationCompat.CATEGORY_CALL)?.color = Color.parseColor("#179a63")

        /* val notification: Notification? = notificationBuilder?.build()
         notificationManager?.notify(notificationId, notification)*/
        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notificationBuilder?.build()?.let { notify(notificationId, it) }
        }
        startForeground(notificationId, notificationBuilder?.build())
    }

    private fun acceptCallIntent(notificationId: Int): PendingIntent? {
        val intent = Intent(this, CallActivity::class.java)
        intent.putExtra(Constant.CHAT_USER_NAME, calleeName)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_USER_PICTURE, callImage)
        intent.putExtra(Constant.INITIATOR, initiator)
        intent.putExtra(Constant.IS_CALL_ACCEPTED, true)
        intent.putExtra(Constant.NOTIFICATION_CLICK_TYPE, "accept_call")

        return PendingIntent.getActivity(
            this,
            notificationId + 1,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )
    }

    private fun rejectCallIntent(notificationId: Int): PendingIntent? {
        val intent = Intent(this, RejectCallReceiver::class.java)
        intent.putExtra(Constant.CHAT_ID, chatId)
        intent.putExtra(Constant.CALL_ID, callId)
        intent.putExtra("isFromPush", isFromPush)
        intent.putExtra("notificationId", notificationId)

        return PendingIntent.getBroadcast(
            this,
            notificationId + 2,
            intent,
            0
        )
    }

    private fun fullScreenIntent(notificationId: Int): PendingIntent? {
        val intent = Intent(this, CallActivity::class.java)
        intent.putExtra(Constant.CHAT_USER_NAME, calleeName)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_USER_PICTURE, callImage)
        intent.putExtra(Constant.INITIATOR, initiator)
        intent.putExtra(Constant.IS_CALL_ACCEPTED, false)

        return PendingIntent.getActivity(
            this,
            notificationId + 3,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun showCallAcceptedNotification() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        val channelId: String = ("ongoing_id")
        val channelName: String = ("Ongoing Call Notification")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var mChannel = notificationManager?.getNotificationChannel(channelId)
            if (mChannel == null) {
                mChannel = NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_LOW
                )
                notificationManager?.createNotificationChannel(mChannel)
            }
        }
        val description: String = if (isVideo == true) {
            "Ongoing video call"
        } else {
            "Ongoing voice call"
        }
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
//        notificationBuilder?.mActions?.clear()
        val intent = Intent(this, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Constant.CHAT_USER_NAME, calleeName)
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo)
        intent.putExtra(Constant.CHAT_USER_PICTURE, callImage)
        intent.putExtra(Constant.INITIATOR, initiator)
        intent.putExtra(Constant.NOTIFICATION_CLICK_TYPE, "update_screen")

        val pendingIntent =
            PendingIntent.getActivity(
                this,
                notificationId + 5,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        notificationBuilder?.setSmallIcon(CallsGlobal.getNotificationIcon())
            ?.setContentTitle(getString(R.string.app_name))
            ?.setContentText(description)
            ?.setPriority(NotificationCompat.PRIORITY_LOW)
            ?.setColor(Color.parseColor("#179a63"))
            ?.setContentIntent(pendingIntent)
            ?.addAction(
                R.drawable.call_cancel,
                "Hangup",
                rejectCallIntent(notificationId)
            )
        with(NotificationManagerCompat.from(this)) {
            notificationBuilder?.build()?.let { notify(notificationId, it) }
        }
        startForeground(notificationId, notificationBuilder?.build())
    }

// Event Bus Subscriber

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPermissionsGranted(event: PermissionsGranted) {
        logE("Event Received in WebrtcService :$event")
        if (event.isPermissionsGranted) {
            intent?.let { startWebrtc(it) }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onClickingEvents(event: ClickEvents) {
        logE("Event Received in WebrtcService :$event")
        when (event.event) {
            Constant.HANG_UP_CLICKED -> {
                SocketIO.onHangout(chatId.toString(), callId.toString())
                logE("Hangup called 4")
                hangup()
            }
            Constant.ANSWER_CLICKED -> {
                showCallAcceptedNotification()
                SocketIO.onReadyForCall(
                    chatId.toString(),
                    calleeName.toString(),
                    callId.toString(),
                    connectedUserId.toString(),
                    socketId.toString(),
                    sessionId.toString()
                )
            }
            Constant.CAMERA_SWITCH_CLICKED -> {
                switchCamera()
            }
            Constant.CAMERA_OFF_CLICKED -> {
                val item =
                    peerMediaList.single { it.user.id == FetchData.getData(this, "userId").toInt() }
                val index = peerMediaList.indexOf(item)
                if (event.isCameraOff) {
                    videoCapture?.stopCapture()
                    SocketIO.switchToVideo(chatId.toString(), false)
                    item.isVideo = false
                    EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                } else {
                    videoCapture?.startCapture(1280, 720, 22)
                    SocketIO.switchToVideo(chatId.toString(), true)
                    item.isVideo = true
                    EventBus.getDefault().post(PeerMediaEvent(peerMediaList, index))
                }
            }
        }
    }

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    fun onAddParticipants(event: AddParticipantsEvent) {
        logE("Event Received in WebrtcService :$event")
        val memberList = ArrayList<GroupUsers>()
        for (user in peerMediaList) {
            memberList.add(
                    GroupUsers(
                            0,
                            user.user.id,
                            StringBuilder(user.user.firstname).append(user.user.lastname)
                                    .toString(),
                            user.user.profile_image
                    )
            )
        }
        val intent = Intent(this, SelectGroupConvoMembersActivity::class.java)
        intent.putExtra(Constant.ADD_MEMBERS, true)
        intent.putExtra(Constant.GROUP_MEMBERS, memberList)
        event.context.startActivityForResult(intent, Constant.SELECT_CONTACT)
    }*/

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    fun onAddParticipantsList(event: AddParticipantsListEvent) {
        logE("Event Received in WebrtcService :$event")
        event.members.forEach {
            toast(this, "Calling ${it.firstname}")
            SocketIO
                    .addToCallSocket(
                            chatId.toString(),
                            callId.toString(),
                            it.id.toString()
                    )
        }
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun feedCallScreen(event: FeedCallScreenEvent) {
        logE("Event Received in WebrtcService :$event")
        Handler().postDelayed(
            { EventBus.getDefault().post(EglBaseModel(rootEglBase)) },
            100
        )
        if (event.updateMe) {

            Handler().postDelayed(
                {
                    EventBus.getDefault()
                        .post(
                            UpdateScreenEvent(
                                calleeName,
                                isVideo,
                                callImage,
                                initiator,
                                isFirstCall
                            )
                        )
                },
                500
            )

            Handler().postDelayed(
                {
                    EventBus.getDefault()
                        .post(
                            ConnectionStatusEvent(
                                Constant.CONNECTED,
                                false,
                                timeWhenCallStarted
                            )
                        )
                },
                500
            )
            Handler().postDelayed(
                { EventBus.getDefault().post(PeerMediaEvent(peerMediaList, -1)) },
                500
            )
            Handler().postDelayed(
                { EventBus.getDefault().post(CallButtonsSettings(isSpeakerEnabled, isMuted)) },
                500
            )
        } else {
            if (isNewCallReceived)
                EventBus.getDefault().post(CallStatus("Ringing"))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun speakerSettings(event: SpeakerSettings) {
        logE("Event Received in WebrtcService :$event")
        if (event.isSpeakerEnabled) {
            isSpeakerEnabled = true
            turnOnSpeakers()
        } else {
            isSpeakerEnabled = false
            turnOFFSpeakers()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun muteSettings(event: MuteSettings) {
        logE("Event Received in WebrtcService :$event")
        if (event.isMuted) {
            muteMicroPhone(true)
            isMuted = true
        } else {
            isMuted = false
            muteMicroPhone(false)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSendReadForCall(event: SendReadyForCall) {
        logE("Event Received in WebrtcService :$event")
        showCallAcceptedNotification()
        isFirstCall = event.isCallAccepted
        SocketIO.onReadyForCall(
            chatId.toString(),
            calleeName.toString(),
            callId.toString(),
            connectedUserId.toString(),
            socketId.toString(),
            sessionId.toString()
        )
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCheckingService(event: CheckServiceEvent) {
        logE("Event Received in WebrtcService :$event")
        if (timeWhenCallStarted != 0L)
            Handler().postDelayed(
                {
                    EventBus.getDefault()
                        .post(CallInfoEvent(calleeName.toString(), timeWhenCallStarted))
                },
                100
            )

    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}
