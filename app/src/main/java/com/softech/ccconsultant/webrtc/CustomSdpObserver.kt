package com.softech.ccconsultant.webrtc

import com.softech.ccconsultant.webrtc.util.D
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription

internal open class CustomSdpObserver(logTag: String) : SdpObserver {


    private var tag: String? = null

    init {
        tag = this.javaClass.canonicalName
        this.tag = this.tag + " " + logTag
    }


    override fun onCreateSuccess(sessionDescription: SessionDescription) {
        D.e(
                tag.toString(),
                "onCreateSuccess() called with: sessionDescription = [$sessionDescription]"
        )
    }

    override fun onSetSuccess() {
        D.e(tag.toString(), "onSetSuccess() called")
    }

    override fun onCreateFailure(s: String) {
        D.e(tag.toString(), "onCreateFailure() called with: s = [$s]")
    }

    override fun onSetFailure(s: String) {
        D.e(tag.toString(), "onSetFailure() called with: s = [$s]")
    }

}
