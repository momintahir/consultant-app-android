package com.softech.ccconsultant.webrtc

import com.softech.ccconsultant.webrtc.util.D
import org.webrtc.*

internal open class CustomPeerConnectionObserver(logTag: String) : PeerConnection.Observer {

    private var logTag: String? = null

    init {
        this.logTag = this.javaClass.canonicalName
        this.logTag = this.logTag + " " + logTag
    }

    override fun onSignalingChange(signalingState: PeerConnection.SignalingState) {
        D.d(
                logTag.toString(),
                "onSignalingChange() called with: signalingState = [$signalingState]"
        )
    }

    override fun onIceConnectionChange(iceConnectionState: PeerConnection.IceConnectionState) {
        D.d(
                logTag.toString(),
                "onIceConnectionChange() called with: iceConnectionState = [$iceConnectionState]"
        )
    }

    override fun onIceConnectionReceivingChange(b: Boolean) {
        D.d(logTag.toString(), "onIceConnectionReceivingChange() called with: b = [$b]")
    }

    override fun onIceGatheringChange(iceGatheringState: PeerConnection.IceGatheringState) {
        D.d(
                logTag.toString(),
                "onIceGatheringChange() called with: iceGatheringState = [$iceGatheringState]"
        )
    }

    override fun onIceCandidate(iceCandidate: IceCandidate) {
        D.d(
                logTag.toString(),
                "onIceCandidate() called with: iceCandidate = [$iceCandidate]"
        )
    }

    override fun onIceCandidatesRemoved(iceCandidates: Array<IceCandidate>) {
        D.d(
                logTag.toString(),
                "onIceCandidatesRemoved() called with: iceCandidates = [$iceCandidates]"
        )
    }

    override fun onAddStream(mediaStream: MediaStream) {
        D.d(logTag.toString(), "onAddStream() called with: mediaStream = [$mediaStream]")
    }

    override fun onRemoveStream(mediaStream: MediaStream) {
        D.d(logTag.toString(), "onRemoveStream() called with: mediaStream = [$mediaStream]")
    }

    override fun onDataChannel(dataChannel: DataChannel) {
        D.d(logTag.toString(), "onDataChannel() called with: dataChannel = [$dataChannel]")
    }

    override fun onRenegotiationNeeded() {
        D.d(logTag.toString(), "onRenegotiationNeeded() called")
    }

    override fun onAddTrack(rtpReceiver: RtpReceiver, mediaStreams: Array<MediaStream>) {
        D.d(
                logTag.toString(),
                "onAddTrack() called with: rtpReceiver = [$rtpReceiver], mediaStreams = [$mediaStreams]"
        )
    }
}
