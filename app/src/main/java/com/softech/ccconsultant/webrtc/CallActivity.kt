package com.softech.ccconsultant.webrtc

import android.Manifest
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.softech.ccconsultant.R
import com.softech.ccconsultant.databinding.CallActivityBinding
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.webrtc.model.*
import com.softech.ccconsultant.webrtc.util.CallsGlobal
import com.softech.ccconsultant.webrtc.util.Constant
import com.softech.ccconsultant.webrtc.util.D
import com.softech.ccconsultant.webrtc.util.GlideDownloader
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.exitProcess

class CallActivity : AppCompatActivity(), View.OnClickListener, EasyPermissions.PermissionCallbacks,
        SensorEventListener {
    private val TAG = this.javaClass.simpleName
    var chatId: String? = null
    private var callImage: String? = null
    private lateinit var audioManager: AudioManager
    private var calleeName: String? = null
    private var isVideo = false
    lateinit var binding: CallActivityBinding
    private var isSpeakerOn = false
    private var isCameraOn = false
    private var isMicOn = false
    private var isFirstCall = false
    private var savedBaseTime: Long = 0
    private var peerMediaList: ArrayList<PeerMedia> = ArrayList()
    private var initiator = false
    var height = 0
    var width = 0
    private var mSensorManager: SensorManager? = null
    private var mProximity: Sensor? = null
    var showOrUpdateScreen: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        binding = DataBindingUtil.setContentView(this, R.layout.call_activity)
        turnScreenOnAndKeyguardOff()
        getDataFromIntent()
        initActivity()
        initPermissions()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        logE("onNewIntent is called")
        when (intent?.getStringExtra(Constant.NOTIFICATION_CLICK_TYPE)) {
            "reject_call" -> {
                finish()
            }
            "accept_call" -> {
                answerCall()
            }
            else -> {
                logE("onNewIntent is called but do nothing")
            }
        }

    }

    private fun Activity.turnScreenOnAndKeyguardOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                            or WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
            )
        }

        with(getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                requestDismissKeyguard(this@turnScreenOnAndKeyguardOff, null)
            }
        }
    }

    private fun getDataFromIntent() {
        showOrUpdateScreen = intent?.getStringExtra(Constant.NOTIFICATION_CLICK_TYPE)
        isVideo = intent.getBooleanExtra(Constant.IS_VIDEO_CALL, false)
        calleeName = intent.getStringExtra(Constant.CHAT_USER_NAME)
        callImage = intent.getStringExtra(Constant.CHAT_USER_PICTURE)
        initiator = intent.getBooleanExtra(Constant.INITIATOR, false)
        isFirstCall = intent.getBooleanExtra(Constant.IS_CALL_ACCEPTED, false)
    }

    private fun setScreen(name: String?, image: String?) {
        binding.tvName.text = name.toString()
        logE("setScreen :$image")
        GlideDownloader.load(
                this,
                binding.cvCalleeView,
                image,
                R.drawable.dummy_placeholder,
                R.drawable.dummy_placeholder
        )
    }

    private fun initActivity() {
        setScreen(calleeName, callImage)
        initViews()
        logE("Show Or Update Screen :$showOrUpdateScreen")
        if (showOrUpdateScreen == "update_screen") {
            binding.ibAnswer.visibility = View.GONE
            EventBus.getDefault().post(FeedCallScreenEvent(true))
        } else if (showOrUpdateScreen == "show_screen") {
            binding.ibAnswer.visibility = View.GONE
            EventBus.getDefault().post(FeedCallScreenEvent(false))
        } else {
            if (initiator) {
                binding.ibAnswer.visibility = View.GONE
                binding.tvCallStatus.text = "Calling"
            } else {
                binding.tvCallStatus.text = "Incoming"
                if (isFirstCall) {
                    CallsGlobal.stopCallTune()
                    showOrUpdateScreen = null
                    binding.ibAnswer.visibility = View.GONE
                    EventBus.getDefault().post(SendReadyForCall(true))
                }
            }
        }

    }

    private fun initPermissions() {
        val permission = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        )
        if (EasyPermissions.hasPermissions(this, *permission)) {
            if (showOrUpdateScreen.isNullOrEmpty()) {
                logE("Init Permissions granted")
                EventBus.getDefault().post(PermissionsGranted(true))
            }
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permissions_text_camera),
                    Constant.CAMERA_PERMISSION,
                    *permission
            )
        }
    }

    private fun initViews() {
        binding.rippleAnim.startRippleAnimation()
        initAudioVideoViews(isVideo)
        binding.ibHangUp.setOnClickListener(this)
        binding.ibAnswer.setOnClickListener(this)
        binding.ibMute.setOnClickListener(this)
//        binding.ibAddParticipant.setOnClickListener(this)
        binding.rlCameras.setOnClickListener(this)
        binding.ibSpeaker.setOnClickListener(this)
        binding.btnCameraOff.setOnClickListener(this)
        binding.ibMinimize.setOnClickListener(this)
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mProximity = mSensorManager?.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    private fun initAudioVideoViews(isVideo: Boolean) {
        if (isVideo) {
            binding.tvCall.text = "Video Call"
            binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_on)
        } else {
            binding.tvCall.text = "Voice Call"
            binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_off)
        }
        switchVideoViews(isVideo)
    }

    private fun switchVideoViews(isVideo: Boolean) {
        if (isVideo) {
            binding.ibCameraSwitch.alpha = 1f
            binding.ibCameraSwitch.setOnClickListener(this)
            binding.btnCameraOff.setBackgroundResource(R.drawable.camera_on)
            isSpeakerOn = true
            isCameraOn = true
        } else {
            binding.ibCameraSwitch.alpha = 0.5f
            binding.ibCameraSwitch.setOnClickListener(null)
            isSpeakerOn = false
            isCameraOn = false
            binding.btnCameraOff.setBackgroundResource(R.drawable.camera_off)
        }
    }

    private fun startChronometer(base: Long) {
        binding.tvCallStatus.visibility = View.GONE
        binding.chronometer.visibility = View.VISIBLE
        binding.chronometer.base = base
        binding.chronometer.start()
    }


    private fun answerCall() {
        CallsGlobal.stopCallTune()
        binding.ibAnswer.visibility = View.GONE
        binding.tvCallStatus.text = "Connecting"
        EventBus.getDefault().post(ClickEvents(Constant.ANSWER_CLICKED))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ibHangUp -> {
                EventBus.getDefault().post(ClickEvents(Constant.HANG_UP_CLICKED))
            }
            R.id.ibAnswer -> {
                answerCall()
            }
            R.id.ibSpeaker -> {
                if (!isSpeakerOn) {
                    binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_on)
                    isSpeakerOn = true
                    EventBus.getDefault().post(SpeakerSettings(true))
//                    turnOnSpeakers()
                } else {
                    binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_off)
                    isSpeakerOn = false
                    EventBus.getDefault().post(SpeakerSettings(false))
//                    turnOFFSpeakers()
                }
            }
            R.id.ibMute -> {
                if (!isMicOn) {
                    binding.ibMute.setBackgroundResource(R.drawable.icon_mute)
                    isMicOn = true
                    EventBus.getDefault().post(MuteSettings(true))
//                    muteMicroPhone(true)
                } else {
                    binding.ibMute.setBackgroundResource(R.drawable.icon_unmute)
                    isMicOn = false
                    EventBus.getDefault().post(MuteSettings(false))
//                    muteMicroPhone(false)
                }
            }
            R.id.ibCameraSwitch -> {
                EventBus.getDefault().post(ClickEvents(Constant.CAMERA_SWITCH_CLICKED))
            }
            R.id.ibMinimize -> {
                onBackPressed()
            }

            R.id.btnCameraOff -> {
                if (isCameraOn) {
                    isCameraOn = false
                    binding.btnCameraOff.setBackgroundResource(R.drawable.camera_off)
                    switchVideoViews(false)
                    EventBus.getDefault().post(ClickEvents(Constant.CAMERA_OFF_CLICKED, true))
                } else {
                    isCameraOn = true
                    binding.btnCameraOff.setBackgroundResource(R.drawable.camera_on)
                    switchVideoViews(true)
                    EventBus.getDefault().post(ClickEvents(Constant.CAMERA_OFF_CLICKED, false))
                }
            }

            /* R.id.ibAddParticipant -> {
                 EventBus.getDefault().post(AddParticipantsEvent(this))
             }*/
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constant.CAPTURE_PERMISSION_REQUEST_CODE -> {
                    /* logE("onActivityResult permission accepted")
                     mMediaProjectionPermissionResultCode = resultCode
                     mMediaProjectionPermissionResultData = data
                     initVideoCapturer(isScreenCapturer = true)
                     changeVideoTrack(localScreenVideoTrack)
                     val item =
                         peerMediaList.single { it.user.id == sharedPrefsHelper.getUser()?.id }
                     val idIndex = peerMediaList.indexOf(item)
                     item.track = localScreenVideoTrack
                     (binding.recyclerview.adapter as CallingAdapter).itemChanged(
                         peerMediaList,
                         idIndex
                     )*/
                }
                /*Constant.SELECT_CONTACT -> {
                    val list =
                            data?.getParcelableArrayListExtra<UserFriendData>(Constant.key_selected_members) as ArrayList<UserFriendData>
                    EventBus.getDefault().post(AddParticipantsListEvent(list))
                }*/
            }
        }
    }

    private fun logE(msg: String) {
        D.e(TAG, msg)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        D.e("requestCode", " $requestCode")
        when (requestCode) {
            Constant.CAMERA_PERMISSION -> {
                EventBus.getDefault().post(PermissionsGranted(true))
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        D.e("onPermissionsDenied", "requestCode: $requestCode")
        when (requestCode) {
            Constant.CAMERA_PERMISSION -> {
                if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
                    AppSettingsDialog.Builder(this).build().show()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onResume() {
        super.onResume()
        if (mProximity != null)
            mSensorManager?.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL)
        else logE("Proximity sensor is null in onResume")
    }

    override fun onPause() {
        super.onPause()
        if (mProximity != null)
            mSensorManager?.unregisterListener(this)
        else logE("Proximity sensor is null in onPause")
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val params = window.attributes
        if (event?.sensor?.type == Sensor.TYPE_PROXIMITY) {
            if (event.values[0] == 0f) {
                params.screenBrightness = 0f
                window.attributes = params
            } else {
                params.screenBrightness = -1f
                window.attributes = params
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEgleBaseEvent(event: EglBaseModel) {
        logE("Event Received in CallActivity :$event")
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        height = displayMetrics.heightPixels
        width = displayMetrics.widthPixels
        val flexBoxLayoutManager = FlexboxLayoutManager(this).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            alignItems = AlignItems.STRETCH
        }
        val adapterData = AdapterData(
                this,
                event.eglBase,
                (height - resources.getDimension(R.dimen._100sdp)).toInt(),
                width,
                isVideo,
                FetchData.getData(this, "userId").toInt()
        )
        binding.recyclerview.apply {
            layoutManager = flexBoxLayoutManager
            adapter = CallingAdapter(adapterData)
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
        logE("Layout manager is set for flex")
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPeerMediaEvent(event: PeerMediaEvent) {
        logE("Event Received in CallActivity :$event")
        if (event.index == -1) {
            (binding.recyclerview.adapter as CallingAdapter).updateList(
                    event.peerMediaList
            )
        } else {
            (binding.recyclerview.adapter as CallingAdapter).itemChanged(
                    event.peerMediaList, event.index
            )
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onConnectionStatusEvent(event: ConnectionStatusEvent) {
        logE("Event Received in CallActivity :$event")
        if (event.status == Constant.CONNECTED) {
            if (event.isFromReconnecting) {
                startChronometer(savedBaseTime)
            } else {
                startChronometer(event.timeWhenCallStarted)
                binding.cvCalleeView.visibility = View.GONE
                binding.rippleAnim.stopRippleAnimation()
                binding.recyclerview.visibility = View.VISIBLE
                binding.rlBottomOptions.visibility = View.VISIBLE
//                binding.ibAddParticipant.visibility = View.VISIBLE
                binding.ibMinimize.visibility = View.VISIBLE
            }
        } else if (event.status == Constant.RECONNECTING) {
            binding.chronometer.visibility = View.GONE
            binding.tvCallStatus.visibility = View.VISIBLE
            binding.chronometer.stop()
            savedBaseTime = binding.chronometer.base
            binding.tvCallStatus.text = "Reconnecting"
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onHangup(event: HangupCallScreen) {
        logE("Event Received in CallActivity  :$event")
        if (isTaskRoot) {
            logE("isTaskRoot is true")
            finish()
            exitProcess(0)
        } else {
            logE("isTaskRoot is false")
            finish()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCallStatusEvent(event: CallStatus) {
        logE("Event Received in CallActivity :$event")
        binding.tvCallStatus.text = event.status
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCallButtonSettings(event: CallButtonsSettings) {
        logE("Event Received in CallActivity :$event")
        if (event.isSpeakerEnabled)
            binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_on)
        if (event.isMuteEnabled)
            binding.ibMute.setBackgroundResource(R.drawable.icon_mute)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUpdateScreen(event: UpdateScreenEvent) {
        logE("Event Received in CallActivity :$event")
        isVideo = event.isVideo!!
        calleeName = event.calleeName
        callImage = event.image
        initiator = event.initiator!!
        isFirstCall = event.isFirstCall!!
        setScreen(calleeName, callImage)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
}
