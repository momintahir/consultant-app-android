package com.softech.ccconsultant.webrtc.callbacks

import org.json.JSONObject

interface WebSocketCallback {
    fun webSocketCallback(jsonObject: JSONObject)
}

interface WebSocketNewCallListener {
    fun newCallListener(jsonObject: JSONObject)
}
