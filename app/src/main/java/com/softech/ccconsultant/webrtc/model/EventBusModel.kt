package com.softech.ccconsultant.webrtc.model

import org.webrtc.EglBase

data class PermissionsGranted(
        val isPermissionsGranted: Boolean
)

data class EglBaseModel(
        val eglBase: EglBase?
)

data class PeerMediaEvent(
        val peerMediaList: ArrayList<PeerMedia>,
        val index: Int
)

data class ConnectionStatusEvent(
        val status: String,
        val isFromReconnecting: Boolean = false,
        val timeWhenCallStarted: Long = 0
)

data class CallStatus(
        val status: String
)

data class ClickEvents(
        val event: String,
        val isCameraOff: Boolean = false
)

data class HangupCallScreen(
        val isFromPush: Boolean
)

data class FeedCallScreenEvent(
        val updateMe: Boolean
)

data class CallButtonsSettings(
        val isSpeakerEnabled: Boolean,
        val isMuteEnabled: Boolean
)

data class SpeakerSettings(
        val isSpeakerEnabled: Boolean
)

data class MuteSettings(
        val isMuted: Boolean
)

data class SendReadyForCall(
        val isCallAccepted: Boolean
)

data class CheckServiceEvent(
        val checkingService: Boolean
)

data class CallInfoEvent(
        val name: String,
        val time: Long
)

data class UpdateScreenEvent(
        val calleeName: String?,
        val isVideo: Boolean?,
        val image: String?,
        val initiator: Boolean?,
        val isFirstCall: Boolean?
)

