package com.softech.ccconsultant.webrtc.util

object Constant {

    const val ADD_PARTICIPANTS = "ADD_PARTICIPANTS"
    var IsAppRunningCheck = false

    //WebRtc Actions
    const val CANDIDATE = "candidate"
    const val NEW_CALL = "newcall"
    const val READY_FOR_CALL = "readyforcall"
    const val ANSWER = "answer"
    const val OFFER = "offer"
    const val TYPE = "type"
    const val REJECT = "reject"
    const val SEND_REQUEST = "send_request"
    const val ADD_TO_CALL = "addtocall"
    const val CHECK_CALL_STATUS = "checkCallStatus"
    const val SWITCH_VIDEO = "switchvideo"
    const val CALL_ACCEPTED = "callaccepted"
    const val CHAT_USER_NAME = "chat_user_name"
    const val CHAT_USER_PICTURE = "chat_user_picture"
    const val CHAT_ID = "kalam_chat_id"
    const val IS_VIDEO_CALL = "isVideoCall"
    const val CONNECTED_USER_ID = "connectedUserId"
    const val IS_FROM_PUSH = "is_call_boolean"
    const val INITIATOR = "caller_initiator"
    const val IS_CALL_ACCEPTED = "is_call_accepted"
    const val PUSH_TOKEN = "push_token_key"
    const val IS_NEW_TOKEN = "is_new_token"

    const val CAMERA_PERMISSION = 1002
    const val CAPTURE_PERMISSION_REQUEST_CODE = 1040





    //Call New Actions
    const val NEW_CALL_RECEIVED = "newcallreceived"
    const val NOTIFICATION_CLICK_TYPE = "notification_click_type"
    const val UPDATE_PUSH_TOKEN = "updatepushtoken"


    //WebRtc States
    const val CONNECTING = "Connecting"
    const val CONNECTED = "Connected"
    const val RECONNECTING = "Reconnecting"
    const val FAILED = "Disconnected"
    const val CLOSED = "Disconnected"
    const val CALLING = "Calling"

    //New Constants
    const val CALL_ID = "callId"
    const val SOCKET_ID = "socketId"
    const val SESSION_ID = "sessionId"
    const val HANG_UP_CLICKED = "hangup_clicked"
    const val ANSWER_CLICKED = "answer_clicked"
    const val CAMERA_SWITCH_CLICKED = "camera_switch_clicked"
    const val CAMERA_OFF_CLICKED = "camera_off_clicked"
}