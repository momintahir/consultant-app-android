package com.softech.ccconsultant.webrtc

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager
import com.softech.ccconsultant.R
import com.softech.ccconsultant.databinding.ItemCallingBinding
import com.softech.ccconsultant.webrtc.model.AdapterData
import com.softech.ccconsultant.webrtc.model.PeerMedia
import com.softech.ccconsultant.webrtc.util.Constant
import com.softech.ccconsultant.webrtc.util.D
import com.softech.ccconsultant.webrtc.util.GlideDownloader


class CallingAdapter(
        val adapterData: AdapterData
) :
        RecyclerView.Adapter<CallingAdapter.CallViewModel>() {
    private val TAG = this.javaClass.simpleName
    private var peerMediaList: ArrayList<PeerMedia> = ArrayList()

    fun itemChanged(peerMediaList: ArrayList<PeerMedia>, position: Int) {
        this.peerMediaList = peerMediaList
        notifyItemChanged(position)
    }

    fun updateList(peerMediaList: ArrayList<PeerMedia>) {
        this.peerMediaList = peerMediaList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CallViewModel {
        return CallViewModel(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_calling,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: CallViewModel, position: Int) {
        val item = peerMediaList[position]
        if (item.isVideo == true) {
            holder.binding.remoteVideoView.visibility = View.VISIBLE
            holder.binding.ivUser.visibility = View.GONE
            holder.binding.remoteVideoView.clearImage()
            item.track?.addSink(holder.binding.remoteVideoView)
            logE("Here is the stream :${item.track}")
        } else {
            showImage(holder, item)
        }
        val lp = holder.binding.rlMainCall.layoutParams
        if (lp is FlexboxLayoutManager.LayoutParams) {
            lp.flexGrow = 1f
            logE("Peer List size :${peerMediaList.size}")
            if (item.isExpanded) {
                lp.width = adapterData.width
                lp.height = adapterData.height
            } else {
                when (peerMediaList.size) {
                    1 -> {
                        holder.binding.tvStatus.visibility = View.GONE
                        lp.height = FlexboxLayout.LayoutParams.MATCH_PARENT
                        logE("Adapter List size is 1")
                    }
                    2 -> {
                        holder.binding.tvStatus.visibility = View.GONE
                        lp.width = FlexboxLayout.LayoutParams.MATCH_PARENT
                        lp.height = adapterData.height / 2
                        logE("Adapter List size is 2")
                    }
                    else -> {
                        lp.width = adapterData.width / 2
                        lp.height = adapterData.height / 2
                        logE("Adapter List size is greater than 2")
                    }
                }
            }
            if (adapterData.userId == item.user.id) {
                holder.binding.tvStatus.visibility = View.GONE
                holder.binding.tvName.text = "You"
            } else {
                if (item.user.firstname.isNotEmpty()) {
                    holder.binding.tvName.visibility = View.VISIBLE
                    holder.binding.tvName.text =
                            StringBuilder(item.user.firstname).append(item.user.lastname).toString()
                } else holder.binding.tvName.visibility = View.GONE
                when (item.state) {
                    Constant.CONNECTED -> {
                        holder.binding.tvStatus.visibility = View.VISIBLE
                        holder.binding.tvStatus.text = item.state
                        Handler().postDelayed(
                                { holder.binding.tvStatus.visibility = View.GONE },
                                1500
                        )
                    }

                    else -> {
                        holder.binding.tvStatus.visibility = View.VISIBLE
                        holder.binding.tvStatus.text = item.state
                    }
                }
            }
        } else {
            logE("Not a flex box manager")
        }
        holder.binding.rlMainCall.setOnClickListener {
            item.isExpanded = !item.isExpanded
            notifyItemChanged(position)
        }
    }

    private fun showImage(holder: CallViewModel, item: PeerMedia) {
        holder.binding.remoteVideoView.visibility = View.GONE
        holder.binding.ivUser.visibility = View.VISIBLE
        GlideDownloader.load(
                adapterData.context,
                holder.binding.ivUser,
                item.user.profile_image,
                R.color.grey,
                R.color.grey
        )
    }

    override fun getItemCount(): Int {
        return peerMediaList.size
    }

    private fun logE(msg: String) {
        D.e(TAG, msg)
    }


    inner class CallViewModel(val binding: ItemCallingBinding) :
            RecyclerView.ViewHolder(binding.root) {
        init {
            binding.remoteVideoView.init(adapterData.eglBase?.eglBaseContext, null)
            binding.remoteVideoView.setZOrderMediaOverlay(true)
            binding.remoteVideoView.setMirror(true)
        }
    }
}
