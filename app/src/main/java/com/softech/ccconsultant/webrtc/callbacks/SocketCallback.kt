package com.softech.ccconsultant.webrtc.callbacks

import org.json.JSONArray
import org.json.JSONObject

interface SocketCallback {
    fun socketResponse(jsonObject: JSONObject, type: String)
}

interface MessageTypingListener {
    fun typingResponse(jsonObject: JSONObject, isTyping: Boolean)
}

interface JSONArrayCallBack {
    fun onArrayCallback(array: JSONArray?, isStatusResponse: Boolean)
}

interface LocationsCallback {
    fun onLocationCallback(jsonObject: JSONObject)
}
