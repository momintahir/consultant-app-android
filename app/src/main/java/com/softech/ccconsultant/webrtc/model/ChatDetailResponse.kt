package com.softech.ccconsultant.webrtc.model

import android.content.Context
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.webrtc.EglBase
import org.webrtc.VideoTrack

data class GroupMembers(
        @SerializedName("firstname")
        val firstname: String,
        @SerializedName("lastname")
        val lastname: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("profile_image")
        val profile_image: String?,
        @SerializedName("sessionId")
        val sessionId: String
)

data class PeerData(
        @SerializedName("position")
        val position: Int,
        @SerializedName("userId")
        val userId: String,
        @SerializedName("chatId")
        val chatId: String,
        @SerializedName("callId")
        val callId: String,
        @SerializedName("isVideo")
        val isVideo: Boolean,
        @SerializedName("sessionId")
        val sessionId: String,
        @SerializedName("socketId")
        val socketId: String
)

data class PeerMedia(
        @SerializedName("user")
        var user: GroupMembers,
        @SerializedName("state")
        var state: String,
        @SerializedName("isVideo")
        var isVideo: Boolean?,
        @SerializedName("track")
        var track: VideoTrack?,
        @SerializedName("isExpanded")
        var isExpanded: Boolean
)

data class AdapterData(
        val context: Context,
        var eglBase: EglBase?,
        val height: Int,
        val width: Int,
        val isVideo: Boolean,
        val userId: Int
)

@Parcelize
data class GroupUsers(
        @SerializedName("is_admin")
        val is_admin: Int,
        @SerializedName("user_id")
        val user_id: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("profile_image")
        val profile_image: String?
) : Parcelable