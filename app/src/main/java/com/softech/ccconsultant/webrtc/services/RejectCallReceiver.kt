package com.softech.ccconsultant.webrtc.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.softech.ccconsultant.webrtc.SocketIO
import com.softech.ccconsultant.webrtc.model.ClickEvents
import com.softech.ccconsultant.webrtc.util.Constant
import com.softech.ccconsultant.webrtc.util.D
import org.greenrobot.eventbus.EventBus

class RejectCallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val chatId = intent?.getStringExtra(Constant.CHAT_ID)
        val callId = intent?.getStringExtra(Constant.CALL_ID)
        val isFromPush = intent?.getBooleanExtra("isFromPush", false)
        D.e("RejectCallReceiver", "callId:$chatId")
        SocketIO.onHangout(chatId.toString(), callId.toString())
        EventBus.getDefault().post(ClickEvents(Constant.HANG_UP_CLICKED))
    }
}