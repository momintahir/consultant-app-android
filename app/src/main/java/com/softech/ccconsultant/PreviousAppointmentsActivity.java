package com.softech.ccconsultant;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.adapters.SearchAdapter;
import com.softech.ccconsultant.data.models.models.SearchAppointmentModel;
import com.softech.ccconsultant.sharedpreferences.FetchData;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviousAppointmentsActivity extends AppCompatActivity {

    @BindView(R.id.recyclViewAppointments)
    RecyclerView recyclViewAppointments;
    ArrayList<SearchAppointmentModel> appointmentsModelArrayList = new ArrayList<SearchAppointmentModel>();
    private SearchAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_appointments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Appointments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviousAppointmentsActivity.this.finish();
            }
        });

        ButterKnife.bind(this);


        String searchData = FetchData.getData(PreviousAppointmentsActivity.this, "searchData");
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<SearchAppointmentModel>>(){}.getType();
        appointmentsModelArrayList =gson.fromJson(searchData.toString(), type);
        populateRecylView();
    }

    private void populateRecylView() {

        mAdapter = new SearchAdapter(appointmentsModelArrayList, PreviousAppointmentsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PreviousAppointmentsActivity.this);
        recyclViewAppointments.setLayoutManager(mLayoutManager);
        recyclViewAppointments.setItemAnimator(new DefaultItemAnimator());
        recyclViewAppointments.setAdapter(mAdapter);
        // mAdapter.notifyDataSetChanged();
        recyclViewAppointments.addItemDecoration(new DividerItemDecoration(PreviousAppointmentsActivity.this, DividerItemDecoration.VERTICAL));

    }


}
