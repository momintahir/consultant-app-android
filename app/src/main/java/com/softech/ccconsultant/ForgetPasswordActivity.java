package com.softech.ccconsultant;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Dialogues;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgetPasswordActivity extends AppCompatActivity {

    private final String TAG = "ForgetPasswordActivity";

    @BindView(R.id.editTextEmailAddress)
    EditText editTextEmailAddress;
    @BindView(R.id.buttonForgotPassword)
    Button buttonForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgetPasswordActivity.this.finish();
            }
        });

        ButterKnife.bind(ForgetPasswordActivity.this);

        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextEmailAddress.getText().toString().length() > 0) {
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                    String email = editTextEmailAddress.getText().toString().trim();
                    boolean isValidEmaillId = isValidEmaillId(email);
                    if (email.matches(emailPattern)) {
                        forgetPassword();
                    } else {
                        Toast.makeText(ForgetPasswordActivity.this, "Please add correct Email address.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ForgetPasswordActivity.this, "Please add Email address.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void forgetPassword() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Forget Password");
        params.put("email", editTextEmailAddress.getText().toString());

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                if (response != null) {

                    try {
                        Log.e(TAG, "Forget Password Response: "+response );
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {

                            Dialogues.showOkDialogue(ForgetPasswordActivity.this, "We have sent you an email in response to your request to reset your password. Please check your inbox.");

                        }
                        if (Integer.parseInt(code) == 504) {
                            Dialogues.showOkDialogue(ForgetPasswordActivity.this,"Email not Exist.");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {
                Logs.showLog("Login", response);
            }
        }, ForgetPasswordActivity.this, params,
                Const.FORGOTPASSWORD);
        apiConnection.makeStringReq();
    }
}
