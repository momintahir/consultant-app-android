
package com.softech.ccconsultant.data.models.models.SelectedDocument;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("OID")
    @Expose
    private Integer oID;
    @SerializedName("PATIENTCHARTFOLDER")
    @Expose
    private Integer pATIENTCHARTFOLDER;
    @SerializedName("FILENAME")
    @Expose
    private String fILENAME;
    @SerializedName("FILETYPE")
    @Expose
    private String fILETYPE;
    @SerializedName("FILEPATH")
    @Expose
    private String fILEPATH;
    @SerializedName("FILEDATA")
    @Expose
    private Object fILEDATA;
    @SerializedName("FILETHUMB")
    @Expose
    private String fILETHUMB;
    @SerializedName("FILEREMARKS")
    @Expose
    private String fILEREMARKS;
    @SerializedName("FILESIZE")
    @Expose
    private Integer fILESIZE;
    @SerializedName("INSERTUSER")
    @Expose
    private Integer iNSERTUSER;
    @SerializedName("Total")
    @Expose
    private Integer total;
    @SerializedName("FILE")
    @Expose
    private String file;

    public String getFile() {
        return file;
    }
    public void setFile(String file) {
        this.file = file;
    }

    public Integer getOID() {
        return oID;
    }

    public void setOID(Integer oID) {
        this.oID = oID;
    }

    public Integer getPATIENTCHARTFOLDER() {
        return pATIENTCHARTFOLDER;
    }

    public void setPATIENTCHARTFOLDER(Integer pATIENTCHARTFOLDER) {
        this.pATIENTCHARTFOLDER = pATIENTCHARTFOLDER;
    }

    public String getFILENAME() {
        return fILENAME;
    }

    public void setFILENAME(String fILENAME) {
        this.fILENAME = fILENAME;
    }

    public String getFILETYPE() {
        return fILETYPE;
    }

    public void setFILETYPE(String fILETYPE) {
        this.fILETYPE = fILETYPE;
    }

    public String getFILEPATH() {
        return fILEPATH;
    }

    public void setFILEPATH(String fILEPATH) {
        this.fILEPATH = fILEPATH;
    }

    public Object getFILEDATA() {
        return fILEDATA;
    }

    public void setFILEDATA(Object fILEDATA) {
        this.fILEDATA = fILEDATA;
    }

    public String getFILETHUMB() {
        return fILETHUMB;
    }

    public void setFILETHUMB(String fILETHUMB) {
        this.fILETHUMB = fILETHUMB;
    }

    public String getFILEREMARKS() {
        return fILEREMARKS;
    }

    public void setFILEREMARKS(String fILEREMARKS) {
        this.fILEREMARKS = fILEREMARKS;
    }

    public Integer getFILESIZE() {
        return fILESIZE;
    }

    public void setFILESIZE(Integer fILESIZE) {
        this.fILESIZE = fILESIZE;
    }

    public Integer getINSERTUSER() {
        return iNSERTUSER;
    }

    public void setINSERTUSER(Integer iNSERTUSER) {
        this.iNSERTUSER = iNSERTUSER;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
