package com.softech.ccconsultant.data.models

import java.io.Serializable

data class LabsList(
        val PharmacyList:List<Pharmacy>,
        val PathLabList: List<Lab>,
        val RadLabList: List<Lab>
) : Serializable {

}