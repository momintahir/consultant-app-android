package com.softech.ccconsultant.data.models.models.Login;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("response")
	private String response;

	@SerializedName("action")
	private String action;

	public Meta getMeta(){
		return meta;
	}

	public String getResponse(){
		return response;
	}

	public String getAction(){
		return action;
	}
}