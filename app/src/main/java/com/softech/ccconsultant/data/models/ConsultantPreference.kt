package com.softech.ccconsultant.data.models

import java.io.Serializable

data class ConsultantPreference(
        val Oid: Int,
        val Address:String,
        val Name: String
) : Serializable {

}