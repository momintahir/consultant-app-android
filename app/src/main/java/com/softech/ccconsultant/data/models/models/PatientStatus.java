package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;

public class PatientStatus {

    @SerializedName("iPatID")
    private String iPatID;

    @SerializedName("IsOnline")
    private String IsOnline;

    public String getiPatID() {
        return iPatID;
    }

    public String getIsOnline() {
        return IsOnline;
    }
}
