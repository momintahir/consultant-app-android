package com.softech.ccconsultant.data.models

import java.io.Serializable

data class Lab(
        val labID: Int,
        val labAddress:String,
        val labName: String,
        val labCity: String,
        val labType: String
        ) : Serializable {
        override fun toString(): String {
                return labName
        }
}