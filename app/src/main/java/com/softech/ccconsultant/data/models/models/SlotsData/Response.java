
package com.softech.ccconsultant.data.models.models.SlotsData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("AppID")
    @Expose
    private Integer appID;
    @SerializedName("iPatID")
    @Expose
    private Integer iPatID;
    @SerializedName("vPatientName")
    @Expose
    private Object vPatientName;
    @SerializedName("EventType")
    @Expose
    private Integer eventType;
    @SerializedName("vStartTime")
    @Expose
    private String vStartTime;
    @SerializedName("vEndTime")
    @Expose
    private Object vEndTime;
    @SerializedName("vCell")
    @Expose
    private Object vCell;
    @SerializedName("vNotes")
    @Expose
    private Object vNotes;
    @SerializedName("vMrNo")
    @Expose
    private Object vMrNo;
    @SerializedName("vAge")
    @Expose
    private Object vAge;
    @SerializedName("slotTime")
    @Expose
    private String slotTime;
    @SerializedName("slotEndTime")
    @Expose
    private String slotEndTime;
    @SerializedName("slotGroup")
    @Expose
    private String slotGroup;
    @SerializedName("iSlotID")
    @Expose
    private Integer iSlotID;
    @SerializedName("iSlotStatus")
    @Expose
    private Integer iSlotStatus;
    @SerializedName("isAllowed")
    @Expose
    private Integer isAllowed;
    @SerializedName("iCalendarID")
    @Expose
    private Integer iCalendarID;
    @SerializedName("isWaiting")
    @Expose
    private Integer isWaiting;
    @SerializedName("slot")
    @Expose
    private String slot;
    @SerializedName("vDate")
    @Expose
    private String vDate;
    @SerializedName("dAppDate")
    @Expose
    private Object dAppDate;
    @SerializedName("isDel")
    @Expose
    private Integer isDel;
    @SerializedName("iTWS")
    @Expose
    private Integer iTWS;
    @SerializedName("vUserName")
    @Expose
    private Object vUserName;
    @SerializedName("dInsertDT")
    @Expose
    private Object dInsertDT;
    @SerializedName("IsDisabled")
    @Expose
    private Integer isDisabled;
    @SerializedName("vDay")
    @Expose
    private String vDay;
    @SerializedName("BillNo")
    @Expose
    private Integer billNo;
    @SerializedName("ConId")
    @Expose
    private Integer conId;
    @SerializedName("OrgId")
    @Expose
    private Integer orgId;
    @SerializedName("FirstName")
    @Expose
    private Object firstName;
    @SerializedName("LastName")
    @Expose
    private Object lastName;
    @SerializedName("DOB")
    @Expose
    private Object dOB;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("Address")
    @Expose
    private Object address;
    @SerializedName("City")
    @Expose
    private Object city;
    @SerializedName("Country")
    @Expose
    private Object country;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("MRNo")
    @Expose
    private Object mRNo;
    @SerializedName("IsPortal")
    @Expose
    private Integer isPortal;
    @SerializedName("Avatar")
    @Expose
    private Object avatar;
    @SerializedName("AppointmentType")
    @Expose
    private Object appointmentType;
    @SerializedName("ConDebt")
    @Expose
    private Integer conDebt;
    @SerializedName("OrgDebt")
    @Expose
    private Integer orgDebt;

    public Integer getAppID() {
        return appID;
    }

    public void setAppID(Integer appID) {
        this.appID = appID;
    }

    public Integer getIPatID() {
        return iPatID;
    }

    public void setIPatID(Integer iPatID) {
        this.iPatID = iPatID;
    }

    public Object getVPatientName() {
        return vPatientName;
    }

    public void setVPatientName(Object vPatientName) {
        this.vPatientName = vPatientName;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public String getVStartTime() {
        return vStartTime;
    }

    public void setVStartTime(String vStartTime) {
        this.vStartTime = vStartTime;
    }

    public Object getVEndTime() {
        return vEndTime;
    }

    public void setVEndTime(Object vEndTime) {
        this.vEndTime = vEndTime;
    }

    public Object getVCell() {
        return vCell;
    }

    public void setVCell(Object vCell) {
        this.vCell = vCell;
    }

    public Object getVNotes() {
        return vNotes;
    }

    public void setVNotes(Object vNotes) {
        this.vNotes = vNotes;
    }

    public Object getVMrNo() {
        return vMrNo;
    }

    public void setVMrNo(Object vMrNo) {
        this.vMrNo = vMrNo;
    }

    public Object getVAge() {
        return vAge;
    }

    public void setVAge(Object vAge) {
        this.vAge = vAge;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public String getSlotEndTime() {
        return slotEndTime;
    }

    public void setSlotEndTime(String slotEndTime) {
        this.slotEndTime = slotEndTime;
    }

    public String getSlotGroup() {
        return slotGroup;
    }

    public void setSlotGroup(String slotGroup) {
        this.slotGroup = slotGroup;
    }

    public Integer getISlotID() {
        return iSlotID;
    }

    public void setISlotID(Integer iSlotID) {
        this.iSlotID = iSlotID;
    }

    public Integer getISlotStatus() {
        return iSlotStatus;
    }

    public void setISlotStatus(Integer iSlotStatus) {
        this.iSlotStatus = iSlotStatus;
    }

    public Integer getIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(Integer isAllowed) {
        this.isAllowed = isAllowed;
    }

    public Integer getICalendarID() {
        return iCalendarID;
    }

    public void setICalendarID(Integer iCalendarID) {
        this.iCalendarID = iCalendarID;
    }

    public Integer getIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(Integer isWaiting) {
        this.isWaiting = isWaiting;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public String getVDate() {
        return vDate;
    }

    public void setVDate(String vDate) {
        this.vDate = vDate;
    }

    public Object getDAppDate() {
        return dAppDate;
    }

    public void setDAppDate(Object dAppDate) {
        this.dAppDate = dAppDate;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getITWS() {
        return iTWS;
    }

    public void setITWS(Integer iTWS) {
        this.iTWS = iTWS;
    }

    public Object getVUserName() {
        return vUserName;
    }

    public void setVUserName(Object vUserName) {
        this.vUserName = vUserName;
    }

    public Object getDInsertDT() {
        return dInsertDT;
    }

    public void setDInsertDT(Object dInsertDT) {
        this.dInsertDT = dInsertDT;
    }

    public Integer getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getVDay() {
        return vDay;
    }

    public void setVDay(String vDay) {
        this.vDay = vDay;
    }

    public Integer getBillNo() {
        return billNo;
    }

    public void setBillNo(Integer billNo) {
        this.billNo = billNo;
    }

    public Integer getConId() {
        return conId;
    }

    public void setConId(Integer conId) {
        this.conId = conId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getDOB() {
        return dOB;
    }

    public void setDOB(Object dOB) {
        this.dOB = dOB;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getMRNo() {
        return mRNo;
    }

    public void setMRNo(Object mRNo) {
        this.mRNo = mRNo;
    }

    public Integer getIsPortal() {
        return isPortal;
    }

    public void setIsPortal(Integer isPortal) {
        this.isPortal = isPortal;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public Object getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(Object appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Integer getConDebt() {
        return conDebt;
    }

    public void setConDebt(Integer conDebt) {
        this.conDebt = conDebt;
    }

    public Integer getOrgDebt() {
        return orgDebt;
    }

    public void setOrgDebt(Integer orgDebt) {
        this.orgDebt = orgDebt;
    }

}
