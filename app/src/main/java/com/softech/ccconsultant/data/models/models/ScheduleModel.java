package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScheduleModel {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ClinicId")
    @Expose
    private Integer clinicId;
    @SerializedName("ConsultantId")
    @Expose
    private Integer consultantId;
    @SerializedName("Mon")
    @Expose
    private Integer mon;
    @SerializedName("Tue")
    @Expose
    private Integer tue;
    @SerializedName("Wed")
    @Expose
    private Integer wed;
    @SerializedName("Thu")
    @Expose
    private Integer thu;
    @SerializedName("Fri")
    @Expose
    private Integer fri;
    @SerializedName("Sat")
    @Expose
    private Integer sat;
    @SerializedName("Sun")
    @Expose
    private Integer sun;
    @SerializedName("GenDay")
    @Expose
    private Integer genDay;
    @SerializedName("MonStart")
    @Expose
    private String monStart;
    @SerializedName("MonEnd")
    @Expose
    private String monEnd;
    @SerializedName("TueStart")
    @Expose
    private String tueStart;
    @SerializedName("TueEnd")
    @Expose
    private String tueEnd;
    @SerializedName("WedStart")
    @Expose
    private String wedStart;
    @SerializedName("WedEnd")
    @Expose
    private String wedEnd;
    @SerializedName("ThuStart")
    @Expose
    private String thuStart;
    @SerializedName("ThuEnd")
    @Expose
    private String thuEnd;
    @SerializedName("FriStart")
    @Expose
    private String friStart;
    @SerializedName("FriEnd")
    @Expose
    private String friEnd;
    @SerializedName("SatStart")
    @Expose
    private String satStart;
    @SerializedName("SatEnd")
    @Expose
    private String satEnd;
    @SerializedName("SunStart")
    @Expose
    private String sunStart;
    @SerializedName("SunEnd")
    @Expose
    private String sunEnd;
    @SerializedName("GenStart")
    @Expose
    private Object genStart;
    @SerializedName("GenEnd")
    @Expose
    private Object genEnd;
    @SerializedName("Hours")
    @Expose
    private Object hours;
    @SerializedName("Minutes")
    @Expose
    private Object minutes;
    @SerializedName("Duration")
    @Expose
    private Object duration;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public Integer getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Integer consultantId) {
        this.consultantId = consultantId;
    }

    public Integer getMon() {
        return mon;
    }

    public void setMon(Integer mon) {
        this.mon = mon;
    }

    public Integer getTue() {
        return tue;
    }

    public void setTue(Integer tue) {
        this.tue = tue;
    }

    public Integer getWed() {
        return wed;
    }

    public void setWed(Integer wed) {
        this.wed = wed;
    }

    public Integer getThu() {
        return thu;
    }

    public void setThu(Integer thu) {
        this.thu = thu;
    }

    public Integer getFri() {
        return fri;
    }

    public void setFri(Integer fri) {
        this.fri = fri;
    }

    public Integer getSat() {
        return sat;
    }

    public void setSat(Integer sat) {
        this.sat = sat;
    }

    public Integer getSun() {
        return sun;
    }

    public void setSun(Integer sun) {
        this.sun = sun;
    }

    public Integer getGenDay() {
        return genDay;
    }

    public void setGenDay(Integer genDay) {
        this.genDay = genDay;
    }

    public String getMonStart() {
        return monStart;
    }

    public void setMonStart(String monStart) {
        this.monStart = monStart;
    }

    public String getMonEnd() {
        return monEnd;
    }

    public void setMonEnd(String monEnd) {
        this.monEnd = monEnd;
    }

    public String getTueStart() {
        return tueStart;
    }

    public void setTueStart(String tueStart) {
        this.tueStart = tueStart;
    }

    public String getTueEnd() {
        return tueEnd;
    }

    public void setTueEnd(String tueEnd) {
        this.tueEnd = tueEnd;
    }

    public String getWedStart() {
        return wedStart;
    }

    public void setWedStart(String wedStart) {
        this.wedStart = wedStart;
    }

    public String getWedEnd() {
        return wedEnd;
    }

    public void setWedEnd(String wedEnd) {
        this.wedEnd = wedEnd;
    }

    public String getThuStart() {
        return thuStart;
    }

    public void setThuStart(String thuStart) {
        this.thuStart = thuStart;
    }

    public String getThuEnd() {
        return thuEnd;
    }

    public void setThuEnd(String thuEnd) {
        this.thuEnd = thuEnd;
    }

    public String getFriStart() {
        return friStart;
    }

    public void setFriStart(String friStart) {
        this.friStart = friStart;
    }

    public String getFriEnd() {
        return friEnd;
    }

    public void setFriEnd(String friEnd) {
        this.friEnd = friEnd;
    }

    public String getSatStart() {
        return satStart;
    }

    public void setSatStart(String satStart) {
        this.satStart = satStart;
    }

    public String getSatEnd() {
        return satEnd;
    }

    public void setSatEnd(String satEnd) {
        this.satEnd = satEnd;
    }

    public String getSunStart() {
        return sunStart;
    }

    public void setSunStart(String sunStart) {
        this.sunStart = sunStart;
    }

    public String getSunEnd() {
        return sunEnd;
    }

    public void setSunEnd(String sunEnd) {
        this.sunEnd = sunEnd;
    }

    public Object getGenStart() {
        return genStart;
    }

    public void setGenStart(Object genStart) {
        this.genStart = genStart;
    }

    public Object getGenEnd() {
        return genEnd;
    }

    public void setGenEnd(Object genEnd) {
        this.genEnd = genEnd;
    }

    public Object getHours() {
        return hours;
    }

    public void setHours(Object hours) {
        this.hours = hours;
    }

    public Object getMinutes() {
        return minutes;
    }

    public void setMinutes(Object minutes) {
        this.minutes = minutes;
    }

    public Object getDuration() {
        return duration;
    }

    public void setDuration(Object duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "ScheduleModel{" +
                "id=" + id +
                ", clinicId=" + clinicId +
                ", consultantId=" + consultantId +
                ", mon=" + mon +
                ", tue=" + tue +
                ", wed=" + wed +
                ", thu=" + thu +
                ", fri=" + fri +
                ", sat=" + sat +
                ", sun=" + sun +
                ", genDay=" + genDay +
                ", monStart='" + monStart + '\'' +
                ", monEnd='" + monEnd + '\'' +
                ", tueStart='" + tueStart + '\'' +
                ", tueEnd='" + tueEnd + '\'' +
                ", wedStart='" + wedStart + '\'' +
                ", wedEnd='" + wedEnd + '\'' +
                ", thuStart='" + thuStart + '\'' +
                ", thuEnd='" + thuEnd + '\'' +
                ", friStart='" + friStart + '\'' +
                ", friEnd='" + friEnd + '\'' +
                ", satStart='" + satStart + '\'' +
                ", satEnd='" + satEnd + '\'' +
                ", sunStart='" + sunStart + '\'' +
                ", sunEnd='" + sunEnd + '\'' +
                ", genStart=" + genStart +
                ", genEnd=" + genEnd +
                ", hours=" + hours +
                ", minutes=" + minutes +
                ", duration=" + duration +
                '}';
    }
}
