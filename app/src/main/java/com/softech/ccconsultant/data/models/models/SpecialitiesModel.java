package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;


public class SpecialitiesModel {

    @SerializedName("OID")
    private String OID;
    @SerializedName("SpecialityDesc")
    private String SpecialityDesc;

    public String getOID() {
        return OID;
    }

    public String getSpecialityDesc() {
        return SpecialityDesc;
    }

    @Override
    public String toString() {
        return SpecialityDesc;
    }

}
