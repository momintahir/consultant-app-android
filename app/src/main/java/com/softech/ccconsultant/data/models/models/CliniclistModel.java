package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Robin.Yaqoob on 19-Dec-17.
 */

public class CliniclistModel {

    @SerializedName("OID")
    private String OID;
    @SerializedName("ClinicName")
    private String ClinicName;
    @SerializedName("DayIntervals")
    private List<DayIntervalsModel> DayIntervals;

    public String getOID() {
        return OID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public List<DayIntervalsModel> getDayIntervals() {
        return DayIntervals;
    }

    @Override
    public String toString() {
        return   ClinicName  ;
    }



}
