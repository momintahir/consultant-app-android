package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;

public class GeneralResponse {

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("response")
	private String response;

	@SerializedName("action")
	private String action;

	public Meta getMeta() {
		return meta;
	}

	public String getResponse() {
		return response;
	}

	public String getAction() {
		return action;
	}

	public static class Meta {

		@SerializedName("code")
		private int code;

		@SerializedName("message")
		private String message;

		public int getCode() {
			return code;
		}

		public String getMessage() {
			return message;
		}

	}
}