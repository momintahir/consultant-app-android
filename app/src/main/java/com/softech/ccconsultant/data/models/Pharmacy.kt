package com.softech.ccconsultant.data.models

import java.io.Serializable

data class Pharmacy(
        val id: Int,
        val pharmacyAddress:String,
        val pharmacyName: String,
        val pharmacyCity: String
) : Serializable {
    override fun toString(): String {
        return pharmacyName
    }
}