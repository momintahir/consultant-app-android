package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 15-Jan-18.
 */

public class CalendarsModel {

    @SerializedName("ID")
    private String ID;
    @SerializedName("CalID")
    private String CalID;
    @SerializedName("ClinicName")
    private String ClinicName;
    @SerializedName("CalendarName")
    private String CalendarName;
    @SerializedName("StartDate")
    private String StartDate;
    @SerializedName("EndDate")
    private String EndDate;
    @SerializedName("ClinicId")
    private String ClinicId;

    public String getID() {
        return ID;
    }

    public String getCalID() {
        return CalID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public String getCalendarName() {
        return CalendarName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public String getClinicId() {
        return ClinicId;
    }

    @Override
    public String toString() {
        return  CalendarName ;
    }
}
