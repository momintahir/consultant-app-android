package com.softech.ccconsultant.data.models.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Robin.Yaqoob on 08-Dec-17.
 */

public class AppointmentsModel {

    @SerializedName("iAppID")
    private String iAppID;
    @SerializedName("iPatID")
    private String iPatID;
    @SerializedName("vAppTimeFrom")
    private String AppTimeFrom;
    @SerializedName("ConsultantID")
    private String ConsultantID;
    @SerializedName("AppStatus")
    private String AppStatus;
    @SerializedName("ClinicID")
    private String ClinicID;
    @SerializedName("Clinic")
    private String ClinicName;
    @SerializedName("DateStr")
    private String DateStr;
    @SerializedName("PatientName")
    private String PatientName;
    @SerializedName("PatientBillNotPaid")
    private String PatientBillNotPaid;
    @SerializedName("SlotStatus")
    private Integer slotStatus;

    @SerializedName("PtPhone")
    private String PtPhone;

    public String getPicAddress() {
        return PicAddress;
    }

    public AppointmentsModel(String iAppID, String iPatID, String appTimeFrom, String consultantID,
                             String appStatus, String clinicID, String clinicName, String dateStr,
                             String patientName, String patientBillNotPaid, Integer slotStatus,
                             String picAddress,String PtPhone) {
        this.iAppID = iAppID;
        this.iPatID = iPatID;
        AppTimeFrom = appTimeFrom;
        ConsultantID = consultantID;
        AppStatus = appStatus;
        ClinicID = clinicID;
        ClinicName = clinicName;
        DateStr = dateStr;
        PatientName = patientName;
        PatientBillNotPaid = patientBillNotPaid;
        this.slotStatus = slotStatus;
        PicAddress = picAddress;
        this.PtPhone=PtPhone;
    }

    @SerializedName("PicAddress")
    private String PicAddress;

    public Integer getSlotStatus() {
        return slotStatus;
    }
    public void setSlotStatus(Integer slotStatus) {
        this.slotStatus = slotStatus;
    }
    public String getiAppID() {
        return iAppID;
    }

    public String getiPatID() {
        return iPatID;
    }

    public String getAppTimeFrom() {
        return AppTimeFrom;
    }

    public String getPtPhone() {
        return PtPhone;
    }

    public String getConsultantID() {
        return ConsultantID;
    }

    public String getAppStatus() {
        return AppStatus;
    }

    public String getClinicID() {
        return ClinicID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public String getDateStr() {
        return DateStr;
    }

    public String getPatientName() {
        return PatientName;
    }

    public String getPatientBillNotPaid() {
        return PatientBillNotPaid;
    }
}
