package com.softech.ccconsultant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_web_view.*
import android.view.View
import com.bumptech.glide.Glide
import java.io.File


class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        //     setSupportActionBar(fileTopbar)
        //  supportActionBar?.title="PDF VIEW"
        val bundle: Bundle? = intent.extras
//        fileWebView.getSettings().setJavaScriptEnabled(true)
//        fileWebView.setWebChromeClient(WebChromeClient())
//       linksBinding.webview.getSettings().setUseWideViewPort(true);
//       linksBinding.webview.getSettings().setLoadWithOverviewMode(true);
        val filePath = bundle?.getString(
                "filePath")
        val filetype = bundle?.getString(
                "filetype")
        if (filetype!!.contains("pdf")) {
            fileWebView.visibility = View.VISIBLE
            fileImageView.visibility=View.GONE
            fileWebView.fromFile(File(filePath))
                    .defaultPage(0)
                    .enableSwipe(true).load()
        }
        else if(filetype!!.contains("jpeg")||filetype.contains("png"))
        {
            fileImageView.visibility=View.VISIBLE
            fileWebView.visibility = View.GONE

            Glide.with(this).load(File(filePath)).into(fileImageView)
        }

//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        supportActionBar!!.setDisplayShowHomeEnabled(true)
//        fileTopbar.setNavigationOnClickListener()
//        {
//            this.finish()
//        }

    }
}
