package com.softech.ccconsultant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SpashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_spash)
        startActivity(Intent(this@SpashActivity, LoginActivity::class.java))
        finish()
    }
}
