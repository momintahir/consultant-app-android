package com.softech.ccconsultant;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.messaging.FirebaseMessaging;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.softech.ccconsultant.fragments.FragmentAppointments;
import com.softech.ccconsultant.fragments.FragmentMyProfile;
import com.softech.ccconsultant.fragments.FragmentSchedule;
import com.softech.ccconsultant.fragments.FragmentSearch;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.CallBacks;
import com.softech.ccconsultant.utils.Dialogues;
import com.softech.ccconsultant.webrtc.util.Constant;
import com.softech.ccconsultant.webrtc.util.D;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class TabsActivity extends AppCompatActivity {

    private static final String TAG = "TabsActivity";

    //private Toolbar toolbar;
    private TabLayout tabLayout;
    ImageView calendarBtn;
    TextView textViewTitle;
    ImageView floatingButton;
    private String fcmToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        initPermissions();
    }

    void initPermissions() {
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                   /* SocketIO.Companion.getInstance().connectSocket(FetchData.getData(TabsActivity.this, "userId"),
                            FetchData.getData(TabsActivity.this, "Name"));
                    SocketIO.Companion.getInstance().connectListeners();*/
                    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/HelveticaNeueLTStd-ThCn.otf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    );
                    initViews();
                    initClicks();

                    SharedPreferences prefs = getSharedPreferences("appData", 0);

                    fcmToken =  prefs.getString(Constant.PUSH_TOKEN, "");
                    if (fcmToken.equals("")) {
                        updateToken();
                    }
                    boolean isNewToken=prefs.getBoolean(Constant.IS_NEW_TOKEN, false);
                    if (isNewToken) {
                        fcmToken = FetchData.getData(TabsActivity.this, Constant.PUSH_TOKEN);
//                        SocketIO.Companion.getInstance().updatePushToken(fcmToken);
                        SharedPreferences sp = getSharedPreferences("appData", 0);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean(Constant.IS_NEW_TOKEN, false);
                        editor.apply();
                    }
                } else {
                    D.Companion.e("Capturing Image", "onPermissionDenied");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();

            }
        }).check();
    }

    public void updateToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            fcmToken = task.getResult();
                            SharedPreferences sp = getSharedPreferences("appData", 0);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString(Constant.PUSH_TOKEN, fcmToken);
                            editor.apply();
                            Log.e("updateToken", "updateToken " + fcmToken);
                        }
                    }else{
                        Log.e("getFCMToken", "getInstance Failed:${task.exception");
                    }
                });
    }

    private void initClicks() {

        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAlertDialog();

            }
        });
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    calendarBtn.setVisibility(View.VISIBLE);

                } else
                    calendarBtn.setVisibility(View.GONE);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(TabsActivity.this, SlotsActivity.class));
            }
        });
    }

    private void initViews() {
        ViewPager viewPager = findViewById(R.id.viewpager);
        floatingButton = findViewById(R.id.floatingButton);
        calendarBtn = findViewById(R.id.calendarBtn);
        textViewTitle = findViewById(R.id.textViewTitle);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
//        builder1.setTitle("Title");
        builder1.setMessage("Please Choose Type");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Choose Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(TabsActivity.this, ConsultantPreferencesActivity.class);
                        intent.putExtra("isCamera", false);
                        startActivity(intent);
                        dialog.cancel();
                    }
                });
        builder1.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(TabsActivity.this, ConsultantPreferencesActivity.class);
                intent.putExtra("isCamera", true);
                startActivity(intent);
                dialogInterface.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentAppointments(), "Appointments");
        adapter.addFragment(new FragmentSearch(), "Patients");
        //  adapter.addFragment(new FragmentSearch(), "");
        //    adapter.addFragment(new DocumentFragment(), "Docs");
        adapter.addFragment(new FragmentSchedule(), "Calendars");
//        adapter.addFragment(new FragmentHistory(), "History");
        adapter.addFragment(new FragmentMyProfile(), "MY Profile");
        //    adapter.addFragment(new FragmentSettings(), "Settings");
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    textViewTitle.setText("Your Appointments");

                } else if (position == 1) {
                    textViewTitle.setText("Your Documents");
                } else if (position == 2) {
                    textViewTitle.setText("Your Calendars");
                } else if (position == 3) {
                    textViewTitle.setText("Your Profile");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentAppointments.timer.cancel();
        moveTaskToBack(true);
        return;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_settings) {

            Dialogues.yesnoDialogue(TabsActivity.this, "Are you sure you want to logout?", new CallBacks() {
                @Override
                public void yes() {
                    SharedPreferences preferences = getSharedPreferences("appData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.commit();
                    Intent intent = new Intent(TabsActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    TabsActivity.this.finish();
                }

                @Override
                public void no() {

                }
            });

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


//        public void onPageSelected(int position) {
//            // Just define a callback method in your fragment and call it like this!
//
//            if (position == 0) {
////                getSupportActionBar().setTitle("Appointments");
//                textViewTitle.setText("Your Appointments");
//
//            } else if (position == 1) {
////                getSupportActionBar().setTitle("Search");
//                textViewTitle.setText("Your Search");
//            } else if (position == 2) {
////                getSupportActionBar().setTitle("Search");
//                textViewTitle.setText("Your Search");
//            } else if (position == 3) {
////                getSupportActionBar().setTitle("My Search");
//                textViewTitle.setText("Your Search");
//            }
//
//        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
