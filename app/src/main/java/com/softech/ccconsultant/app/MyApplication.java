package com.softech.ccconsultant.app;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.LruBitmapCache;
import com.softech.ccconsultant.webrtc.SocketIO;
import com.softech.ccconsultant.webrtc.util.CallsGlobal;
import com.softech.ccconsultant.webrtc.util.D;


public class MyApplication extends MultiDexApplication implements LifecycleObserver {

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static final String TAG = MyApplication.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        CallsGlobal.INSTANCE.setAppInBackground(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (!CallsGlobal.INSTANCE.getKeepSocketConnected()) {
                D.Companion.e("onAppBackgrounded", "onAppBackgrounded");
                SocketIO.INSTANCE.didEnterBackground(FetchData.getData(this, "userId"));
                SocketIO.INSTANCE.disconnectSocket();
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        D.Companion.e("onAppForegrounded", "onAppForegrounded");
        CallsGlobal.INSTANCE.setAppInBackground(false);
        if (SocketIO.INSTANCE.getMSocket() == null) {
            SocketIO.INSTANCE.connectSocket(FetchData.getData(this, "userId"),
                    FetchData.getData(this, "Name"),
                    FetchData.getData(this, "access_token"),
                    getApplicationContext());
//            SocketIO.INSTANCE.connectListeners();
            D.Companion.e("onAppForegrounded", "Connected");
        } else {
            D.Companion.e("onAppForegrounded", "Not Connected");
            if (!SocketIO.INSTANCE.getMSocket().connected()) {
                SocketIO.INSTANCE.connectSocket(FetchData.getData(this, "userId"),
                        FetchData.getData(this, "Name"),
                        FetchData.getData(this, "access_token"),
                        getApplicationContext());
//                SocketIO.INSTANCE.connectListeners();
                D.Companion.e("onAppForegrounded", "Connected after not connected");
            }
        }
        SocketIO.INSTANCE.didEnterForeground(FetchData.getData(this, "userId"));
    }

}
