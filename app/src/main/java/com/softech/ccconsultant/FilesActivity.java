package com.softech.ccconsultant;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.adapters.FilesAdapter;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.models.SelectedDocument.Response;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesActivity extends AppCompatActivity implements IWebListener,FilesAdapter.OndownloadReportViewListener {

    private final String TAG = "FilesActivity";

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String reportUrl;
    FragmentManager fragmentManager;
    List<com.softech.ccconsultant.data.models.models.SelectedDocument.Response> selectedDocumentList;
    FilesAdapter filesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        ButterKnife.bind(this,this);
        callFilesService();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Files");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilesActivity.this.finish();
            }
        });
    }
    public void callFilesService()
    {
        String consultantID = FetchData.getData(this, "userId");
        String access_token = FetchData.getData(this, "access_token");
        String patientID = FetchData.getData(this, "patientID");
        String folderID = FetchData.getData(this, "folderID");


        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Load Selected Folders");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId",patientID);
        params.put("folderId",folderID);
        params.put("stage","yes");


        Log.e(TAG, "Acutal Parameters: "+ params);
        JSONObject jsonObject = new JSONObject(params);
        try {
            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
            Log.e(TAG, "Encrypted String: "+encrypt );
            Map<String, String> finalParams = new HashMap<String, String>();
            finalParams.put("encryptObj", encrypt);
            ApiConnection apiConnection = new ApiConnection(this, this,finalParams, Const.SelectedDocuments);
            apiConnection.makeStringReq();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void success(String response)
    {
        if(response !=null) {

            try {
                Log.e(TAG, "LoadSelectedFolder Response: "+ response);
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.parseInt(code)== 200)
                {
                    String decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV);

                    String message = meta.getString("message");
                    selectedDocumentList = new Gson().fromJson(decryptedString, new TypeToken<List<Response>>(){}.getType());

//                    linearLayout1.setVisibility(View.VISIBLE);
                    filesAdapter = new FilesAdapter(this,selectedDocumentList,this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(filesAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

                }
                else if(Integer.valueOf(code)== 504)
                {
                    String message = meta.getString("message");

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (CheckPermission("Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
                    File inputfile = downloadfile(reportUrl);
                    File    outputfile = downloadfile(reportUrl);

                }

            }
        }
    }
    public static boolean CheckPermission(String Text, String permission, Context mContext, int reqCode) {
        if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {
            reqPermission(permission, mContext, reqCode, Text);
            return false;
        } else {
            return true;
        }
    }

    public static void reqPermission(String permission, final Context mContext, int reqCode, final String Message) {
        if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, permission)) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{permission}, reqCode);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);
            builder.setTitle("Permission");
            builder.setMessage("You needs to Grant Access " + Message + " From Settings >> AppInfo >> Permissions For Using Application");
            builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //
                    dialog.dismiss();
                    String packageName = "softech.assetconnect.js";
                    try {
                        //Open the specific App Info page:
                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + packageName));
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //e.printStackTrace();
                        Toast.makeText(mContext, "You need to Grant Access " + Message + " From Settings >> AppInfo >> Permissions For Using Application", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        }
    }

    public File downloadfile(String urlString) {
        File directory = this.getCacheDir();
        String filename = "fileTest";

        File file = new File(directory, filename);
        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection connection = null;
            try {
                connection = url.openConnection();
                InputStream in = connection.getInputStream();
                FileOutputStream fos = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                while (true) {
                    int len = in.read(buf);
                    if (len == -1) {
                        break;
                    }
                    fos.write(buf, 0, len);
                }
                in.close();
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public void error(String response) {

    }

    @Override
    public File downloadReport(String url)
    {
        reportUrl=url;
        File outputfile=null;
        if (CheckPermission("Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
                outputfile = downloadfile(url);
        }
return outputfile;
    }
}
