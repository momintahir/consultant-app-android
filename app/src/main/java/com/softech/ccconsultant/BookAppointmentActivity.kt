package com.softech.ccconsultant

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.softech.ccconsultant.apiconnections.ApiConnection
import com.softech.ccconsultant.apiconnections.RetrofitApi
import com.softech.ccconsultant.data.models.models.AppointmentsModel
import com.softech.ccconsultant.data.models.models.GeneralResponse
import com.softech.ccconsultant.data.models.models.PatientList.Response
import com.softech.ccconsultant.interfaces.IWebListener
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.utils.AESEncryption
import com.softech.ccconsultant.utils.Const
import com.softech.ccconsultant.utils.Logs
import kotlinx.android.synthetic.main.content_book_appointment.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


class BookAppointmentActivity : AppCompatActivity() {

    private val TAG = "BookAppointmentActivity"

    internal var response: Response? = null
    internal lateinit var selectedDate: String
    internal lateinit var slotStartTime: String
    internal var slotEndTime: String = ""
    internal var slotID: String = ""
    internal var labId: String = ""
    internal var pharmId: String = ""
    internal var radId: String = ""
    internal var filepath: String = ""
    internal var calendarId: String = ""
    internal var hospitalId: String = ""
    internal var consultantID: String = ""
    internal var patientId: String = "0"
    internal var access_token: String = ""
    private var pDialog: ProgressDialog? = null
    private var appointmentsModel: AppointmentsModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointments)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        editTextPhoneInitial.addTextChangedListener(PhoneNumberFormattingTextWatcher())

        supportActionBar!!.title = "Book Appointment"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        pDialog = ProgressDialog(this)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)

        toolbar.setNavigationOnClickListener { this@BookAppointmentActivity.finish() }
        val intent = intent.extras
        if (intent != null) {

            selectedDate = intent.getString("selectedDate", "")
            slotStartTime = intent.getString("slotStartTime", "")
            slotEndTime = intent.getString("slotEndTime", "")
            slotID = intent.getInt("slotID").toString()
            calendarId = intent.getInt("calendarID").toString()
            selectedDate = intent.getString("selectedDate", "")
            hospitalId = intent.getString("hospitalId", "")

            if (selectedDate == "") {
                labId = intent.getString("labId", "")
                pharmId = intent.getString("pharId", "")
                radId = intent.getString("radId", "")
                filepath = intent.getString("filePath", "")
                supportActionBar!!.title = "Search Patient"
                hospitalId = FetchData.getData(this, "clinicId")
                buttonSubmit.text = "Upload Prescription"


            }
            consultantID = FetchData.getData(this@BookAppointmentActivity, "userId")
            access_token = FetchData.getData(this@BookAppointmentActivity, "access_token")
        }

        val bundle = getIntent().extras
        if (bundle != null) {
            var gson = Gson()
            var studentDataObjectAsAString = getIntent().getStringExtra("model")
            appointmentsModel= gson.fromJson(studentDataObjectAsAString, AppointmentsModel::class.java)

            if (appointmentsModel!=null) {
//            appointmentsModel = bundle.getSerializable("model") as AppointmentsModel
                consultantID = bundle.getString("consultantID")!!
                access_token = bundle.getString("accessToken")!!
                hospitalId = bundle.getString("clinicID")!!

                editTextPhoneInitial.setText(appointmentsModel!!.ptPhone)

                val name = appointmentsModel!!.patientName
                var lastName = "";
                var firstName= "";


                    lastName = name.substring(name.lastIndexOf(" ")+1);
                    firstName = name.substring(0, name.lastIndexOf(' '));

                editTextFirstName.setText(firstName)
                editTextLastName.setText(lastName)
            }
        }


        buttonSubmit!!.setOnClickListener()
        {

            if (buttonSubmit.text == "Upload Prescription") {
                uploadPrescription()
            } else {
                if (editTextFirstName!!.text?.length!! > 0 && editTextPhoneInitial.text?.length!! > 0) {
                        if (editTextPhoneInitial.length() == 16) {
                            addAppointmentService()
                        }
                     else {
                        Toast.makeText(this, "Phone No is not in proper format", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_LONG).show()
                }
            }
        }
        searchBtn.setOnClickListener()
        {
            if (editTextPhoneInitial.length() == 16) {
                getpatientList()

            }
            else {
                Toast.makeText(applicationContext, "Please Enter Patient Number in proper format.", Toast.LENGTH_SHORT).show()

            }

        }


        editTextPhoneInitial.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (i == 11) {
                    val input = editTextPhoneInitial.text.toString()
                    val number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)".toRegex(), "(+$1)$2-$3") //(+92)XXX-XXXXXXX
                    println(number)
                    editTextPhoneInitial.setText(number)
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })


//        editTextPhone.addTextChangedListener(object: TextWatcher {
//            override fun beforeTextChanged(charSequence:CharSequence, i:Int, i1:Int, i2:Int) {
//            }
//            override fun onTextChanged(charSequence:CharSequence, i:Int, i1:Int, i2:Int) {
//                if (i == 11)
//                {
//                    val input = editTextPhone.getText().toString()
//                    val number = input.replaceFirst(("(\\d{2})(\\d{3})(\\d+)").toRegex(), "(+$1)$2-$3")//(+92)XXX-XXXXXXX
//                    println(number)
//                    editTextPhone.setText(number)
//                }
//            }
//            override fun afterTextChanged(editable: Editable) {
//            }
//        })
    }

    fun uploadPrescription() {
        Log.e(TAG, "uploadPrescription: " )

        if (!pDialog?.isShowing!!) {
            pDialog?.show()
        }
        var gender = "1"

        gender = if (spinnerGender!!.selectedItem.toString().equals("male", ignoreCase = true)) {
            "1"
        } else {
            "2"
        }
        val file = File(filepath)
        val mimetype = FetchData.getMimeType(file.path)
        val consultantID = FetchData.getData(this, "userId")
        val access_token = FetchData.getData(this, "access_token")
        val patientID = FetchData.getData(this, "patientID")
        val folderID = FetchData.getData(this, "folderID")
        val params = HashMap<String, String>()
        params["action"] = "Upload Prescription"
        params["ConID"] = consultantID
        params["access_token"] = access_token
        params["PatID"] = patientId
        params["ClinicID"] = hospitalId
        params["PathLabID"] = labId
        params["RadLabID"] = radId
        params["PharmacyID"] = pharmId
        params["patFirstName"] = editTextFirstName!!.text.toString()
        params["patLastName"] = editTextLastName!!.text.toString()
        params["patPhoneNumber"] = editTextPhoneInitial!!.text.toString()
        params["patGender"] = gender

        Log.e(TAG, "Encrypted Object Params: $params")

        val jsonObject = JSONObject(params as Map<*, *>)
        val encryptString = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)

        Log.e(TAG, "Encrypted Object: $encryptString")

//        Ion.with(this).load(Const.BASE_URL + Const.UPLOAD_PRESCRIPTION)
//                .setMultipartParameter("encryptObj", encryptString)
//                .setMultipartFile("File", mimetype, file)
//                .asString()
//                .setCallback(FutureCallback<String> { e, result ->
//                    try {
//                        if (pDialog?.isShowing()!!) {
//                            pDialog?.dismiss()
//                        }
//                        val jsonObject = JSONObject(result)
//                        val meta = jsonObject.getJSONObject("meta")
//                        val code = meta.getString("code")
//                        if (Integer.valueOf(code) == 200) {
//                            val message = meta.getString("message")
//                            Toast.makeText(this@BookAppointmentActivity, message, Toast.LENGTH_LONG).show()
//                            this.finish()
//                        } else {
//                            val message = jsonObject.getString("error")
//                            Toast.makeText(this@BookAppointmentActivity, message, Toast.LENGTH_LONG).show()
//                            //  recyclViewSearch.setVisibility(View.GONE);
//                        }
//                    } catch (e1: JSONException) {
//                        e1.printStackTrace()
//                    }
//                })

        try {
            // add another part within the multipart request
            val requestEncryptedString: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), encryptString)
            val requestFile: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
            val imageFile = MultipartBody.Part.createFormData("File", file.name, requestFile)
            RetrofitApi.getService(Const.BASE_URL)
                .uploadPrescription(requestEncryptedString, imageFile)
                .enqueue(object : Callback<GeneralResponse?> {
                    override fun onResponse(
                        call: Call<GeneralResponse?>,
                        response: retrofit2.Response<GeneralResponse?>
                    ) {
                        if (pDialog!!.isShowing) {
                            pDialog!!.dismiss()
                        }
                        if (response.isSuccessful && response.body() != null) {
                            if (response.body()!!.meta.code == 200) {
                                Toast.makeText(this@BookAppointmentActivity, "File uploaded...", Toast.LENGTH_SHORT).show()
                                startActivity(Intent(this@BookAppointmentActivity, TabsActivity::class.java))
                                finishAffinity()
                            }
                        }

                    }

                    override fun onFailure(call: Call<GeneralResponse?>, t: Throwable) {
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun uploadClaimForm() {


    }

    private fun getpatientList() {
        val consultantID = FetchData.getData(this!!, "userId")
        val access_token = FetchData.getData(this!!, "access_token")
        var number=editTextPhoneInitial!!.text.toString()
        val params = HashMap<String, String>()
        params["action"] = "Search Patient Result"
        params["consultantId"] = consultantID
        params["access_token"] = access_token
        params["contactNo"] =  number
        params["clinicId"] = hospitalId

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val jsonResponse = jsonObject.getJSONArray("response")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<Response>>() {

                            }.type
                            val searchedPatientList = gson.fromJson<List<Response>>(jsonResponse.toString(), type)
                            Logs.showLog("Login", response)
                            if (searchedPatientList.size > 0) {
                                var patienNameArrayList: ArrayList<String> = ArrayList()
                                for (patList in searchedPatientList) {
                                    patienNameArrayList.add(patList.patient + " - "+patList.mobile)

                                }
                                showOptionsMenu(patienNameArrayList, searchedPatientList)
//                                val adapter=ArrayAdapter<String>(this@BookAppointmentActivity,R.layout.spinner,patienNameArrayList)
//                                adapter.setDropDownViewResource(R.layout.spinner)
//                                spinnerGender.setAdapter(adapter)
//                                spinnerGender.setPrompt("Select Patient")
                            }

                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Patient List", response)
                }
            }

            override fun error(response: String) {
                Log.v("Error Add Appointment", "")

            }
        }, this@BookAppointmentActivity, params,
                Const.SEARCH)
        apiConnection.makeStringReq()
    }

    private fun showOptionsMenu(stringArray: ArrayList<String>, fullPatientList: List<Response>) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Choose Patient")
        val adapter = ArrayAdapter<String>(this@BookAppointmentActivity, R.layout.spinner, stringArray)
        alert.setAdapter(adapter, DialogInterface.OnClickListener { dialog, which ->
            val strName = adapter.getItem(which)
            editTextFirstName.setText(fullPatientList.get(which).firstName)
            editTextLastName.setText(fullPatientList.get(which).lastName)
//            editTextPhone.setText(fullPatientList.get(which).mobile)
            patientId = fullPatientList.get(which).patId.toString()
            if (fullPatientList.get(which).gender.equals("Male")) {
                spinnerGender.setSelection(0)
            } else {
                spinnerGender.setSelection(1)
            }
        })
        alert.setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
        alert.show()
    }

    private fun addAppointmentService() {
        Log.e(TAG, "addAppointmentService: ")
        val params = HashMap<String, String>()

        if (appointmentsModel!=null){
            var gender = "1"
            gender = if (spinnerGender!!.selectedItem.toString().equals("male", ignoreCase = true)) {
                "1"
            } else {
                "2"
            }
            params["action"] = "Add Appointment"
            params["consultantId"] = consultantID
            params["access_token"] = access_token
            params["clincID"] = hospitalId

            params["date"] = selectedDate
            params["TimeStart"] = slotStartTime
            params["TimeEnd"] = slotEndTime
            params["SlotID"] = slotID
            params["patId"] = appointmentsModel!!.getiPatID()
            params["CalendarID"] = calendarId
            params["patFirstName"] = editTextFirstName!!.text.toString()
            params["patLastName"] = editTextLastName!!.text.toString()
            params["patPhoneNumber"] = editTextPhoneInitial!!.text.toString()
            params["patGender"] = gender
            params["notes"] = ""
            params["appId"] = appointmentsModel!!.getiAppID()
            params["rescheduleCheck"] = "true"

        }

        else {
            var gender = "1"

            gender = if (spinnerGender!!.selectedItem.toString().equals("male", ignoreCase = true)) {
                "1"
            } else {
                "2"
            }
            params["action"] = "Add Appointment"
            params["consultantId"] = consultantID
            params["access_token"] = access_token
            params["clincID"] = hospitalId

            params["date"] = selectedDate
            params["TimeStart"] = slotStartTime
            params["TimeEnd"] = slotEndTime
            params["SlotID"] = slotID
            params["patId"] = patientId
            params["CalendarID"] = calendarId
            params["patFirstName"] = editTextFirstName!!.text.toString()
            params["patLastName"] = editTextLastName!!.text.toString()
            params["patPhoneNumber"] = editTextPhoneInitial!!.text.toString()
            params["patGender"] = gender
            params["notes"] = ""
            params["appId"] = "0"
            params["rescheduleCheck"] = "false"

        }

        Log.e(TAG, "Calling bookAppointment Service: " )
        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {
                Log.e(TAG, "Book Appointment Response: $response")
                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val response1 = jsonObject.getString("response")
                            Toast.makeText(applicationContext, response1, Toast.LENGTH_SHORT).show()

                            val intent = Intent(this@BookAppointmentActivity, TabsActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                            finish()
                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Book an Appointment", response)
                }
            }
            override fun error(response: String) {
                Log.e(TAG, "Error Response: $response" )
            }
        }, this@BookAppointmentActivity, params, Const.ACTION_BOOK_APPOINTMENT)
        apiConnection.makeStringReq()
    }
}
