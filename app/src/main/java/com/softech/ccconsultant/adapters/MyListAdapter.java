package com.softech.ccconsultant.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.softech.ccconsultant.R;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private ArrayList<String> listdata;
    private ArrayList<String> hours;
    private ArrayList<String> minutes;
    Context context;
    public MyListAdapter(Context context,ArrayList<String> listdata,ArrayList<String> hours,
                         ArrayList<String> minutes) {
        this.listdata = listdata;
        this.context=context;
        this.hours=hours;
        this.minutes=minutes;



    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_schedule, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        ArrayAdapter hour = new ArrayAdapter(context,android.R.layout.simple_spinner_item,hours);
        hour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        viewHolder.spinnerStartHour.setAdapter(hour);


        ArrayAdapter min = new ArrayAdapter(context,android.R.layout.simple_spinner_item,minutes);
        min.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        viewHolder.spinnerStartMin.setAdapter(min);



        ArrayAdapter hourEnd = new ArrayAdapter(context,android.R.layout.simple_spinner_item,hours);
        hourEnd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        viewHolder.spinnerEndHour.setAdapter(hourEnd);


        ArrayAdapter minEnd = new ArrayAdapter(context,android.R.layout.simple_spinner_item,minutes);
        minEnd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        viewHolder.spinnerEndMin.setAdapter(minEnd);





        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String myListData = listdata.get(position);
        holder.textView.setText(myListData);


        holder.spinnerStartHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                Toast.makeText(context, position +" " + pos, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public AppCompatSpinner spinnerStartHour,spinnerEndHour;
        public AppCompatSpinner spinnerStartMin,spinnerEndMin;
        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.text);
            this.spinnerStartHour=itemView.findViewById(R.id.spinnerStartHour);
            this.spinnerStartMin=itemView.findViewById(R.id.spinnerStartMin);
            this.spinnerEndHour=itemView.findViewById(R.id.spinnerEndHour);
            this.spinnerEndMin=itemView.findViewById(R.id.spinnerEndMin);



        }
    }
}
