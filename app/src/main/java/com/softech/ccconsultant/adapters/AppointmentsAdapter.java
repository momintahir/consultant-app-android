package com.softech.ccconsultant.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softech.ccconsultant.DocumentActivity;
import com.softech.ccconsultant.PatientProfileActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.data.models.models.AppointmentsModel;
import com.softech.ccconsultant.data.models.models.PatientStatus;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Robin.Yaqoob on 30-Nov-17.
 */

public class AppointmentsAdapter extends RecyclerView.Adapter<AppointmentsAdapter.MyViewHolder> {

    private List<AppointmentsModel> appointmentsModelList;
    private List<PatientStatus> patientStatusList;
    private Activity activity;
    OnDeleteAptClickListenter listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewClinic, textViewDate, textViewTime, textViewPhone, textViewPatientName;
        public ImageView docBtn, imageViewProfile, deleteBtn, ivVideoCall, ivAudioCall,green_switch,red_switch,rescheduleBtn;
        CircleImageView imageViewPatient;
        int position;

        public MyViewHolder(View view, OnDeleteAptClickListenter listenter) {
            super(view);

            textViewPatientName = (TextView) view.findViewById(R.id.textViewPatientName);
            textViewClinic = (TextView) view.findViewById(R.id.textViewClinic);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);
            textViewTime = (TextView) view.findViewById(R.id.textViewTime);
            textViewPhone = (TextView) view.findViewById(R.id.textViewPhone);
            docBtn = (ImageView) view.findViewById(R.id.docsBtn);
            green_switch=view.findViewById(R.id.green_switch);
            red_switch=view.findViewById(R.id.red_switch);
            imageViewPatient = view.findViewById(R.id.imageViewPatient);
            imageViewProfile = (ImageView) view.findViewById(R.id.imageViewProfile);
            deleteBtn = (ImageView) view.findViewById(R.id.deleteButton);
            ivAudioCall = (ImageView) view.findViewById(R.id.ivAudioCall);
            ivVideoCall = (ImageView) view.findViewById(R.id.ivVideoCall);
            rescheduleBtn=view.findViewById(R.id.rescheduleBtn);
            try {
                if (appointmentsModelList!=null)
                deleteBtn.setOnClickListener(v ->

                        listener.OnDeleteAptClick(v, appointmentsModelList.get(position)));

            }
            catch (NullPointerException e){
                e.printStackTrace();
            }

            ivAudioCall.setOnClickListener(v -> listener.OnDeleteAptClick(v, appointmentsModelList.get(position)));

            ivVideoCall.setOnClickListener(v -> listener.OnDeleteAptClick(v, appointmentsModelList.get(position)));

        }
    }


    public interface OnDeleteAptClickListenter {
        void OnDeleteAptClick(View caller, AppointmentsModel mItem);
    }

    public AppointmentsAdapter(List<AppointmentsModel> appointmentsModelList, Activity activity, OnDeleteAptClickListenter listener) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
        this.listener = listener;
    }

    public AppointmentsAdapter(List<AppointmentsModel> appointmentsModelList, Activity activity) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
    }

    public AppointmentsAdapter(List<PatientStatus> patientStatusList,List<AppointmentsModel> appointmentsModelList,
                               Activity activity,OnDeleteAptClickListenter listener) {
        this.patientStatusList = patientStatusList;
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
        this.listener = listener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_appointments, parent, false);

        return new MyViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.position = position;
        final AppointmentsModel clinicsModels = appointmentsModelList.get(position);
        holder.textViewPatientName.setText(clinicsModels.getPatientName());
        holder.textViewClinic.setText(clinicsModels.getClinicName());
        holder.textViewDate.setText(clinicsModels.getDateStr());
        holder.textViewTime.setText(clinicsModels.getAppTimeFrom());
        if (clinicsModels.getSlotStatus() != null && clinicsModels.getSlotStatus() == 1) {
            holder.deleteBtn.setVisibility(View.VISIBLE);

        } else {
            holder.deleteBtn.setVisibility(View.GONE);

        }
//        Picasso.with(activity)
//                .load(clinicsModels.getPicAddress())
//                .placeholder(R.mipmap.pic)
//                .into(holder.imageViewPatient);


                Glide
                .with(activity)
                .load(clinicsModels.getPicAddress())
                .centerCrop()
                .placeholder(R.mipmap.pic)
                .into(holder.imageViewPatient);

        if (patientStatusList!=null && appointmentsModelList!=null){

            try {
                if (patientStatusList.get(position).getiPatID().equals(appointmentsModelList.get(position).getiPatID())){
                    if (patientStatusList.get(position).getIsOnline().equals("0")){
                        holder.red_switch.setVisibility(View.VISIBLE);
                        holder.green_switch.setVisibility(View.GONE);

                    }
                    else {
                        holder.green_switch.setVisibility(View.VISIBLE);
                        holder.red_switch.setVisibility(View.GONE);
                    }

                }
            }
            catch (Exception e){
                e.printStackTrace();
            }


        }


        holder.docBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SaveData.SaveData(activity, "patientID", clinicsModels.getiPatID());
                    activity.startActivity(new Intent(activity, DocumentActivity.class));

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        holder.imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    activity.startActivity(new Intent(activity, PatientProfileActivity.class).putExtra("patientId", clinicsModels.getiPatID()));

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


        holder.rescheduleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appointmentsModelList!=null) {

                    try {
                        listener.OnDeleteAptClick(v, appointmentsModelList.get(position));

                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    //    }
    @Override
    public int getItemCount() {
        return appointmentsModelList.size();
    }
}