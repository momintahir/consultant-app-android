package com.softech.ccconsultant.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softech.ccconsultant.DocumentActivity;
import com.softech.ccconsultant.PatientProfileActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.data.models.models.SearchAppointmentModel;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.squareup.picasso.Picasso;

import java.util.List;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private List<SearchAppointmentModel> appointmentsModelList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewClinic, textViewPatientName,textViewPhone, textViewDate, textViewTime;
        public ImageView docBtn, imageViewProfile;
        ImageView imageViewPatient,deletBtn;
        public LinearLayout parentView;

        public MyViewHolder(View view) {
            super(view);

            textViewClinic = (TextView) view.findViewById(R.id.textViewClinic);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);
            textViewTime = (TextView) view.findViewById(R.id.textViewTime);
            textViewPhone = (TextView) view.findViewById(R.id.textViewPhone);
            textViewPatientName = (TextView) view.findViewById(R.id.textViewPatientName);
            imageViewPatient = (ImageView) view.findViewById(R.id.imageViewPatient);
            docBtn = (ImageView) view.findViewById(R.id.docsBtn);
            deletBtn = (ImageView) view.findViewById(R.id.deleteButton);
            parentView = (LinearLayout) view.findViewById(R.id.parentView);

            imageViewProfile = (ImageView) view.findViewById(R.id.imageViewProfile);
        }
    }


    public SearchAdapter(List<SearchAppointmentModel> appointmentsModelList, Activity activity) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_patients, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.deletBtn.setVisibility(View.GONE);
        if (position % 2 == 0) {
            holder.parentView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.parentView.setBackgroundColor(Color.parseColor("#f6f2f1"));
        }
        final SearchAppointmentModel clinicsModels = appointmentsModelList.get(position);

        holder.textViewPatientName.setText(clinicsModels.getFirstName() + " " + clinicsModels.getLastName());
      //  holder.textViewDate.setText(clinicsModels.getAppDate());
        holder.textViewPhone.setText(clinicsModels.getMobile());
        holder.textViewDate.setText(clinicsModels.getCity());
        String url = "https://www.healthcarepakistan.pk/Patient_training/Content/patientImages/" + clinicsModels.getPatId() + ".png";
//        Picasso.with(activity)
//                .load(clinicsModels.getPicAddress())
//                .placeholder(R.mipmap.pic)
//                .into(holder.imageViewPatient);

        Glide
                .with(activity)
                .load(clinicsModels.getPicAddress())
                .centerCrop()
                .placeholder(R.mipmap.pic)
                .into(holder.imageViewPatient);

        holder.docBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.SaveData(activity, "patientID", clinicsModels.getPatId());
                activity.startActivity(new Intent(activity, DocumentActivity.class));

            }
        });

        holder.imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.startActivity(new Intent(activity, PatientProfileActivity.class).putExtra("patientId", clinicsModels.getPatId()));
            }
        });
    }

    //    public void updateList(ArrayList<ClinicsModels> list){
//        appointmentsModelList = list;
//        notifyDataSetChanged();
//    }
    @Override
    public int getItemCount() {
        return appointmentsModelList.size();
    }
}