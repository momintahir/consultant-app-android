package com.softech.ccconsultant.adapters

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.softech.ccconsultant.R
import com.softech.ccconsultant.data.models.ConsultantPreference

class PrescriptionAdapter(val consultantprefList: MutableList<ConsultantPreference>?, val activity: Activity, val listener: OnPrescriptionClickListener) :
        RecyclerView.Adapter<ViewHolder>() {


    interface OnPrescriptionClickListener {
        fun OnPrescriptionClick(prescriptionId: Int,position: Int)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0?.context).inflate(R.layout.list_item_consultantpreferences, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return consultantprefList!!.size
    }

    override fun onBindViewHolder(viewholder: ViewHolder, position: Int) {
        val consutlantPreference: ConsultantPreference? = consultantprefList?.get(position)

        when (position) {
            0 -> viewholder.preferencTitle.text = "Pharmacies"
            1 -> viewholder.preferencTitle.text = "Laboratory"
            else -> viewholder.preferencTitle.text = "Radiology"
        }
        viewholder.preferenceName.text = consutlantPreference?.Name
        viewholder.mView.setOnClickListener()
        {
            if (viewholder.selectPrescImg.visibility == View.GONE) {
                viewholder.selectPrescImg.visibility = View.VISIBLE


            } else {
                viewholder.selectPrescImg.visibility = View.GONE
            }
            if (listener != null) {
                listener.OnPrescriptionClick(consutlantPreference!!.Oid,position)
            }
        }
    }
}

class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
    val selectPrescImg = itemview.findViewById<ImageView>(R.id.selectPrescImg)
    val preferencTitle = itemview.findViewById<TextView>(R.id.preferencTitle)
    val preferenceName = itemview.findViewById<TextView>(R.id.preferenceName)
    val mView = itemview.findViewById<LinearLayout>(R.id.mView)

}