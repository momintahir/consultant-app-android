package com.softech.ccconsultant.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.softech.ccconsultant.R;
import com.softech.ccconsultant.data.models.models.ClinicsModels;
import com.softech.ccconsultant.interfaces.Callbacks;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Robin.Yaqoob on 30-Nov-17.
 */

public class ClinicsAdapter extends RecyclerView.Adapter<ClinicsAdapter.MyViewHolder> {

    private List<ClinicsModels> clinicsModelsList;
    int lastposition = -1;
    private Callbacks callbacks;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewClinic;
        public CheckBox checkBoxClinic;

        public MyViewHolder(View view) {
            super(view);

            textViewClinic = (TextView) view.findViewById(R.id.textViewClinic);
            checkBoxClinic= (CheckBox) view.findViewById(R.id.checkBoxClinic);
        }
    }


    public ClinicsAdapter(List<ClinicsModels> clinicsModelsList, Callbacks callbacks) {
        this.clinicsModelsList = clinicsModelsList;
        this.callbacks = callbacks;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_clinics, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ClinicsModels clinicsModels = clinicsModelsList.get(position);
        holder.textViewClinic.setText(clinicsModels.getClinicName());

        holder.checkBoxClinic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                lastposition = position;
                ClinicsModels clinicsModels1 = clinicsModelsList.get(position);
                callbacks.data(clinicsModels1.getOID());

                if(lastposition !=position)
                {

                }


            }
        });
    }
    public void updateList(ArrayList<ClinicsModels> list){
        clinicsModelsList = list;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return clinicsModelsList.size();
    }
}