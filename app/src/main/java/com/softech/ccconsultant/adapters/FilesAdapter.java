package com.softech.ccconsultant.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softech.ccconsultant.R;
import com.softech.ccconsultant.WebViewActivity;
import com.softech.ccconsultant.data.models.models.SelectedDocument.Response;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by saimshafqat on 02/08/2018.
 */

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {

    private final List<Response> mValues;
    private Activity mContext;
    LayoutInflater mLayoutInflater;
    OndownloadReportViewListener listener;

    public FilesAdapter(Activity context, List<Response> items, OndownloadReportViewListener listener) {
        mValues = items;
        this.listener = listener;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public interface OndownloadReportViewListener {
        public File downloadReport(String report);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.titlelabel.setText(mValues.get(position).getFILENAME());
        if (mValues.get(position).getFILETYPE().equals("image/jpeg") || mValues.get(position).getFILETYPE().equals("image/jpg")) {

            holder.documentImageView.setImageResource(R.drawable.jpg);
        } else if (mValues.get(position).getFILETYPE().equals("application/pdf")) {
            holder.documentImageView.setImageResource(R.drawable.jpg);

        } else if (mValues.get(position).getFILETYPE().equals("text/plain")) {
            holder.documentImageView.setImageResource(R.drawable.txt);

        } else if (mValues.get(position).getFILETYPE().equals("application/msword")
                || mValues.get(position).getFILETYPE()
                .equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            holder.documentImageView.setImageResource(R.drawable.doc);

        } else {
            holder.documentImageView.setImageResource(R.drawable.empty);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                String decryptedString = AESEncryption.decrypt(mValues.get(position).getFILEPATH(), Const.Encryption_Key, Const.Encryption_IV);
                SaveData.SaveData(mContext, "webLink", decryptedString);
                // File inputfile=downloadfile(decryptedString);
                // File outputfile=downloadfile(decryptedString);

                File inputfile = null;
                File outputfile = null;
              //  byte[] decodedBytes = Base64.getDecoder().decode(mValues.get(position).getFile());
                byte[] decodedBytes= Base64.decode(mValues.get(position).getFile(),1);
                inputfile = getfileFromBase64String(decodedBytes, mValues.get(position).getFILETYPE());
                outputfile = getfileFromBase64String(decodedBytes, mValues.get(position).getFILETYPE());
                AESEncryption.encryptFile(2, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputfile);
                String filetype = mValues.get(position).getFILETYPE();
                if (filetype.equals("image/jpeg") || filetype.equals("image/png") ||
                        filetype.equals("image/jpg") || filetype.equals("application/pdf")) {
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("filePath", outputfile.getAbsolutePath());
                    intent.putExtra("filetype", mValues.get(position).getFILETYPE());
                    mContext.startActivity(intent);
                } else {
                    Intent actionIntent = new Intent(Intent.ACTION_VIEW);
                    if (mValues.get(position).getFILETYPE().contains(".xls") || mValues.get(position).getFILETYPE().contains(".xlsx")) {
                        actionIntent.setDataAndType(Uri.fromFile(inputfile), "application/vnd.ms-excel");
                    } else
                        actionIntent.setDataAndType(Uri.fromFile(inputfile), mValues.get(position).getFILETYPE());
                    mContext.startActivity(actionIntent);
                }
            }
        });
    }

    public File getfileFromBase64String(byte[] dataBytes, String filetype) {
        File file = null;

        try {
            filetype = filetype.substring(filetype.length() - 3);

            if (filetype.equals("peg")) {
                filetype = "jpg";
            }
            File directory = mContext.getCacheDir();
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            String filename = "ReportTest_" + ts + "." + filetype;

            file = new File(directory, filename);

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(dataBytes);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

//    public interface OnListInOnListInteractionListenerteractionListener {
//        void onListInteraction(String url);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView titlelabel;


        ImageView documentImageView;
        String URL;
        String mitem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            titlelabel = (TextView) view.findViewById(R.id.documentNameLabel);
            documentImageView = (ImageView) view.findViewById(R.id.docPicture);
        }
    }
}