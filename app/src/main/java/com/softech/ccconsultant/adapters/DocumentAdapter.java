package com.softech.ccconsultant.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softech.ccconsultant.FilesActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.data.models.models.DocumentFolder.Response;
import com.softech.ccconsultant.sharedpreferences.SaveData;

import java.util.List;



public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    private final List<Response> mValues;
    Activity mContext;
    LayoutInflater mLayoutInflater;
    boolean mIsselectedDocument;
    int selectedIndex = -1;

    public DocumentAdapter(Activity context, List<Response> items, boolean isSelectedDocument) {
        mValues = items;
        mIsselectedDocument = isSelectedDocument;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (mIsselectedDocument) {
            if (selectedIndex == position) {
                holder.correctImg.setVisibility(View.VISIBLE);
            } else {
                holder.correctImg.setVisibility(View.GONE);
            }
            holder.documentImageView.setVisibility(View.GONE);
            holder.folderImg.setVisibility(View.VISIBLE);
        } else {
            holder.documentImageView.setVisibility(View.VISIBLE);
            holder.folderImg.setVisibility(View.GONE);
            holder.correctImg.setVisibility(View.GONE);

        }

        holder.titlelabel.setText(mValues.get(position).getFOLDERNAME());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsselectedDocument) {
                    selectedIndex = position;
                    notifyDataSetChanged();
                    SaveData.SaveData(mContext, "folderID", mValues.get(position).getOID().toString());
                    return;
                }
                SaveData.SaveData(mContext, "folderID", mValues.get(position).getOID().toString());
                mContext.startActivity(new Intent(mContext, FilesActivity.class));
            }
        });
        // holder.URL=mValues.get(position).getUrl();
//         Glide.with(mContext)
//                .load(mValues.get(position).getUrl()).asBitmap().placeholder(R.drawable.image_placeholder_post).error(R.drawable.image_placeholder_post)
//                .into(holder.circleImageView);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface OnListInteractionListener {
        void onListInteraction(String url);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView titlelabel;


        ImageView documentImageView, correctImg, folderImg;
        String URL;
        String mitem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            titlelabel = (TextView) view.findViewById(R.id.documentNameLabel);
            documentImageView = (ImageView) view.findViewById(R.id.docPicture);
            correctImg = (ImageView) view.findViewById(R.id.correctImg);
            folderImg = (ImageView) view.findViewById(R.id.folderImg);

        }
    }
}
