package com.softech.ccconsultant.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.softech.ccconsultant.BookAppointmentActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.data.models.models.AppointmentsModel;
import com.softech.ccconsultant.data.models.models.SlotsData.Response;

import java.util.List;

/**
 * Created by saimshafqat on 31/01/2019.
 */
public class SlotsAdapter extends RecyclerView.Adapter<SlotsAdapter.ViewHolder> {
    Activity mContext;
    LayoutInflater mLayoutInflater;
    List<Response> mitem;
    String clinicID, selectedDate,consultantID,accessToken,hospitalId;
    AppointmentsModel appointmentsModel;

    public SlotsAdapter(Activity context, List<Response> items, String clinicID,
                        String selectedDate,AppointmentsModel appointmentsModel,String consultantID,
                        String accessToken,String hospitalId) {

        this.mContext = context;
        this.mitem = items;
        this.clinicID = clinicID;
        this.selectedDate = selectedDate;
        this.appointmentsModel=appointmentsModel;
        this.consultantID=consultantID;
        this.accessToken=accessToken;
        this.hospitalId=hospitalId;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_slots, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.slotText.setText(mitem.get(position).getSlotTime());


//        if (mitem.get(position).getAppID() == -1) {
//            if (mitem.get(position).getIsDisabled()==0) {
//                holder.slotText.setEnabled(true);
//                holder.slotText.setBackgroundResource(R.color.selectGreen);
//            }
//            else
//                {
//                    holder.slotText.setEnabled(false);
//                    holder.slotText.setBackgroundResource(R.color.lightgrey);
//                }
//        } else {
//            holder.slotText.setEnabled(false);
//            holder.slotText.setBackgroundResource(R.color.lightgrey);
//        }

        Integer isAllowed=mitem.get(position).getIsAllowed();
        Integer isDisabled=mitem.get(position).getIsDisabled();
        Integer isDel=mitem.get(position).getIsDel();
        Integer appId=mitem.get(position).getAppID();


        assert isAllowed != null;
        assert isDel != null;
        if (isAllowed==1 && isDisabled==0 && isDel==0 && appId==0){
            holder.slotText.setEnabled(true);
            holder.slotText.setBackgroundResource(R.color.selectGreen);
        }
        else {
            holder.slotText.setEnabled(false);
            holder.slotText.setBackgroundResource(R.color.lightgrey);
        }



        holder.slotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAppointmentActivity(mitem.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mitem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView slotText;


        ImageView documentImageView;
        String URL;
        String mitem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            slotText = (TextView) view.findViewById(R.id.slotText);
        }
    }

    private void openAppointmentActivity(Response slotsData) {
        Intent intent = new Intent(mContext, BookAppointmentActivity.class);
        intent.putExtra("selectedDate", selectedDate);
        intent.putExtra("hospitalId", clinicID);
        intent.putExtra("slotStartTime", slotsData.getSlotTime());
        intent.putExtra("slotEndTime", slotsData.getSlotEndTime());
        intent.putExtra("slotID", slotsData.getISlotID());
        intent.putExtra("calendarID", slotsData.getICalendarID());
        if (appointmentsModel!=null){
            intent.putExtra("consultantID",consultantID);
            intent.putExtra("accessToken",accessToken);
            intent.putExtra("clinicID",clinicID);
            Gson gson = new Gson();
            String modell = gson.toJson(appointmentsModel);
            intent.putExtra("model",modell);
        }
        mContext.startActivity(intent);
    }
}
