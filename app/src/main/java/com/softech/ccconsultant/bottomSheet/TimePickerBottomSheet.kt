package com.softech.ccconsultant.bottomSheet

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.softech.ccconsultant.R
import com.softech.ccconsultant.utils.Util


class TimePickerBottomSheet(
    private val onTimePickerControlsListener: OnTimePickerControlsListener,
    private val btnStartTime: AppCompatButton,
    private val btnEndTime: AppCompatButton,
    private val checkBox: CheckBox
) : BottomSheetDialogFragment(){


    private val TAG = "TimePickerBottomSheet"

    lateinit var tvCancel:TextView
    lateinit var timePickerStart:TimePicker
    lateinit var timePickerEnd:TimePicker
    lateinit var btnDone:AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogStyle)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        // This listener's onShow is fired when the dialog is shown
        dialog.setOnShowListener { dialog -> // In a previous life I used this method to get handles to the positive and negative buttons
            // of a dialog in order to change their Typeface. Good ol' days.
            val d = dialog as BottomSheetDialog

            // This is gotten directly from the source of BottomSheetDialog
            // in the wrapInBottomSheet() method
            val bottomSheet = (d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)!!
            BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.time_picker_bottom_sheet_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    private fun initViews(view: View) {
        tvCancel=view.findViewById(R.id.tvCancel)
        timePickerStart=view.findViewById(R.id.timePickerStart)
        timePickerStart.setIs24HourView(true)

        timePickerEnd=view.findViewById(R.id.timePickerEnd)
        timePickerEnd.setIs24HourView(true)

        btnDone=view.findViewById(R.id.btnDone)

        tvCancel.setOnClickListener{
            onTimePickerControlsListener.onTimePickerCancelled(checkBox)
            dismiss()
        }

        btnDone.setOnClickListener{
            val startHour = Util.checkDigit(timePickerStart.hour)
            val startMinute = Util.checkDigit(timePickerStart.minute)
            val endHour = Util.checkDigit(timePickerEnd.hour)
            val endMinute = Util.checkDigit(timePickerEnd.minute)
            onTimePickerControlsListener.onTimeSelected("$startHour:$startMinute", "$endHour:$endMinute", btnStartTime, btnEndTime, checkBox)
            dismiss()
        }
    }

    interface OnTimePickerControlsListener{
        fun onTimeSelected(startTime: String, endTime: String, btnStartTime: AppCompatButton, btnEndTime: AppCompatButton, checkBox: CheckBox)
        fun onTimePickerCancelled(checkBox: CheckBox)
    }

}