package com.softech.ccconsultant.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.ChangePasswordActivity;
import com.softech.ccconsultant.LoginActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.apiconnections.ApiConnectionBackground;
import com.softech.ccconsultant.apiconnections.RetrofitApi;
import com.softech.ccconsultant.data.models.Lab;
import com.softech.ccconsultant.data.models.LabsList;
import com.softech.ccconsultant.data.models.Pharmacy;
import com.softech.ccconsultant.data.models.models.GeneralResponse;
import com.softech.ccconsultant.data.models.models.SpecialitiesModel;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.CallBacks;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Dialogues;
import com.softech.ccconsultant.utils.Logs;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyProfile extends Fragment {

    private static final String TAG = "FragmentMyProfile";

    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textViewDOB)
    TextView textViewDOB;
    @BindView(R.id.editTextQualification)
    EditText editTextQualification;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextFirstName)
    EditText editTextFirstName;
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;
    @BindView(R.id.buttonChangePassword)
    Button buttonChangePassword;
    @BindView(R.id.buttonLogout)
    TextView buttonLogout;
    @BindView(R.id.licenceNoLabal)
    TextView licensNumber;
    @BindView(R.id.buttonUpdateProfile)
    Button buttonUpdateProfile;
    @BindView(R.id.spinnerSpeciality)
    Spinner spinnerSpeciality;
    @BindView(R.id.pharmacySpinner)
    Spinner pharmacySpinner;
    @BindView(R.id.labSpinner)
    Spinner labSpinner;
    @BindView(R.id.radiologySpinner)
    Spinner radiologySpinner;
    @BindView(R.id.profileName)
    TextView profileName;
    @BindView(R.id.addressLabel)
    TextView addressLabel;
    @BindView(R.id.phoneLabel)
    TextView phoneLabel;
    List<SpecialitiesModel> specialitiesModelList;
    LabsList labsListObject;
    final Calendar myCalendar = Calendar.getInstance();
    File mFile;
    int pharmID = 0;
    int labID = 0;
    int radID = 0;
    ProgressDialog pDialog;

    public FragmentMyProfile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_myprofile, container, false);

        ButterKnife.bind(this, view);
        getSpecialities();
        hitLabsApi();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProfileImage();

            }
        });
        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
            }
        });

        buttonUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

        textViewDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog =  new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logOut();
            }
        });
        labSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Lab lab = labsListObject.getPathLabList().get(i);
                    labID = lab.getLabID();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        pharmacySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Pharmacy lab = labsListObject.getPharmacyList().get(i);
                    pharmID = lab.getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        radiologySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Lab lab = labsListObject.getRadLabList().get(i);
                    radID = lab.getLabID();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editTextPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(i ==11)
                {
                    String input = editTextPhoneNumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    editTextPhoneNumber.setText(number);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    private void logOut() {

        Dialogues.yesnoDialogue(getActivity(), "Are you sure you want to logout?", new CallBacks() {
            @Override
            public void yes() {

                JSONObject params = new JSONObject();
                String consultantID = FetchData.getData(getActivity(), "userId");
                String access_token = FetchData.getData(getActivity(), "access_token");

               String email= FetchData.getData(getContext(), "userEmail");
                String encrypt = AESEncryption.encrypt(email, Const.Encryption_Key, Const.Encryption_IV);
                try {
                    params.put("Email", encrypt);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                    @Override
                    public void success(String response) {
                        if (response != null) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject meta = jsonObject.getJSONObject("meta");
                                String code = meta.getString("code");
                                if (Integer.valueOf(code) == 200) {

                                    SaveData.SaveData(getActivity(), "exists", "false");
                                    SharedPreferences preferences = getActivity().getSharedPreferences("appData", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.commit();
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().finish();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Logs.showLog("Login", response);
                        }
                    }

                    @Override
                    public void error(String response) {

                    }
                },
                        getActivity(), params, Const.LOGOUT);
                apiConnection.makeStringReq();



            }

            @Override
            public void no() {

            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        textViewDOB.setText(sdf.format(myCalendar.getTime()));

    }

    private void updateProfile() {

        Map<String, String> params = new HashMap<String, String>();
        String consultantID = FetchData.getData(requireActivity(), "userId");
        String access_token = FetchData.getData(requireActivity(), "access_token");

        params.put("action", "Consultant Profile");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("speciality", spinnerSpeciality.getSelectedItem().toString());
        params.put("phoneNumber", editTextPhoneNumber.getText().toString());
        params.put("address", editTextAddress.getText().toString());
        params.put("city", editTextCity.getText().toString());
        params.put("license", licensNumber.getText().toString());
        params.put("firstName", editTextFirstName.getText().toString());
        params.put("lastName", editTextLastName.getText().toString());
        params.put("dob", textViewDOB.getText().toString());
        params.put("qualification", editTextQualification.getText().toString());
        params.put("PrefPharmacyID", String.valueOf(pharmID));
        params.put("PrefPathId", String.valueOf(labID));
        params.put("PrefRadID", String.valueOf(radID));
        params.put("country", "PK");
        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                if (response != null) {

                    try {

                        Log.e(TAG, "Update Profile Response: "+ response);

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {

                            phoneLabel.setText("Phone :"+editTextPhoneNumber.getText().toString());
                            Toast.makeText(requireActivity(), "Profile Updated.", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Logs.showLog("Login", response);
                }
            }
            @Override
            public void error(String response) {

            }
        },
                getActivity(), params, Const.UPDATEPROFILE);
        apiConnection.makeStringReq();
    }

    private void updateProfileWithImage(File file) {
        if (!pDialog.isShowing()) {
            pDialog.show();
        }

        String consultantID = FetchData.getData(requireActivity(), "userId");
        String access_token = FetchData.getData(requireActivity(), "access_token");
        File  compressedImageFile = null;

        try {
            compressedImageFile = new Compressor(requireActivity()).compressToFile(file);
            int file_size = Integer.parseInt(String.valueOf(compressedImageFile.length()/1024));
           // Toast.makeText(getActivity(), ""+file_size, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (compressedImageFile != null) {
            Log.e(TAG, "Consultant ID: "+ consultantID);
            Log.e(TAG, "Compress Image File: "+ compressedImageFile);

            try {
                // add another part within the multipart request
                RequestBody requestAction = RequestBody.create(MediaType.parse("multipart/form-data"), "Consultant Profile Picture Update");
                RequestBody requestUserId = RequestBody.create(MediaType.parse("multipart/form-data"), consultantID);
                MultipartBody.Part imageFile;
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), compressedImageFile);
                imageFile = MultipartBody.Part.createFormData("imageFile", compressedImageFile.getName(), requestFile);

                RetrofitApi.getService(Const.BASE_URL).updateProfileImage(requestAction, requestUserId, imageFile).enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<GeneralResponse> call, @NotNull Response<GeneralResponse> response) {
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        Log.e(TAG, "Update Profile Image Response: "+response.toString());

                        if(response.isSuccessful() && response.body()!=null){
                            if(response.body().getMeta().getCode()==200){
                                Toast.makeText(requireActivity(), "Profile Image updated.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    @Override
                    public void onFailure(@NotNull Call<GeneralResponse> call, @NotNull Throwable t) {
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        Log.e(TAG, "onFailure: "+ t.getMessage());
                    }
                });

            }
            catch (Exception e){
                e.printStackTrace();
            }

        }

        //
//        String mimetype = FetchData.getMimeType(mFile.getPath());
//        Ion.with(this)
//                .load(Const.BASE_URL + Const.UPDATEPROFILEIMAGE)
//                .setMultipartParameter("action", "Consultant Profile Picture Update")
//                .setMultipartParameter("consultantId", consultantID)
//                .setMultipartParameter("access_token", access_token)
//                .setMultipartFile("imageFile", mimetype, compressedImageFile)
//                .asString()
//                .setCallback(new FutureCallback<String>() {
//                    @Override
//                    public void onCompleted(Exception e, String result) {
//                        if (pDialog.isShowing()) {
//                            pDialog.dismiss();
//                        }
//                        if (result != null) {
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result);
//                                JSONObject meta = jsonObject.getJSONObject("meta");
//                                String code = meta.getString("code");
//                                if (Integer.valueOf(code) == 200) {
//                                    Toast.makeText(getActivity(), "Profile Updated.", Toast.LENGTH_SHORT).show();
//
//                                }
//                            } catch (JSONException e1) {
//                                e.printStackTrace();
//                            }
//
//                            Logs.showLog("Profile update", result);
//                        }
//                    }
//                });



    }

    private void getProfile() {
        Map<String, String> params = new HashMap<String, String>();
        String consultantID = FetchData.getData(getActivity(), "userId");
        String access_token = FetchData.getData(getActivity(), "access_token");

        params.put("action", "Consultant Profile");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);

        ApiConnectionBackground apiConnection = new ApiConnectionBackground(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        Log.e(TAG, "Get Profile Response: "+response );

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {
                            JSONObject jsonResponse = jsonObject.getJSONObject("response");

                            String firstName = jsonResponse.getString("FirstName");
                            String lastName = jsonResponse.getString("LastName");
                            String speciality = jsonResponse.getString("Speciality");
                            String cellNumber = jsonResponse.getString("CellNumber");
                            String address = jsonResponse.getString("Address");
                            String city = jsonResponse.getString("City");
                            String DOB = jsonResponse.getString("DOB");
                            String Qualification = jsonResponse.getString("Qualification");
                            String filpath = jsonResponse.getString("FilePath");

                            //int index = specialitiesModelList.indexOf(speciality);
                            profileName.setText(firstName +" " +lastName);
                            addressLabel.setText(address);
                            phoneLabel.setText(cellNumber);
                            editTextFirstName.setText(firstName);
                            editTextLastName.setText(lastName);
                            editTextPhoneNumber.setText(cellNumber);
                            editTextAddress.setText(address);
                            editTextCity.setText(city);
                            licensNumber.setText(jsonResponse.getString("License"));
                            textViewDOB.setText(DOB);
                            editTextQualification.setText(Qualification);
//                            Picasso.with(getActivity())
//                                    .load(filpath).placeholder(R.mipmap.pic).error(R.mipmap.pic)
//                                    .into(imageView);


                            Glide.with(requireActivity())
                                    .load(filpath)
                                    .centerCrop()
                                    .skipMemoryCache(true)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .placeholder(R.mipmap.pic)
                                    .into(imageView);

                            ArrayAdapter<SpecialitiesModel> adapter =
                                    new ArrayAdapter<SpecialitiesModel>(requireActivity(), R.layout.spinner, specialitiesModelList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerSpeciality.setAdapter(adapter);
                            for (int i = 0; i < specialitiesModelList.size(); i++) {
                                SpecialitiesModel clinicsModels = specialitiesModelList.get(i);


                                String name = clinicsModels.getSpecialityDesc();
                                if (name.equals(speciality)) {

                                    spinnerSpeciality.setSelection(i);
                                    break;
                                }


                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {
            }
        },

                getActivity(), params, Const.GETPROFILe);
        apiConnection.makeStringReq();

    }

    public void hitLabsApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("ConID", FetchData.getData(requireActivity(), "userId"));
        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                Log.e(TAG, "GetLabAndPharmacyList Response: "+ response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {



                            JSONObject jsonResponse = jsonObject.getJSONObject("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<LabsList>() {
                            }.getType();
                            labsListObject = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<Pharmacy> pharAdapter =
                                    new ArrayAdapter<Pharmacy>(getActivity(), R.layout.spinner, labsListObject.getPharmacyList());
                            pharAdapter.setDropDownViewResource(R.layout.spinner);
                            pharmacySpinner.setAdapter(pharAdapter);
                            pharmacySpinner.setPrompt("Select Pharmacy");
                            ArrayAdapter<Lab> labAdapter =
                                    new ArrayAdapter<Lab>(getActivity(), R.layout.spinner, labsListObject.getPathLabList());
                            labAdapter.setDropDownViewResource(R.layout.spinner);
                            labSpinner.setAdapter(labAdapter);
                            labSpinner.setPrompt("Select Lab");
                            ArrayAdapter<Lab> radAdapter =
                                    new ArrayAdapter<Lab>(getActivity(), R.layout.spinner, labsListObject.getRadLabList());
                            labAdapter.setDropDownViewResource(R.layout.spinner);
                            radiologySpinner.setAdapter(radAdapter);
                            radiologySpinner.setPrompt("Select Radiology");

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }

            }

            @Override
            public void error(String response) {
            }
        }, getActivity(), params, Const.GETLABANDPHARMACYLIST);
        apiConnection.makeStringReq();
    }

    public void getSpecialities() {
        Map<String, String> params = new HashMap<String, String>();

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "Get Specialities Response: "+response );


                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {
                            JSONArray jsonResponse = jsonObject.getJSONArray("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<SpecialitiesModel>>() {
                            }.getType();
                            specialitiesModelList = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<SpecialitiesModel> adapter =
                                    new ArrayAdapter<SpecialitiesModel>(getActivity(), R.layout.spinner, specialitiesModelList);


                            adapter.setDropDownViewResource(R.layout.spinner);
                            spinnerSpeciality.setAdapter(adapter);
                            spinnerSpeciality.setPrompt("Select Site");

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
                getProfile();
            }

            @Override
            public void error(String response) {
                getProfile();
            }
        }, getActivity(), params, Const.GETSPECIALITIES);
        apiConnection.makeStringReq();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                mFile = list.get(0);
                File inputfile = new File(mFile.getPath());
//
//                mFile= AESEncryption.encryptFile(inputfile,Const.Encryption_Key,Const.Encryption_IV,mFile);
                Glide.with(getActivity()).load(mFile).into(imageView);

                if (mFile != null) {
                    updateProfileWithImage(inputfile);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                } else {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                }
                break;
            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Storage", Manifest.permission.READ_EXTERNAL_STORAGE, getActivity(), 101)) {
                        try{
                            EasyImage.openCameraForImage(FragmentMyProfile.this, 101);
                        }catch(Exception e){

                        }

                    }
                } else {
                    if (FetchData.CheckPermission("Storage", Manifest.permission.READ_EXTERNAL_STORAGE, getActivity(), 101)) {
                        EasyImage.openGallery(FragmentMyProfile.this, 101);
                    }
                }

                break;
        }
    }

    private void selectProfileImage() {
        final CharSequence[] items = {"Select Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Select Camera")) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                } else if (items[item].equals("Gallery")) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity(), 101)) {
                        EasyImage.openGallery(FragmentMyProfile.this, 101);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


}