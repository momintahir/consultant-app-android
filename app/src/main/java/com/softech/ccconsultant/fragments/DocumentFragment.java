package com.softech.ccconsultant.fragments;

import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softech.ccconsultant.R;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.Const;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DocumentFragment extends Fragment implements IWebListener {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    FragmentManager fragmentManager;

    private OnFragmentInteractionListener mListener;

    public DocumentFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DocumentFragment newInstance() {
        DocumentFragment fragment = new DocumentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callDocumentService();


    }
    public void callDocumentService()
    {
            String consultantID = FetchData.getData(requireActivity(), "userId");
            String access_token = FetchData.getData(requireActivity(), "access_token");

            Map<String, String> params = new HashMap<String, String>();
            params.put("action", "Load Folders");
            params.put("consultantId", consultantID);
            params.put("access_token", access_token);
            params.put("patientId","116");
            ApiConnection apiConnection = new ApiConnection(DocumentFragment.this,
                    getActivity(),params, Const.Documents);
            apiConnection.makeStringReq();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        fragmentManager = getFragmentManager();
        return view;
    }

    @Override
    public void success(String response)
    {


    }

    @Override
    public void error(String response) {

    }

    // TODO: Rename method, update argument and hook method into UI event


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
