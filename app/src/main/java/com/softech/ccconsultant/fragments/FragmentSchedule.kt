package com.softech.ccconsultant.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.softech.ccconsultant.R
import com.softech.ccconsultant.adapters.MyListAdapter
import com.softech.ccconsultant.apiconnections.ApiConnection
import com.softech.ccconsultant.bottomSheet.TimePickerBottomSheet
import com.softech.ccconsultant.bottomSheet.TimePickerBottomSheet.OnTimePickerControlsListener
import com.softech.ccconsultant.data.models.models.CliniclistModel
import com.softech.ccconsultant.data.models.models.ScheduleModel
import com.softech.ccconsultant.interfaces.IWebListener
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.utils.Const
import com.softech.ccconsultant.utils.Dialogues
import org.json.JSONException
import org.json.JSONObject


class FragmentSchedule : Fragment(), OnTimePickerControlsListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private val TAG = "FragmentSchedule"

    lateinit var recyclerView: RecyclerView
    lateinit var mAdapter: MyListAdapter
    lateinit var list: ArrayList<String>
    lateinit var spinnerClinicList: Spinner
    
    var clinicID: String? = null
    lateinit var btnUpdateSchedule: Button

    lateinit var sundayCheckbox: AppCompatCheckBox
    lateinit var mondayCheckbox: CheckBox
    lateinit var tuesdayCheckbox: CheckBox
    lateinit var wednesdayCheckbox: CheckBox
    lateinit var thursdayCheckbox: CheckBox
    lateinit var fridayCheckbox: CheckBox
    lateinit var saturdayCheckbox: CheckBox

    var id: String? = null
    var consultantID: String? = null
    var access_token: String? = null

    lateinit var btnSundayStartTime: AppCompatButton
    lateinit var btnSundayEndTime: AppCompatButton

    lateinit var btnMondayStartTime: AppCompatButton
    lateinit var btnMondayEndTime: AppCompatButton

    lateinit var btnTuesdayStartTime: AppCompatButton
    lateinit var btnTuesdayEndTime: AppCompatButton

    lateinit var btnWednesdayStartTime: AppCompatButton
    lateinit var btnWednesdayEndTime: AppCompatButton

    lateinit var btnThursdayStartTime: AppCompatButton
    lateinit var btnThursdayEndTime: AppCompatButton

    lateinit var btnFridayStartTime: AppCompatButton
    lateinit var btnFridayEndTime: AppCompatButton

    lateinit var btnSaturdayStartTime: AppCompatButton
    lateinit var btnSaturdayEndTime: AppCompatButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_schedule, container, false)
        list = ArrayList()
        consultantID = FetchData.getData(requireActivity(), "userId")
        access_token = FetchData.getData(requireActivity(), "access_token")
        initViews(view)
        getClinicList()
        btnUpdateSchedule.setOnClickListener { v: View? ->
            try {
                updateSchedule()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return view
    }

    @Throws(JSONException::class)
    private fun updateSchedule() {
        val schedule = JSONObject()
        //        Map<String, String> schedule = new HashMap<String, String>();
        schedule.put("Id", id)
        schedule.put("ClinicId", clinicID)
        schedule.put("ConsultantId", consultantID)
        if (mondayCheckbox.isChecked) {
            schedule.put("Mon", 1.toString())
        } else {
            schedule.put("Mon", 0.toString())
        }
        if (tuesdayCheckbox.isChecked) {
            schedule.put("Tue", 1.toString())
        } else {
            schedule.put("Tue", 0.toString())
        }
        if (wednesdayCheckbox.isChecked) {
            schedule.put("Wed", 1.toString())
        } else {
            schedule.put("Wed", 0.toString())
        }
        if (thursdayCheckbox.isChecked) {
            schedule.put("Thu", 1.toString())
        } else {
            schedule.put("Thu", 0.toString())
        }
        if (fridayCheckbox.isChecked) {
            schedule.put("Fri", 1.toString())
        } else {
            schedule.put("Fri", 0.toString())
        }
        if (saturdayCheckbox.isChecked) {
            schedule.put("Sat", 1.toString())
        } else {
            schedule.put("Sat", 0.toString())
        }
//        if (sundayCheckbox.isChecked) {
//            schedule.put("Sun", 1.toString())
//        } else {
//            schedule.put("Sun", 0.toString())
//        }

//        schedule.put("GenDay", "0");
        schedule.put("MonStart", "${btnMondayStartTime.text}")
        schedule.put("MonEnd", "${btnMondayEndTime.text}")

        schedule.put("TueStart", "${btnTuesdayStartTime.text}")
        schedule.put("TueEnd", "${btnTuesdayEndTime.text}}")

        schedule.put("WedStart", "${btnWednesdayStartTime.text}")
        schedule.put("WedEnd", "${btnWednesdayEndTime.text}")

        schedule.put("ThuStart", "${btnThursdayStartTime.text}")
        schedule.put("ThuEnd", "${btnThursdayEndTime.text}")

        schedule.put("FriStart", "${btnFridayStartTime.text}")
        schedule.put("FriEnd", "${btnFridayEndTime.text}")

        schedule.put("SatStart", "${btnSaturdayStartTime.text}")
        schedule.put("SatEnd", "${btnSaturdayEndTime.text}")

        schedule.put("SunStart", "${btnSundayStartTime.text}}")
        schedule.put("SunEnd", "${btnSundayEndTime.text}")

        //        schedule.put("GenStart", null);
//        schedule.put("GenEnd", null);
//        schedule.put("Hours", null);
//        schedule.put("Minutes", null);
//        schedule.put("Duration", null);


//        JSONObject jsonObject = new JSONObject();
        val params: MutableMap<String, String?> = HashMap()
        params["access_token"] = access_token
        params["schedule"] = schedule.toString()

        Log.e(TAG, "Schedule Params: $params")

        val apiConnection = ApiConnection(
            object : IWebListener {
                override fun success(response: String) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            Toast.makeText(
                                requireActivity(),
                                "Calendar updated successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            Dialogues.showOkDialogue(activity, error)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun error(response: String) {}
            },
            activity, params, Const.UPDATE_SCHEDULE
        )
        apiConnection.makeStringReq()

    }

    private fun initViews(view: View) {
        sundayCheckbox = view.findViewById(R.id.sundayCheckbox)
        mondayCheckbox = view.findViewById(R.id.mondayCheckbox)
        tuesdayCheckbox = view.findViewById(R.id.tuesdayCheckbox)
        wednesdayCheckbox = view.findViewById(R.id.wednesdayCheckbox)
        thursdayCheckbox = view.findViewById(R.id.thursdayCheckbox)
        fridayCheckbox = view.findViewById(R.id.fridayCheckbox)
        saturdayCheckbox = view.findViewById(R.id.saturdayCheckbox)
        btnUpdateSchedule = view.findViewById(R.id.btnUpdateSchedule)

        spinnerClinicList = view.findViewById(R.id.spinnerClinicList)

        btnSundayStartTime = view.findViewById(R.id.btnSundayStartTime)
        btnSundayEndTime = view.findViewById(R.id.btnSundayEndTime)

        btnMondayStartTime = view.findViewById(R.id.btnMondayStartTime)
        btnMondayEndTime = view.findViewById(R.id.btnMondayEndTime)

        btnTuesdayStartTime = view.findViewById(R.id.btnTuesdayStartTime)
        btnTuesdayEndTime = view.findViewById(R.id.btnTuesdayEndTime)

        btnWednesdayStartTime = view.findViewById(R.id.btnWednesdayStartTime)
        btnWednesdayEndTime = view.findViewById(R.id.btnWednesdayEndTime)

        btnThursdayStartTime = view.findViewById(R.id.btnThursdayStartTime)
        btnThursdayEndTime = view.findViewById(R.id.btnThursdayEndTime)

        btnFridayStartTime = view.findViewById(R.id.btnFridayStartTime)
        btnFridayEndTime = view.findViewById(R.id.btnFridayEndTime)

        btnSaturdayStartTime = view.findViewById(R.id.btnSaturdayStartTime)
        btnSaturdayEndTime = view.findViewById(R.id.btnSaturdayEndTime)

        sundayCheckbox.setOnClickListener(this)
        mondayCheckbox.setOnClickListener(this)
        tuesdayCheckbox.setOnClickListener(this)
        wednesdayCheckbox.setOnClickListener(this)
        thursdayCheckbox.setOnClickListener(this)
        fridayCheckbox.setOnClickListener(this)
        saturdayCheckbox.setOnClickListener(this)

    }

    private fun getClinicList() {
            val ClinicList = FetchData.getData(requireActivity(), "cliniclist")
            val gson = Gson()
            val type = object : TypeToken<ArrayList<CliniclistModel?>?>() {}.type
            val connectedClinicsModelArrayList = gson.fromJson<ArrayList<CliniclistModel>>(ClinicList.toString(), type)
            val adapter = ArrayAdapter(requireActivity(), R.layout.spinner, connectedClinicsModelArrayList)
            adapter.setDropDownViewResource(R.layout.spinner)
            spinnerClinicList.adapter = adapter
            spinnerClinicList.setSelection(0)
            clinicID = connectedClinicsModelArrayList[0].oid
            spinnerClinicList.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        val cliniclistModel = connectedClinicsModelArrayList[position]
                        clinicID = cliniclistModel.oid
                        getData()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }
        }

    private fun getData() {
        Log.e(TAG, "getData: ")
            val jsonObject = JSONObject()
            try {
                jsonObject.put("consultantId", consultantID)
                jsonObject.put("access_token", access_token)
                jsonObject.put("clinicId", clinicID)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val apiConnection = ApiConnection(
                object : IWebListener {
                    override fun success(response: String) {
                        try {
                            val jsonObject = JSONObject(response)
                            Log.e(TAG, "success: $response")

                            val meta = jsonObject.getJSONObject("meta")
                            val code = meta.getString("code")
                            if (Integer.valueOf(code) == 200) {
                                val gson = Gson()
                                val type = object : TypeToken<ScheduleModel?>() {}.type
                                val jsonArray = jsonObject.getJSONArray("response")
                                val scheduleObject = jsonArray.getJSONObject(0)
                                val scheduleModel = gson.fromJson<ScheduleModel>(
                                    scheduleObject.toString(),
                                    type
                                )
                                id = scheduleModel.id.toString()
                                setData(scheduleModel)
                            } else if (Integer.valueOf(code) == 504) {
                                val error = jsonObject.getString("error")
                                Dialogues.showOkDialogue(activity, error)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                    override fun error(response: String) {
                        Log.e(TAG, "error: $response" )
                    }
                },
                activity, jsonObject, Const.VIEWSCHEDULE
            )
            apiConnection.makeStringReq1()
        }

    private fun setData(scheduleModel: ScheduleModel) {
        Log.e(TAG, "setData: $scheduleModel")

        if(scheduleModel.sun == 1){
            sundayCheckbox.checked = true
            btnSundayStartTime.text = scheduleModel.sunStart
            btnSundayEndTime.text = scheduleModel.sunEnd
        }

        if(scheduleModel.mon == 1){
            mondayCheckbox.checked = true
            btnMondayStartTime.text = scheduleModel.monStart
            btnMondayEndTime.text = scheduleModel.monEnd
        }

        if(scheduleModel.tue == 1){
            tuesdayCheckbox.checked = true
            btnTuesdayStartTime.text = scheduleModel.tueStart
            btnTuesdayEndTime.text = scheduleModel.tueEnd
        }

        if(scheduleModel.wed == 1){
            wednesdayCheckbox.checked = true
            btnWednesdayStartTime.text = scheduleModel.wedStart
            btnWednesdayEndTime.text = scheduleModel.wedEnd
        }

        if(scheduleModel.thu == 1){
            thursdayCheckbox.checked = true
            btnThursdayStartTime.text = scheduleModel.thuStart
            btnThursdayEndTime.text = scheduleModel.thuEnd
        }

        if(scheduleModel.fri == 1){
            fridayCheckbox.checked = true
            btnFridayStartTime.text = scheduleModel.friStart
            btnFridayEndTime.text = scheduleModel.friEnd
        }

        if(scheduleModel.sat == 1){
            saturdayCheckbox.checked = true
            btnSaturdayStartTime.text = scheduleModel.satStart
            btnSaturdayEndTime.text = scheduleModel.satEnd
        }

    }

    override fun onTimeSelected(
        startTime: String,
        endTime: String,
        btnStartTime: AppCompatButton,
        btnEndTime: AppCompatButton,
        checkBox: CheckBox
    ) {
        if(startTime == "00:00" || endTime == "00:00"){
            checkBox.isChecked = false
        }
        btnStartTime.text = startTime
        btnEndTime.text = endTime
    }

    override fun onTimePickerCancelled(checkBox: CheckBox) {
        checkBox.isChecked = false
    }

    private fun disPlayTimePickerBottomSheet(
        btnStartTime: AppCompatButton,
        btnEndTime: AppCompatButton,
        checkBox: CheckBox
    ) {
        val timePickerBottomSheet = TimePickerBottomSheet(this@FragmentSchedule, btnStartTime, btnEndTime, checkBox)
        timePickerBottomSheet.show(childFragmentManager, "bottomSheet")
        timePickerBottomSheet.isCancelable = false
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.sundayCheckbox -> {
                if(sundayCheckbox.isChecked){
                    disPlayTimePickerBottomSheet(btnSundayStartTime, btnSundayEndTime, sundayCheckbox)
                }else{
                    btnSundayStartTime.text = "00:00"
                    btnSundayEndTime.text = "00:00"
                }
            }
            R.id.mondayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnMondayStartTime,
                        btnMondayEndTime,
                        mondayCheckbox
                    )
                }else{
                    btnMondayStartTime.text = "00:00"
                    btnMondayEndTime.text = "00:00"
                }
            }
            R.id.tuesdayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnTuesdayStartTime,
                        btnTuesdayEndTime,
                        tuesdayCheckbox
                    )
                }else{
                    btnTuesdayStartTime.text = "00:00"
                    btnTuesdayEndTime.text = "00:00"
                }
            }
            R.id.wednesdayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnWednesdayStartTime,
                        btnWednesdayEndTime,
                        wednesdayCheckbox
                    )
                }else{
                    btnWednesdayStartTime.text = "00:00"
                    btnWednesdayEndTime.text = "00:00"
                }
            }
            R.id.thursdayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnThursdayStartTime,
                        btnThursdayEndTime,
                        thursdayCheckbox
                    )
                }else{
                    btnThursdayStartTime.text = "00:00"
                    btnThursdayEndTime.text = "00:00"
                }
            }
            R.id.fridayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnFridayStartTime,
                        btnFridayEndTime,
                        fridayCheckbox
                    )
                }else{
                    btnFridayStartTime.text = "00:00"
                    btnFridayEndTime.text = "00:00"
                }
            }
            R.id.saturdayCheckbox -> {
                val isChecked = (view as CheckBox).isChecked
                if(isChecked){
                    disPlayTimePickerBottomSheet(
                        btnSaturdayStartTime,
                        btnSaturdayEndTime,
                        saturdayCheckbox
                    )
                }else{
                    btnSaturdayStartTime.text = "00:00"
                    btnSaturdayEndTime.text = "00:00"
                }
            }
        }
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {

    }

    var CheckBox.checked: Boolean
        get() = isChecked
        set(value) {
            if(isChecked != value) {
                isChecked = value
                jumpDrawablesToCurrentState()
            }
        }

}