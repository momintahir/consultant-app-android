package com.softech.ccconsultant.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.AddCalendarActivity;
import com.softech.ccconsultant.BookAppointmentActivity;
import com.softech.ccconsultant.LoginActivity;
import com.softech.ccconsultant.R;
import com.softech.ccconsultant.SlotsActivity;
import com.softech.ccconsultant.adapters.AppointmentsAdapter;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.models.AppointmentsModel;
import com.softech.ccconsultant.data.models.models.CliniclistModel;
import com.softech.ccconsultant.data.models.models.PatientStatus;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.CallBacks;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Dialogues;
import com.softech.ccconsultant.utils.Logs;
import com.softech.ccconsultant.webrtc.services.WebrtcService;
import com.softech.ccconsultant.webrtc.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentAppointments extends Fragment implements IWebListener, SwipeRefreshLayout.OnRefreshListener, AppointmentsAdapter.OnDeleteAptClickListenter {

    private final String TAG = "FragmentAppointments";

    @BindView(R.id.recyclViewAppointments)
    RecyclerView recyclViewAppointments;
    @BindView(R.id.simpleCalandar)
    CalendarView simpleCalandar;
    @BindView(R.id.textViewFoundNothing)
    TextView textViewFoundNothing;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.spinnerClinicList)
    Spinner spinnerClinicList;
    @BindView(R.id.leftarrow)
    ImageView leftArrow;
    @BindView(R.id.rightarrow)
    ImageView rightarrow;
    @BindView(R.id.dateLabel)
    TextView datelabel;
    @BindView(R.id.tvDay)
    TextView tvDay;
    String clinicID, selectedDate;
    Date currentDated;
    ArrayList<Integer> patientIdsList=new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    ArrayList<AppointmentsModel> appointmentsModelArrayList = new ArrayList<AppointmentsModel>();
    ArrayList<PatientStatus> patientStatusList = new ArrayList<PatientStatus>();
    private AppointmentsAdapter mAdapter;
    String patientIds="";
    public static Timer timer = new Timer();
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 10000;
    String ptPhone;

    public FragmentAppointments() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("FragmentAppointments","Oncreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        TelephonyManager tm = (TelephonyManager)getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();


        Log.d("FragmentAppointments",countryCodeValue);
        View view = inflater.inflate(R.layout.fragment_appointments, container, false);
//        patientIdsList.add(123);
//        patientIdsList.add(456);
//        patientIdsList.add(789);
//        patientIdsList.add(532);
//
//


        ButterKnife.bind(this, view);
        String ClinicList = FetchData.getData(requireActivity(), "cliniclist");
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
        }.getType();
        datelabel.setText(getCurrentDate(Calendar.getInstance().getTime()));
        datelabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (simpleCalandar.getVisibility() == View.GONE) {
                    simpleCalandar.setVisibility(View.VISIBLE);

                } else {
                    simpleCalandar.setVisibility(View.GONE);
                }

            }
        });




//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                timer.scheduleAtFixedRate(new TimerTask() {
//                    @Override
//                    public void run() {
//
//                    }
//                }, 1000, 15000);
//            }
//        }).start();




    final ArrayList<CliniclistModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);

        ArrayAdapter<CliniclistModel> adapter =
                new ArrayAdapter<CliniclistModel>(getActivity(), R.layout.spinner, connectedClinicsModelArrayList);
        adapter.setDropDownViewResource(R.layout.spinner);
        spinnerClinicList.setAdapter(adapter);
        spinnerClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                CliniclistModel cliniclistModel = connectedClinicsModelArrayList.get(position);
                clinicID = cliniclistModel.getOID();
                SaveData.SaveData(getActivity(), "clinicId", clinicID);
                getAppointments();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        simpleCalandar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                selectedDate = String.format("%d/%d/%d", dayOfMonth, month + 1, year);
                currentDated = FetchData.dateFromString(selectedDate);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
                selectedDate = sdf.format(FetchData.dateFromString(selectedDate));
                String newDate = sdf1.format(FetchData.dateFromString(selectedDate));

                datelabel.setText(newDate);
                getAppointments();
            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = FetchData.getPreviousOrNextDate(selectedDate, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
                String newDate = sdf.format(FetchData.dateFromString(selectedDate));
                datelabel.setText(newDate);
                Date date = FetchData.dateFromString(selectedDate);
                sdf = new SimpleDateFormat("EEEE");
                tvDay.setText(sdf.format(date));
                try {
                    simpleCalandar.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(selectedDate).getTime(), true, true);
                    getAppointments();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        rightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = FetchData.getPreviousOrNextDate(selectedDate, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
                String newDate = sdf.format(FetchData.dateFromString(selectedDate));
                datelabel.setText(newDate);

                Date date = FetchData.dateFromString(selectedDate);
                sdf = new SimpleDateFormat("EEEE");
                tvDay.setText(sdf.format(date));
                try {
                    simpleCalandar.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(selectedDate).getTime(), true, true);
                    getAppointments();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        //((TabsActivity) getActivity()).changeTitle("Your Appointments");

        //getAppointments();

        swiperefresh.setOnRefreshListener(FragmentAppointments.this);

//        SpannableString spannableString = new SpannableString(getString(R.string.messageWithSpannableLink));
//        ClickableSpan clickableSpan = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//
//            }
//        };
//        spannableString.setSpan(clickableSpan, spannableString.length() - 20,
//                spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        textViewFoundNothing.setText(spannableString, TextView.BufferType.SPANNABLE);
//        textViewFoundNothing.setMovementMethod(LinkMovementMethod.getInstance());

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        return view;
    }

    public String getCurrentDate(Date date) {
        System.out.println("Current time => " + date);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("MMM dd, yyyy");
        String newDate = df1.format(date);
        selectedDate = df.format(date);
        Log.d("new Date", newDate);
        currentDated = FetchData.dateFromString(selectedDate);
        df1 = new SimpleDateFormat("EEEE");
        tvDay.setText(df1.format(date));

        return newDate;
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    }

    public void getAppointments() {
        appointmentsModelArrayList.clear();
        patientIdsList.clear();
        String consultantID = FetchData.getData(requireActivity(), "userId");
        String access_token = FetchData.getData(requireActivity(), "access_token");
        Map<String, String> params = new HashMap<String, String>();
        params.put("PatientIDs", "List Of Consultant Appointments");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("clinicId", clinicID);
        params.put("date", selectedDate);

        ApiConnection apiConnection = new ApiConnection(FragmentAppointments.this, getActivity(), params, Const.VIEWAPPOINTMENTS);
        apiConnection.makeStringReq();

    }

    public void getAppointmentsOnRefresh() {
        appointmentsModelArrayList.clear();
        patientIdsList.clear();
        String consultantID = FetchData.getData(getActivity(), "userId");
        String access_token = FetchData.getData(getActivity(), "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("PatientIDs", "List Of Consultant Appointments");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("clinicId", clinicID);
        params.put("date", selectedDate);


        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject meta = jsonObject.getJSONObject("meta");
                    String code = meta.getString("code");
                    simpleCalandar.setVisibility(View.GONE);
                    if (Integer.valueOf(code) == 200) {

                        if (jsonObject.getString("action").equals("Delete Appointment")) {
                            getAppointments();
                            return;

                        }
                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<AppointmentsModel>>() {
                        }.getType();
                        appointmentsModelArrayList.clear();
                        patientIdsList.clear();
                        appointmentsModelArrayList = gson.fromJson(jsonArray.toString(), type);

                        for (int i=0;i<appointmentsModelArrayList.size();i++){
                            patientIdsList.add(Integer.parseInt(appointmentsModelArrayList.get(i).getiPatID()));
                        }


                        if (appointmentsModelArrayList.size() > 0) {
                            recyclViewAppointments.setVisibility(View.VISIBLE);
                            textViewFoundNothing.setVisibility(View.GONE);
                            mAdapter = new AppointmentsAdapter(appointmentsModelArrayList, getActivity(), FragmentAppointments.this);
                            recyclViewAppointments.setAdapter(mAdapter);
                            // mAdapter.notifyDataSetChanged();
                        } else {

                            recyclViewAppointments.setVisibility(View.GONE);
                            textViewFoundNothing.setVisibility(View.VISIBLE);
                        }


                    } else if (Integer.valueOf(code) == 504) {
                        String message = meta.getString("message");

                        recyclViewAppointments.setVisibility(View.GONE);
                        textViewFoundNothing.setVisibility(View.VISIBLE);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void error(String response) {

            }
        },
                getActivity(), params, Const.VIEWAPPOINTMENTS);
        apiConnection.makeStringReq();

    }

    @Override
    public void success(String response) {
        Log.e(TAG, "success: "+ response);
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                String message = meta.getString("message");
                simpleCalandar.setVisibility(View.GONE);
                if (Integer.parseInt(code) == 200) {

                    if (jsonObject.getString("action").equals("Delete Appointment")) {
                        getAppointments();
                        return;
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<AppointmentsModel>>() {
                    }.getType();
                    appointmentsModelArrayList.clear();
                    patientIdsList.clear();
                    appointmentsModelArrayList = gson.fromJson(jsonArray.toString(), type);

                    for (int i=0;i<appointmentsModelArrayList.size();i++){
                        patientIdsList.add(Integer.parseInt(appointmentsModelArrayList.get(i).getiPatID()));
                    }


                    if (appointmentsModelArrayList.size() > 0) {
                        recyclViewAppointments.setVisibility(View.VISIBLE);
                        textViewFoundNothing.setVisibility(View.GONE);
                        mAdapter = new AppointmentsAdapter(appointmentsModelArrayList, getActivity(), this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclViewAppointments.setLayoutManager(mLayoutManager);
                        recyclViewAppointments.setItemAnimator(new DefaultItemAnimator());
                        recyclViewAppointments.setAdapter(mAdapter);
                        // mAdapter.notifyDataSetChanged();
                        recyclViewAppointments.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    } else {

                        recyclViewAppointments.setVisibility(View.GONE);
                        textViewFoundNothing.setVisibility(View.VISIBLE);
                    }


                }
                else if (Integer.parseInt(code) == 504) {
                    recyclViewAppointments.setVisibility(View.GONE);
                    textViewFoundNothing.setVisibility(View.VISIBLE);
                }
                else if(Integer.parseInt(code) == 404 && message.equals("Unauthorized")){
                    logOut();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
        Log.d("Appointments", response);
    }

    private void logOut() {
        Dialogues.yesDialogue(requireActivity(), "Unauthorized", "Your verification code has been expired. Login again.", new CallBacks() {
            @Override
            public void yes() {
                JSONObject params = new JSONObject();
                String consultantID = FetchData.getData(requireActivity(), "userId");
                String access_token = FetchData.getData(requireActivity(), "access_token");

                String email= FetchData.getData(requireActivity(), "userEmail");
                String encrypt = AESEncryption.encrypt(email, Const.Encryption_Key, Const.Encryption_IV);
                try {
                    params.put("Email", encrypt);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                    @Override
                    public void success(String response) {
                        if (response != null) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject meta = jsonObject.getJSONObject("meta");
                                String code = meta.getString("code");
                                if (Integer.valueOf(code) == 200) {

                                    SaveData.SaveData(getActivity(), "exists", "false");
                                    SharedPreferences preferences = getActivity().getSharedPreferences("appData", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.commit();
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().finish();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Logs.showLog("Login", response);
                        }
                    }

                    @Override
                    public void error(String response) {

                    }
                }, getActivity(), params, Const.LOGOUT);
                apiConnection.makeStringReq();
            }
            @Override
            public void no() { }
        });
    }

    @Override
    public void error(String response) {
        Log.e(TAG, response);
    }

    public void callingDeleteAppointmentService(AppointmentsModel model) {
        String access_token = FetchData.getData(requireActivity(), "access_token");
        String consultantID = FetchData.getData(requireActivity(), "userId");
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Delete Appointment");
        params.put("appointmentId", model.getiAppID());
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId", model.getiPatID());

        ApiConnection apiConnection = new ApiConnection(FragmentAppointments.this,
                getActivity(), params, Const.ACTION_DELETE_APPOINTMENT);
        apiConnection.makeStringReq();
    }

    public void callingDeleteAppointmentServiceForReschedule(AppointmentsModel model) {
        String access_token = FetchData.getData(getActivity(), "access_token");
        String consultantID = FetchData.getData(getActivity(), "userId");
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Delete Appointment");
        params.put("appointmentId", model.getiAppID());
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId", model.getiPatID());

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    JSONObject meta = jsonObject.getJSONObject("meta");
                    String code = meta.getString("code");
                    if (Integer.parseInt(code) == 200) {
                        Intent intent = new Intent(getActivity(), SlotsActivity.class);
                        intent.putExtra("consultantID",consultantID);
                        intent.putExtra("accessToken",access_token);
                        intent.putExtra("clinicID",clinicID);
                        Gson gson = new Gson();
                        String modell = gson.toJson(model);
                        intent.putExtra("model",modell);
                        Objects.requireNonNull(requireActivity()).startActivity(intent);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void error(String response) {

            }
        },
                getActivity(), params, Const.ACTION_DELETE_APPOINTMENT);
        apiConnection.makeStringReq();
    }

    @Override
    public void onRefresh() {
        getAppointmentsOnRefresh();
        swiperefresh.setRefreshing(false);
    }

    @Override
    public void OnDeleteAptClick(View caller, AppointmentsModel mItem) {
        switch (caller.getId()) {
            case R.id.deleteButton: {
                callingDeleteAppointmentService(mItem);
                break;
            }
            case R.id.ivAudioCall: {
                startNewActivity(false, mItem.getiPatID(), mItem.getPatientName(), "");
                break;
            }
            case R.id.ivVideoCall: {
                startNewActivity(true, mItem.getiPatID(), mItem.getPatientName(), "");
                break;
            }

            case R.id.rescheduleBtn:{
//                callingDeleteAppointmentServiceForReschedule(mItem);
                String access_token = FetchData.getData(requireActivity(), "access_token");
                String consultantID = FetchData.getData(requireActivity(), "userId");
                Intent intent = new Intent(requireActivity(), SlotsActivity.class);
                intent.putExtra("consultantID",consultantID);
                intent.putExtra("accessToken",access_token);
                intent.putExtra("clinicID",clinicID);
                Gson gson = new Gson();
                String modell = gson.toJson(mItem);
                intent.putExtra("model",modell);
                requireActivity().startActivity(intent);
            }
        }
    }

    private void startNewActivity(boolean isVideo, String callerID, String userRealName, String profileImage) {
        Log.e("videocall", "object :" + callerID);
        Intent intent = new Intent(getActivity(), WebrtcService.class);
        intent.putExtra(Constant.INITIATOR, true);
        intent.putExtra(Constant.CHAT_USER_NAME, userRealName);
        intent.putExtra(Constant.IS_VIDEO_CALL, isVideo);
        intent.putExtra(Constant.CHAT_ID, callerID);
        intent.putExtra(Constant.CHAT_USER_PICTURE, profileImage);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getActivity().startForegroundService(intent);
        } else {
            getActivity().startService(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        handler.postDelayed(runnable=new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(runnable, delay);
                if (patientIdsList.size()!=0) {
                    patientIds="";
                    for (int i = 0; i < patientIdsList.size(); i++) {
                        if (i + 1 == patientIdsList.size()) {
                            patientIds += patientIdsList.get(i);
                        } else {
                            patientIds += patientIdsList.get(i) + ",";

                        }
                    }

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("PatientIDs", patientIds);


                    ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                        @Override
                        public void success(String response) {

                            JSONObject jsonObject = null;
                            try {

                                Log.e(TAG, "Get Online Status Response: "+response);

                                jsonObject = new JSONObject(response);
                                JSONObject meta = jsonObject.getJSONObject("meta");
                                String code = meta.getString("code");

                                simpleCalandar.setVisibility(View.GONE);

                                if (Integer.parseInt(code) == 200) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                                    Gson gson = new Gson();
                                    Type type = new TypeToken<ArrayList<PatientStatus>>() {
                                    }.getType();
                                    patientStatusList.clear();
//                                    appointmentsModelArrayList.clear();

//                                appointmentsModelArrayList.add(new AppointmentsModel("123","123","123",
//                                        "123","123","123","3","3","Ahmed",
//                                        "34",54,"123"));
//
//
//                                appointmentsModelArrayList.add(new AppointmentsModel("123","123","123",
//                                        "456","123","123","3","3","Ali",
//                                        "34",54,"123"));
//
//                                appointmentsModelArrayList.add(new AppointmentsModel("123","123","123",
//                                        "789","123","123","3","3","Jamshed",
//                                        "34",54,"123"));
//
//                                appointmentsModelArrayList.add(new AppointmentsModel("123","123","123",
//                                        "532","123","123","3","3","Butt",
//                                        "34",54,"123"));

                                    patientStatusList = gson.fromJson(jsonArray.toString(), type);
                                    Log.d("patientIDs", patientStatusList.toString());
                                    recyclViewAppointments.setVisibility(View.VISIBLE);
                                    textViewFoundNothing.setVisibility(View.GONE);
                                    mAdapter = new AppointmentsAdapter(patientStatusList, appointmentsModelArrayList, getActivity(),FragmentAppointments.this);
                                    recyclViewAppointments.setAdapter(mAdapter);


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void error(String response) {
                            Log.d("patientIDs", "Error");

                        }
                    }, getActivity(), params, Const.GET_ONLINE_STATUS);
                    apiConnection.makeStringReqForStatus();
                }
            }
        },delay);

        Log.d("FragmentAppointments","OnResume");
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}