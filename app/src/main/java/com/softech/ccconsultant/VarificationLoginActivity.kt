package com.softech.ccconsultant

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.softech.ccconsultant.data.models.models.ConnectedClinicsModel
import com.softech.ccconsultant.sharedpreferences.FetchData
import com.softech.ccconsultant.sharedpreferences.SaveData
import kotlinx.android.synthetic.main.activity_varification_login.*
import java.util.*

class VarificationLoginActivity : AppCompatActivity() {
    var twoFACode: String = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_varification_login)

        twoFACode = FetchData.getData(this, "Code")
//        codeField.setText(twoFACode)
        Log.d("twoFACode",twoFACode)

//        codeField.setText(twoFACode)
        applyCodeBtn.setOnClickListener()
        {
            if (codeField.text.toString().equals(twoFACode)) {
                val ClinicList = FetchData.getData(this, "cliniclist")
                val isFirstLogin = FetchData.getData(this, "isFirstLog")
                val gson = Gson()
                val type = object : TypeToken<ArrayList<ConnectedClinicsModel>>() {

                }.type
                val connectedClinicsModelArrayList = gson.fromJson<ArrayList<ConnectedClinicsModel>>(ClinicList.toString(), type)
                if (isFirstLogin == "1") {
                    if (!connectedClinicsModelArrayList.isEmpty()) {

                    } else {
//                        val intent1 = Intent(this@VarificationLoginActivity, WelcomeActivity::class.java)
//                    var Name = FetchData.getData(this, "Name")
//                    intent1.putExtra("name", Name)
//                    intent1.putExtra("welcome", "Your clinic approval request status is Pending")
//                    intent1.putExtra("team", "Your request for the clinic has not been approved yet. Please wait…")
//                        startActivity(intent1)
//                        this@VarificationLoginActivity.finish()
//                        return@setOnClickListener
                        intent = Intent(this@VarificationLoginActivity, ProfileActivity::class.java)
                        this.startActivity(intent)
                    }
                } else if (isFirstLogin == "0") {
                    if (!connectedClinicsModelArrayList.isEmpty()) {
                        var exists = false
                        for (connectedClinics in connectedClinicsModelArrayList) {
                            val IsCalendarExist = connectedClinics.calendarExist
                            if (IsCalendarExist == "1") {
                                exists = true
                            }
                        }
                        if (exists) {
                            intent = Intent(this@VarificationLoginActivity, TabsActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            SaveData.SaveData(this@VarificationLoginActivity, "exists", "true")


                        } else {
                            intent = Intent(this@VarificationLoginActivity, AddCalendarActivity::class.java)
                        }
                        startActivity(intent)
                        this.finish()
                    }
                }
            } else {
                Toast.makeText(this@VarificationLoginActivity, "Please enter the correct code.", Toast.LENGTH_LONG).show()


            }
        }
    }
}
