package com.softech.ccconsultant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.apiconnections.RetrofitApi;
import com.softech.ccconsultant.data.models.models.ConnectedClinicsModel;
import com.softech.ccconsultant.data.models.models.Login.LoginResponse;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;
import com.softech.ccconsultant.webrtc.SocketIO;
import com.softech.ccconsultant.webrtc.util.Constant;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements IWebListener {

    private static final String TAG = "LoginActivity";

    @BindView(R.id.editTextUserName)
    EditText editTextUserName;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.tvSignUp)
    TextView tvSignUp;
    @BindView(R.id.textViewForgotPassword)
    TextView textViewForgotPassword;
  @BindView(R.id.tvTerms)
    TextView tvTerms;

    private boolean isPasswordVisible=false;

    @BindView(R.id.tvCondition)
    TextView tvCondition;

    @BindView(R.id.tvPolicy)
    TextView tvPolicy;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    String fcmToken;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        //  editTextUserName.setText("htkhan@hotmail.com");
//        editTextUserName.setText("farrukh.raza@softech.com.pk");
//        editTextPassword.setText("softech@123");
//        editTextUserName.setText("farrukh.raza@softech.com.pk");
//        editTextPassword.setText("softech@123");
        //editTextUserName.setText("waseem@test.com");
//        editTextUserName.setText("shah1@gmail.com");
//        editTextUserName.setText("alnafora@gmail.com");
//        editTextPassword.setText("123");
//        editTextUserName.setText("glam@test.com");
//        editTextPassword.setText("Imkhan1!");

        setUpViews();


        editTextPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_cancel, 0);


        editTextPassword.setOnTouchListener(new View.OnTouchListener() {
            int DRAWABLE_RIGHT = 2;
            int DRAWABLE_BOTTOM = 3;
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextPassword.getRight() - editTextPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Log.d("Rann","Asd");
                        togglePassVisability();
                        return true;
                    }
                }

                return false;

            }
        });




        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php");
                startActivity(intent);
            }
        });

        tvCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php");
                startActivity(intent);
            }
        });
        tvPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/privacy-clinic.php");
                startActivity(intent);

            }
        });


        textViewForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));

            }
        });

        Bundle intent1 = getIntent().getExtras();

        if (intent1 != null) {
            String Email = intent1.getString("email", "");
            String password = intent1.getString("password", "");
            editTextUserName.setText(Email);
            editTextPassword.setText(password);

        }

        String exists = FetchData.getData(LoginActivity.this, "exists");
        if (exists.equals("true")) {
            String userId = FetchData.getData(LoginActivity.this, "userId");
            if (userId.length() > 0) {
                String Name = FetchData.getData(LoginActivity.this, "Name");
                String IsDoctor = FetchData.getData(LoginActivity.this, "IsDoctor");
                String access_token = FetchData.getData(LoginActivity.this, "access_token");
                String IsFirstLog = FetchData.getData(LoginActivity.this, "IsFirstLog");

                Intent intent = new Intent(LoginActivity.this, TabsActivity.class);
                intent.putExtra("userId", userId);
                intent.putExtra("Name", Name);
                intent.putExtra("IsDoctor", IsDoctor);
                intent.putExtra("access_token", access_token);
                intent.putExtra("IsFirstLog", IsFirstLog);
                startActivity(intent);
            }
        }

        buttonLogin.setOnClickListener(view -> {

            if (editTextPassword.getText().toString().length() > 0 && editTextUserName.getText().toString().length() > 0) {

                boolean isValidEmaillId = isValidEmaillId(editTextUserName.getText().toString());


                FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            fcmToken = task.getResult();
                            Log.e(TAG, "Token: " + fcmToken);
                        }
                    } else {
                        Log.e(TAG, "getInstance Failed:${task.exception");
                    }

                    if (isValidEmaillId) {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", editTextUserName.getText().toString());
                        params.put("password", editTextPassword.getText().toString());
                        params.put("DeviceId", fcmToken);
                        params.put("Device_type", "Android");
                        JSONObject jsonObject = new JSONObject(params);
                        try {
                            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
                            Log.e(TAG, "Json Object: " + jsonObject.toString());
                            Log.e(TAG, "Encrypted Object: " + encrypt);
                            pDialog.show();
                            RetrofitApi.getService(Const.BASE_URL).login(encrypt).enqueue(new Callback<LoginResponse>() {
                                @Override
                                public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response) {
                                    Log.e(TAG, "onResponse: ");
                                    if (pDialog.isShowing()) {
                                        pDialog.dismiss();
                                    }

                                    if(response.isSuccessful() && response.body()!=null){

                                        try {

                                            int code = response.body().getMeta().getCode();
                                            if (code == 200) {
                                                Gson gson = new Gson();
                                                String decryptedString = AESEncryption.decrypt(response.body().getResponse(), Const.Encryption_Key, Const.Encryption_IV);
                                                JSONObject jsonResponse = new JSONObject(decryptedString);

                                                Log.e(TAG, "Json Response: "+ jsonResponse);
                                                String userId = jsonResponse.getString("userId");
                                                String Name = jsonResponse.getString("Name");
                                                String IsDoctor = jsonResponse.getString("IsDoctor");
                                                String access_token = jsonResponse.getString("access_token");
                                                String IsFirstLog = jsonResponse.getString("IsFirstLog");
                                                String Is2FACode = jsonResponse.getString("TwoFACode");


                                                SaveData.SaveData(LoginActivity.this, "userEmail", editTextUserName.getText().toString());
                                                Type type = new TypeToken<ArrayList<ConnectedClinicsModel>>() {
                                                }.getType();


                                                JSONArray ConnectedClinics = jsonResponse.getJSONArray("ConnectedClinics");

                                                ArrayList<ConnectedClinicsModel> connectedClinicsModelArrayList = gson.fromJson(ConnectedClinics.toString(), type);

                                                SaveData.SaveData(LoginActivity.this, "cliniclist", ConnectedClinics.toString());


                                                SharedPreferences sp = getApplicationContext().getSharedPreferences("appData", 0);
                                                SharedPreferences.Editor editor;
                                                editor = sp.edit();
                                                editor.putString("userId", userId);
                                                editor.putString("Name", Name);
                                                editor.putString("access_token", access_token);
                                                editor.putString("IsFirstLog", IsFirstLog);
                                                editor.apply();

                                                if (Is2FACode.equals("")) {
                                                    if (IsFirstLog.equals("1")) {

                                                        Intent intent = null;
                                                        if (connectedClinicsModelArrayList.isEmpty()) {
                                                            intent = new Intent(LoginActivity.this, MainActivity.class);
                                                        }
                                                        intent.putExtra("userId", userId);
                                                        intent.putExtra("Name", Name);
                                                        intent.putExtra("IsDoctor", IsDoctor);
                                                        intent.putExtra("access_token", access_token);
                                                        intent.putExtra("IsFirstLog", IsFirstLog);
                                                        startActivity(intent);
                                                        finish();

                                                    } else if (IsFirstLog.equals("0")) {

                                                        Intent intent = null;
                                                        if (connectedClinicsModelArrayList.isEmpty()) {
                                                            // popup to add or map clinic
//                            Dialogues.showOkDialogue(LoginActivity.this,
//                                    "Your request for the clinic request has not approved it. Please try again later.");

                                                            Intent intent1 = new Intent(LoginActivity.this, WelcomeActivity.class);

                                                            intent1.putExtra("name", Name);
                                                            intent1.putExtra("welcome", "Your clinic approval request status is Pending");
                                                            intent1.putExtra("team", "Your request for the clinic has not been approved yet. Please wait…");


                                                            startActivity(intent1);
                                                            LoginActivity.this.finish();
                                                            return;
                                                        } else {

                                                            boolean exists = false;
                                                            for (ConnectedClinicsModel connectedClinics : connectedClinicsModelArrayList) {
                                                                String IsCalendarExist = connectedClinics.getCalendarExist();
                                                                if (IsCalendarExist.equals("1")) {
                                                                    exists = true;
                                                                }

                                                            }
                                                            if (exists) {

                                                                intent = new Intent(LoginActivity.this, TabsActivity.class);
                                                                SaveData.SaveData(LoginActivity.this, "exists", "true");

                                                            } else {
                                                                intent = new Intent(LoginActivity.this, AddCalendarActivity.class);
                                                            }
                                                        }


                                                        intent.putExtra("userId", userId);
                                                        intent.putExtra("Name", Name);
                                                        intent.putExtra("IsDoctor", IsDoctor);
                                                        intent.putExtra("access_token", access_token);
                                                        intent.putExtra("IsFirstLog", IsFirstLog);
                                                        startActivity(intent);
                                                        finish();

                                                    }
                                                } else {
                                                    SaveData.SaveData(LoginActivity.this, "Code", Is2FACode);
                                                    SaveData.SaveData(LoginActivity.this, "Name", Name);
                                                    SaveData.SaveData(LoginActivity.this, "isFirstLog", IsFirstLog);
                                                    editor.putString(Constant.PUSH_TOKEN, fcmToken);
                                                    startActivity(new Intent(LoginActivity.this, VarificationLoginActivity.class));
                                                }

                                            }
                                            else if (code == 504) {

                                                //String error = jsonObject.getString("error");
                                                //Dialogues.showOkDialogue(LoginActivity.this, error);
                                                JSONObject jsonResponse = jsonObject.getJSONObject("response");

                                                String Name = jsonResponse.getString("Name");
                                                Intent intent1 = new Intent(LoginActivity.this, WelcomeActivity.class);

                                                intent1.putExtra("name", Name);
                                                intent1.putExtra("welcome", "Thank you for sending request to the Clinic");
                                                intent1.putExtra("team", "Your request for the clinic request has not approved yet. Please wait.");

                                                startActivity(intent1);
                                                LoginActivity.this.finish();
                                            }
                                            else if (code == 402) {
                                                Toast.makeText(LoginActivity.this, "Wrong username or password.", Toast.LENGTH_SHORT).show();

                                            }
                                            else if (code == 404) {
                                                Toast.makeText(LoginActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                @Override
                                public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
                                    if (pDialog.isShowing()) {
                                        pDialog.dismiss();
                                    }
                                    Log.e(TAG, "onFailure: "+ t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Please add correct Email address.", Toast.LENGTH_SHORT).show();
                    }

                });

            } else {
                Toast.makeText(LoginActivity.this, "Please fill username and password.", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void togglePassVisability() {
        if (isPasswordVisible) {
            String pass = editTextPassword.getText().toString();
            editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            editTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editTextPassword.setText(pass);
            editTextPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_cancel, 0);

            editTextPassword.setSelection(pass.length());
        } else {
            String pass = editTextPassword.getText().toString();
            editTextPassword.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
            editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            editTextPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye, 0);

            editTextPassword.setText(pass);
            editTextPassword.setSelection(pass.length());
        }
        isPasswordVisible= !isPasswordVisible;
    }

    private void setUpViews() {

        ClickableSpan termsOfServicesClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

            }
        };

        ClickableSpan privacyPolicyClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(getApplicationContext(), "Privacy Policy Clicked", Toast.LENGTH_SHORT).show();
            }
        };

        makeLinks(tvSignUp, new String[]{"Sign Up"}, new ClickableSpan[]{
                termsOfServicesClick, privacyPolicyClick
        });

    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    @Override
    public void success(String response) {

        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.parseInt(code) == 200) {
                    String decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV);
                    JSONObject jsonResponse = new JSONObject(decryptedString);
                    String userId = jsonResponse.getString("userId");
                    String Name = jsonResponse.getString("Name");
                    String IsDoctor = jsonResponse.getString("IsDoctor");
                    String access_token = jsonResponse.getString("access_token");
                    String IsFirstLog = jsonResponse.getString("IsFirstLog");
                    String Is2FACode = jsonResponse.getString("TwoFACode");
                    Log.e("TwoFa Code", "Code :" + Is2FACode);
                    Log.e("Login|Response", "response :" + jsonResponse);

                    SocketIO.INSTANCE.connectSocket(userId, Name, access_token, this);
                    SocketIO.INSTANCE.didEnterForeground(userId);

                    SaveData.SaveData(this, "userEmail", editTextUserName.getText().toString());
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<ConnectedClinicsModel>>() {
                    }.getType();


                    JSONArray ConnectedClinics = jsonResponse.getJSONArray("ConnectedClinics");

                    ArrayList<ConnectedClinicsModel> connectedClinicsModelArrayList = gson.fromJson(ConnectedClinics.toString(), type);

                    SaveData.SaveData(LoginActivity.this, "cliniclist", ConnectedClinics.toString());


                    SharedPreferences sp = getApplicationContext()
                            .getSharedPreferences("appData", 0);
                    SharedPreferences.Editor editor;
                    editor = sp.edit();
                    editor.putString("userId", userId);
                    editor.putString("Name", Name);
                    editor.putString("access_token", access_token);
                    editor.putString("IsFirstLog", IsFirstLog);

                    editor.apply();
                    if (Is2FACode.equals("")) {
                        if (IsFirstLog.equals("1")) {

                            Intent intent = null;
                            if (connectedClinicsModelArrayList.isEmpty()) {
                                intent = new Intent(LoginActivity.this, MainActivity.class);
                            }
                            intent.putExtra("userId", userId);
                            intent.putExtra("Name", Name);
                            intent.putExtra("IsDoctor", IsDoctor);
                            intent.putExtra("access_token", access_token);
                            intent.putExtra("IsFirstLog", IsFirstLog);
                            startActivity(intent);
                            this.finish();

                        } else if (IsFirstLog.equals("0")) {

                            Intent intent = null;
                            if (connectedClinicsModelArrayList.isEmpty()) {
                                // popup to add or map clinic
//                            Dialogues.showOkDialogue(LoginActivity.this,
//                                    "Your request for the clinic request has not approved it. Please try again later.");

                                Intent intent1 = new Intent(LoginActivity.this, WelcomeActivity.class);

                                intent1.putExtra("name", Name);
                                intent1.putExtra("welcome", "Your clinic approval request status is Pending");
                                intent1.putExtra("team", "Your request for the clinic has not been approved yet. Please wait…");


                                startActivity(intent1);
                                LoginActivity.this.finish();
                                return;
                            } else {

                                boolean exists = false;
                                for (ConnectedClinicsModel connectedClinics : connectedClinicsModelArrayList) {
                                    String IsCalendarExist = connectedClinics.getCalendarExist();
                                    if (IsCalendarExist.equals("1")) {
                                        exists = true;
                                        break;
                                    }
                                }
                                if (exists) {

                                    intent = new Intent(LoginActivity.this, TabsActivity.class);
                                    SaveData.SaveData(LoginActivity.this, "exists", "true");

                                } else {
                                    intent = new Intent(LoginActivity.this, AddCalendarActivity.class);
                                }
                            }


                            intent.putExtra("userId", userId);
                            intent.putExtra("Name", Name);
                            intent.putExtra("IsDoctor", IsDoctor);
                            intent.putExtra("access_token", access_token);
                            intent.putExtra("IsFirstLog", IsFirstLog);
                            startActivity(intent);
                            this.finish();

                        }
                    } else {
                        SaveData.SaveData(this, "Code", Is2FACode);
                        SaveData.SaveData(this, "Name", Name);
                        SaveData.SaveData(this, "isFirstLog", IsFirstLog);
                        editor.putString(Constant.PUSH_TOKEN, fcmToken);
                        startActivity(new Intent(LoginActivity.this, VarificationLoginActivity.class));
                    }

                }
                else if (Integer.parseInt(code) == 504) {

                    //String error = jsonObject.getString("error");
                    //Dialogues.showOkDialogue(LoginActivity.this, error);
                    JSONObject jsonResponse = jsonObject.getJSONObject("response");

                    String Name = jsonResponse.getString("Name");
                    Intent intent1 = new Intent(LoginActivity.this, WelcomeActivity.class);

                    intent1.putExtra("name", Name);
                    intent1.putExtra("welcome", "Thank you for sending request to the Clinic");
                    intent1.putExtra("team", "Your request for the clinic request has not approved yet. Please wait.");

                    startActivity(intent1);
                    LoginActivity.this.finish();
                } else if (Integer.parseInt(code) == 402) {
                    Toast.makeText(this, "Wrong username or password.", Toast.LENGTH_SHORT).show();
                } else if (Integer.parseInt(code) == 404) {

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }

    }

    @Override
    public void error(String response) {
        if (response != null) {
            Logs.showLog("Login", response);
        }
    }

}
