package com.softech.ccconsultant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements IWebListener  {

    private static final String TAG = "SignUpActivity";

    @BindView(R.id.buttonNext)
    Button buttonNext;

    @BindView(R.id.editTextBusinessName)
    EditText editTextFirstName;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @BindView(R.id.editConfirmPassword)
    EditText editConfirmPassword;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;

    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;

    @BindView(R.id.tvTerms)
    TextView tvTerms;

    @BindView(R.id.tvCondition)
    TextView tvCondition;

    @BindView(R.id.tvPolicy)
    TextView tvPolicy;
    boolean isValidEmail,isValidPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("SIGN UP");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUpActivity.this.finish();
            }
        });

        ButterKnife.bind(this);
        //        editTextFirstName.setText("asdas");
//        editTextLastName.setText("asdsad");
//        editTextPassword.setText("1133");
//        editTextConfirmPassword.setText("1133");
//        editTextEmail.setText("saim.shafqat93@gmail.com");
        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php");
                startActivity(intent);
            }
        });

        tvCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php");
                startActivity(intent);
            }
        });

        tvPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, WebviewwActivity.class);
                intent.putExtra("url", "https://www.healthcarepakistan.pk/privacy-clinic.php");
                startActivity(intent);

            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber=etPhoneNumber.getText().toString();

                if (editTextFirstName.getText().toString().length() > 0 &&
                        editTextPassword.getText().toString().length() > 0 &&
                        editTextEmail.getText().toString().length() > 0) {
                        String email = editTextEmail.getText().toString();
                        boolean isValidEmailId = isValidEmaillId(email);
                        if (isValidEmailId) {
                            isValidEmail=true;

                            if (editTextPassword.getText().toString().length() > 6){
                                isValidPassword=true;
                                if (phoneNumber.length()==16){

                                    if (editTextPassword.getText().toString().equals(editConfirmPassword.getText().toString())){
                                        TelephonyManager tm = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                                        String countryCodeValue = tm.getNetworkCountryIso();

                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("action", "Create New User");
                                        params.put("ImageFile", "");
                                        params.put("vLastName", "");
                                        params.put("dDOB","");
                                        params.put("vSpeciality", "");
                                        params.put("vAddress","");
                                        params.put("vCity", "");
                                        params.put("vCountry", countryCodeValue);
                                        params.put("vEmailAddress", email);
                                        params.put("iPhoneNumber", phoneNumber);
                                        params.put("Password", editTextPassword.getText().toString());
                                        params.put("Qualification","");
                                        params.put("businessName", editTextFirstName.getText().toString());

                                        Log.e(TAG, "Encrypted Object Params: "+params );

                                        JSONObject jsonObject = new JSONObject(params);
                                        try {
                                            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
                                            Log.d("dsasda", encrypt);
                                            Map<String, String> finalParams = new HashMap<String, String>();
                                            finalParams.put("encryptObject", encrypt);
                                            Log.e(TAG, "Signup Encrypted Object: "+ encrypt);
                                            ApiConnection apiConnection = new ApiConnection(SignUpActivity.this, SignUpActivity.this, finalParams, Const.SIGNUP);
                                            apiConnection.makeStringReq();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        Toast.makeText(SignUpActivity.this, "Password do not match with each other", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else {
                                    Toast.makeText(SignUpActivity.this, "Please enter number in correct format", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else {
                                Toast.makeText(SignUpActivity.this, "Please enter at least 7 characters password", Toast.LENGTH_SHORT).show();
                            }
                        }
//                            Intent intent = new Intent(SignUpActivity.this, ProfileActivity.class);
//                            intent.putExtra("FirstName", editTextFirstName.getText().toString());
//                            intent.putExtra("LastName", editTextLastName.getText().toString());
//                            intent.putExtra("Password", editTextPassword.getText().toString());
//                            intent.putExtra("Email", editTextEmail.getText().toString());
//                            startActivity(intent);
                         else {
                            Toast.makeText(SignUpActivity.this, "Please add correct Email address.", Toast.LENGTH_SHORT).show();
                         }







                } else {
                    Toast.makeText(SignUpActivity.this, "All fields are mandatory. Please enter information in all fields", Toast.LENGTH_SHORT).show();
                }

            }
        });



        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(i ==11)
                {
                    String input = etPhoneNumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    etPhoneNumber.setText(number);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {

                Log.e(TAG, "Signup Response: "+response );

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.parseInt(code) == 200) {

                    Intent intent = new Intent(SignUpActivity.this, WelcomeActivity.class);
                    intent.putExtra("email", editTextEmail.getText().toString());
                    intent.putExtra("password", editTextPassword.getText().toString());
                    intent.putExtra("name", editTextFirstName.getText().toString());
                    intent.putExtra("welcome", "Thank you for registering with Clinic Connect");
                    intent.putExtra("team", "Our Technical team will contact you within 24 hours to confirm your details.");

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(intent);

                    SignUpActivity.this.finish();


                } else if (Integer.parseInt(code) == 404) {
                    String message = meta.getString("message");
                    Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String message = meta.getString("message");
                Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    public void error(String response) {

    }
}
