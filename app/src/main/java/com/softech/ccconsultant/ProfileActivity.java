package com.softech.ccconsultant;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.Lab;
import com.softech.ccconsultant.data.models.LabsList;
import com.softech.ccconsultant.data.models.Pharmacy;
import com.softech.ccconsultant.data.models.models.ConnectedClinicsModel;
import com.softech.ccconsultant.data.models.models.SpecialitiesModel;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.AESEncryption;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements IWebListener {

    private final String TAG = "ProfileActivity";

    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editZipCode)
    EditText editZipCode;
    @BindView(R.id.fieldFirstName)
    EditText editTextFirstName;
    @BindView(R.id.fieldLastName)
    EditText editTextLastName;
    @BindView(R.id.textViewDOB)
    TextView textViewDOB;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.buttonProfileSignup)
    Button buttonSignUp;
    @BindView(R.id.spinnerSpeciality)
    Spinner spinnerSpeciality;
    @BindView(R.id.spinnerPharmacy)
    Spinner spinnerPharmacy;
    @BindView(R.id.spinnerRadiology)
    Spinner spinnerRadiology;
    @BindView(R.id.spinnerLabs)
    Spinner spinnerLabs;
    LabsList labsListObject;
    String FirstName, LastName, Password, Email;
    int pharmID = 0;
    int labID = 0;
    int radID = 0;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar1 = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Complete Your Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.this.finish();
            }
        });

        ButterKnife.bind(this);
        getSpecialities();
        hitLabListApi();

        textViewDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog dialog =  new DatePickerDialog(ProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        editTextEmail.setText(FetchData.getData(this, "userEmail"));
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextFirstName.getText().toString().length() > 0 &&
                        editTextLastName.getText().toString().length() > 0 &&
                        editTextPhoneNumber.getText().toString().length() > 14 &&
                        spinnerSpeciality.getSelectedItem().toString().length() > 0 &&
                        editTextAddress.getText().toString().length() > 0
                        && editZipCode.getText().toString().length()>0 &&
                        textViewDOB.getText().toString().length()>0
                ) {
                    updateProfile();

                } else {
                    Toast.makeText(ProfileActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
        spinnerLabs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Lab lab = labsListObject.getPathLabList().get(i);
                    labID = lab.getLabID();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerPharmacy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Pharmacy lab = labsListObject.getPharmacyList().get(i);
                    pharmID = lab.getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerRadiology.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (labsListObject != null) {
                    Lab lab = labsListObject.getRadLabList().get(i);
                    radID = lab.getLabID();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        editTextPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(i ==11)
                {
                    String input = editTextPhoneNumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    editTextPhoneNumber.setText(number);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textViewDOB.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateProfile() {

        Map<String, String> params = new HashMap<String, String>();
        String consultantID = FetchData.getData(this, "userId");
        String access_token = FetchData.getData(this, "access_token");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("speciality", spinnerSpeciality.getSelectedItem().toString());
        params.put("phoneNumber", editTextPhoneNumber.getText().toString());
        params.put("address", editTextAddress.getText().toString());
//        params.put("city", editTextCity.getText().toString());
//        params.put("license", licensNumber.getText().toString());
        params.put("firstName", editTextFirstName.getText().toString());
        params.put("lastName", editTextLastName.getText().toString());
        params.put("country", "PK");
        params.put("PrefPharmacyID", String.valueOf(pharmID));
        params.put("PrefPathId", String.valueOf(labID));
        params.put("PrefRadID", String.valueOf(radID));
        params.put("zipcode", editZipCode.getText().toString());
        params.put("dob", "");
        params.put("qualification",textViewDOB.getText().toString());
        params.put("city", "");

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                if (response != null) {

                    try {

                        Log.e(TAG, "Setup Profile Response: "+ response);

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.parseInt(code) == 200) {
                            String decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV);
                            JSONObject jsonResponse = new JSONObject(decryptedString);
                            JSONArray ConnectedClinics = jsonResponse.getJSONArray("ConnectedClinics");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<ConnectedClinicsModel>>() {
                            }.getType();
                            ArrayList<ConnectedClinicsModel> connectedClinicsModelArrayList = gson.fromJson(ConnectedClinics.toString(), type);
                            SaveData.SaveData(ProfileActivity.this, "cliniclist", ConnectedClinics.toString());
                            boolean exists = false;
                            for (ConnectedClinicsModel connectedClinics : connectedClinicsModelArrayList) {
                                String IsCalendarExist = connectedClinics.getCalendarExist();
                                if (IsCalendarExist.equals("1")) {
                                    exists = true;
                                }

                            }
                            Intent intent = null;
                            if (exists) {
                                intent = new Intent(ProfileActivity.this, TabsActivity.class);
                                SaveData.SaveData(ProfileActivity.this, "exists", "true");
                                startActivity(intent);

                            } else {
                                intent = new Intent(ProfileActivity.this, AddCalendarActivity.class);
                                startActivity(intent);
                            }
                        }
                        else if (Integer.parseInt(code) == 502) {
                            Toast.makeText(ProfileActivity.this, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();

                        }
                       else if (Integer.parseInt(code) == 504) {
                            Toast.makeText(ProfileActivity.this, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();

                        }

                       else {
                            Intent intent = new Intent(ProfileActivity.this, TabsActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {
                Log.d("asdasd",response);

            }
        },
                this, params, Const.SETUPPROFILE);
        apiConnection.makeStringReq();
    }

    public void hitLabListApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("ConID", FetchData.getData(this, "userId"));
        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {
                            JSONObject jsonResponse = jsonObject.getJSONObject("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<LabsList>() {
                            }.getType();
                            labsListObject = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<Pharmacy> pharAdapter =
                                    new ArrayAdapter<Pharmacy>(ProfileActivity.this, R.layout.spinner, labsListObject.getPharmacyList());
                            pharAdapter.setDropDownViewResource(R.layout.spinner);
                            spinnerPharmacy.setAdapter(pharAdapter);
                            spinnerPharmacy.setPrompt("Select Pharmacy");
                            ArrayAdapter<Lab> labAdapter =
                                    new ArrayAdapter<Lab>(ProfileActivity.this, R.layout.spinner, labsListObject.getPathLabList());
                            labAdapter.setDropDownViewResource(R.layout.spinner);
                            spinnerLabs.setAdapter(labAdapter);
                            spinnerLabs.setPrompt("Select Lab");
                            ArrayAdapter<Lab> radAdapter =
                                    new ArrayAdapter<Lab>(ProfileActivity.this, R.layout.spinner, labsListObject.getRadLabList());
                            labAdapter.setDropDownViewResource(R.layout.spinner);
                            spinnerRadiology.setAdapter(radAdapter);
                            spinnerRadiology.setPrompt("Select Radiology");

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }

            }

            @Override
            public void error(String response) {
                Logs.showLog("Login", response);
            }
        }, this, params, Const.GETLABANDPHARMACYLIST);
        apiConnection.makeStringReq();
    }

    private void getSpecialities() {
        Map<String, String> params = new HashMap<String, String>();

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {
                            JSONArray jsonResponse = jsonObject.getJSONArray("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<SpecialitiesModel>>() {
                            }.getType();
                            List<SpecialitiesModel> specialitiesModelList = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<SpecialitiesModel> adapter =
                                    new ArrayAdapter<SpecialitiesModel>(ProfileActivity.this, R.layout.spinner, specialitiesModelList);
                            adapter.setDropDownViewResource(R.layout.spinner);
                            spinnerSpeciality.setAdapter(adapter);
                            spinnerSpeciality.setPrompt("Select Site");
                            Log.d("selected Speciality", spinnerSpeciality.getSelectedItem().toString());

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {
                Logs.showLog("Login", response);
            }
        }, ProfileActivity.this, params, Const.GETSPECIALITIES);
        apiConnection.makeStringReq();

    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
//                    String responseString = jsonObject.getString("response");
//                    Intent intent = new Intent(ProfileActivity.this, WelcomeActivity.class);
//                    intent.putExtra("email", Email);
//                    intent.putExtra("password", Password);
//                    intent.putExtra("name", FirstName + " " + LastName);
//                    intent.putExtra("welcome", "Thank you for registering with Clinic Connect");
//                    intent.putExtra("team", "Our Technical team will contact you within 24 hours to confirm your details.");
//
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//                    startActivity(intent);
//
//                    ProfileActivity.this.finish();


                } else if (Integer.valueOf(code) == 404) {


                    String message = meta.getString("message");

                    Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }
}
