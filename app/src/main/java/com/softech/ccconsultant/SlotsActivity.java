package com.softech.ccconsultant;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccconsultant.adapters.SlotsAdapter;
import com.softech.ccconsultant.apiconnections.ApiConnection;
import com.softech.ccconsultant.data.models.models.AppointmentsModel;
import com.softech.ccconsultant.data.models.models.CliniclistModel;
import com.softech.ccconsultant.data.models.models.SlotsData.SlotsData;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.FetchData;
import com.softech.ccconsultant.utils.Const;
import com.softech.ccconsultant.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.vo.DateData;

public class SlotsActivity extends AppCompatActivity implements IWebListener {

    private final String TAG = "SlotsActivity";

    @BindView(R.id.calenderview)
    MCalendarView calandarView;
    @BindView(R.id.simpleCalandar)
    CalendarView simpleCalandar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.spinnerClinicList)
    Spinner spinnerClinicList;
    @BindView(R.id.spinClinicList)
    Spinner spinClinicList;
    @BindView(R.id.leftarrow)
    ImageView leftArrow;
    @BindView(R.id.rightarrow)
    ImageView rightarrow;
    @BindView(R.id.dateLabel)
    TextView datelabel;
    @BindView(R.id.slidingLayout)
    LinearLayout slidinglayout;
    DateData datedata;
    String currentDate, selectedDate, clinicID;
    int date, month, year;
    SlotsData slotsData;
    Date startDate, ourEndDate, endDate, currentDated;
    SlotsAdapter slotsAdapter;
    int spinnerPosition = 0;
    ArrayList<String> clinics = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    AppointmentsModel appointmentsModel;
    String consultantID,accessToken,hospitalId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slots);
        Toolbar toolbar = (Toolbar
                ) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add New");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Gson gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
           String  myString = getIntent().getStringExtra("model");
           appointmentsModel = gson.fromJson(myString, AppointmentsModel.class);

            consultantID=bundle.getString("consultantID");
            accessToken=bundle.getString("accessToken");
            hospitalId=bundle.getString("clinicID");
        }



        final Animation slidedown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        final Animation slideup = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SlotsActivity.this.finish();
            }
        });
        ButterKnife.bind(this, this);

        datelabel.setText(getCurrentDate(Calendar.getInstance().getTime()));
        datelabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (simpleCalandar.getVisibility() == View.GONE) {
                    simpleCalandar.setVisibility(View.VISIBLE);
                    simpleCalandar.startAnimation(slidedown);

                } else {
                    simpleCalandar.setVisibility(View.GONE);
                    slidinglayout.startAnimation(slideup);


                }

            }
        });

        //recyclerView.addItemDecoration(new DividerItemDecoration(SlotsActivity.this, DividerItemDecoration.VERTICAL));
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = FetchData.getPreviousOrNextDate(currentDate, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
                String newDate = sdf.format(FetchData.dateFromString(currentDate));
                datelabel.setText(newDate);
                try {
                    simpleCalandar.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(currentDate).getTime(), true, true);
                    callingDaysSlotService();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        rightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = FetchData.getPreviousOrNextDate(currentDate, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
                String newDate = sdf.format(FetchData.dateFromString(currentDate));
                datelabel.setText(newDate);
                try {
                    simpleCalandar.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(currentDate).getTime(), true, true);
                    callingDaysSlotService();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        simpleCalandar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                currentDate = String.format("%d/%d/%d", dayOfMonth, month + 1, year);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
                currentDate = sdf.format(FetchData.dateFromString(currentDate));
                String newDate = sdf1.format(FetchData.dateFromString(currentDate));
                datelabel.setText(newDate);
                callingDaysSlotService();
            }
        });


//        adapter = new ArrayAdapter<String>(
//                this, android.R.layout.simple_spinner_item, clinics);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerClinicList.setAdapter(adapter);


    }

    public String getCurrentDate(Date date) {
        System.out.println("Current time => " + date);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("MMM dd, yyyy");
        String newDate = df1.format(date);
        currentDate = df.format(date);
        currentDated = FetchData.dateFromString(currentDate);
        return newDate;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
        }.getType();
        String ClinicList = FetchData.getData(this, "cliniclist");
        final ArrayList<CliniclistModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);

        ArrayAdapter<CliniclistModel> adapter =
                new ArrayAdapter<CliniclistModel>(this, R.layout.spinner, connectedClinicsModelArrayList);
        adapter.setDropDownViewResource(R.layout.spinner);
        spinClinicList.setAdapter(adapter);
        spinClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                CliniclistModel cliniclistModel = connectedClinicsModelArrayList.get(position);
                clinicID = cliniclistModel.getOID();
                callingDaysSlotService();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void callingDaysSlotService() {

        String consultantID = FetchData.getData(this, "userId");
        String accesstoken = FetchData.getData(this, "access_token");
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Consultant Calendars");
        params.put("consultantId", consultantID);
        params.put("date", currentDate);
        params.put("clinicid", clinicID);
        params.put("access_token", accesstoken);
        ApiConnection apiConnection = new ApiConnection(this,
                this, params, Const.ConsultantCalendars);
        apiConnection.makeStringReq();
    }


    @Override
    public void success(String response) {
        if (response != null) {

            try {

                Log.e(TAG, "GetCurrentDateCalendarSlots: "+response );

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.parseInt(code) == 200) {
                    spinnerClinicList.setEnabled(true);
                    String message = meta.getString("message");
                    slotsData = new Gson().fromJson(response, SlotsData.class);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                    slotsAdapter = new SlotsAdapter(this, slotsData.getResponse(),
                            clinicID, currentDate,appointmentsModel,consultantID,accessToken,hospitalId);
                    recyclerView.setAdapter(slotsAdapter);
                    ;
//                    responseArrayList.add(calendarByDate.getResponse().get(0));

//                    spinnerClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                            SlotsData(position);
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> parent) {
//
//                        }
//                    });

                    // populdateSpinner();
                } else if (Integer.valueOf(code) == 504) {
                    String message = meta.getString("message");
                    slotsAdapter.notifyDataSetChanged();

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                } else if (Integer.valueOf(code) == 499) {
                    String message = meta.getString("message");
                    if (slotsData != null) {
                        slotsData.getResponse().clear();
                        slotsAdapter.notifyDataSetChanged();
                    }
                    spinnerClinicList.setEnabled(false);
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }
}

