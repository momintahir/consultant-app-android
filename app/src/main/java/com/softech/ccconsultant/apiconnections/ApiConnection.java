package com.softech.ccconsultant.apiconnections;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.softech.ccconsultant.LoginActivity;
import com.softech.ccconsultant.app.MyApplication;
import com.softech.ccconsultant.interfaces.IWebListener;
import com.softech.ccconsultant.sharedpreferences.SaveData;
import com.softech.ccconsultant.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class ApiConnection {

    private static final String TAG = "ApiConnection";
    IWebListener iWeb;
    private ProgressDialog pDialog;
    private Activity activity;
    private String tag_string_req = "string_req";
    Map<String, String> params = new HashMap<String, String>();
    JSONObject params1;
    String method;
    File imageFile = null;
    String imageKey, mimetype;


    public ApiConnection(IWebListener iWeb, Activity activity, Map<String, String> params, String method) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params = params;
        this.method = method;
    }

    public ApiConnection(IWebListener iWeb, Activity activity, JSONObject params1, String method) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params1 = params1;
        this.method = method;
    }

    public ApiConnection(IWebListener iWeb, Activity activity, Map<String, String> params, String method, File imageFile, String imagekey, String mimetype) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params = params;
        this.imageFile = imageFile;
        this.imageKey = imagekey;
        this.method = method;
        this.mimetype = mimetype;
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void makeStringReq() {

        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showProgressDialog();
        if (imageFile != null) {
            makeMultipartRequest();
            return;
        }

        Log.e(TAG, "Request: " + Const.BASE_URL + method);
        Log.e(TAG, "Request Json/Params: "+ params);

        StringRequest strReq = new StringRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                Log.e(TAG, response);
                //accessTokenExpired(response.toString());
//                Toast.makeText(activity, response.toString(), Toast.LENGTH_SHORT).show();
                iWeb.success(response.toString());
                //    msgResponse.setText(response.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        Log.e(TAG, "Network Response: "+error.getMessage());
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                2000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    public void makeStringReqForStatus() {
        Log.e(TAG, "Request: " + Const.BASE_URL + method);
        Log.e(TAG, "Request Json/Params: "+ params);

        if (imageFile != null) {
            makeMultipartRequest();
            return;
        }
        StringRequest strReq = new StringRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //accessTokenExpired(response.toString());
//                Toast.makeText(activity, response.toString(), Toast.LENGTH_SHORT).show();
                iWeb.success(response.toString());
                //    msgResponse.setText(response.toString());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        Log.v("", "");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                2000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public void makeMultipartRequest() {
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                hideProgressDialog();
                String resultResponse = new String(response.data);
                iWeb.success(resultResponse.toString());
                Log.i("MultipartResponse", resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                hideProgressDialog();

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError || error instanceof AuthFailureError || error instanceof ClientError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                        iWeb.error(res);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                if (imageFile != null) {
                    params.put(imageKey,
                            String.valueOf(new DataPart("imagephoto", readBytesFromFile(imageFile.getPath()), mimetype)));
                }
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (imageFile != null) {
                    params.put(imageKey, new DataPart("imagephoto", readBytesFromFile(imageFile.getPath()), mimetype));
                }
                // params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mCoverImage.getDrawable()), "image/jpeg"));

                return params;
            }

        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyApplication.getInstance().addToRequestQueue(multipartRequest, tag_string_req);

    }

    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }

    public void makeStringReq1() {

        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showProgressDialog();

        Log.e(TAG, "Request: " + Const.BASE_URL + method);
        Log.e(TAG, "Request Json/Params: "+ params1);

        StringRequest strReq = new StringRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                iWeb.success(response.toString());
                hideProgressDialog();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                return params1.toString().getBytes();
            }

            public String getBodyContentType() {
                return "application/json";
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void accessTokenExpired(String response) {

        try {
            JSONObject res = new JSONObject(response);
            JSONObject meta = res.getJSONObject("meta");
            String code = meta.getString("code");
            if (Integer.valueOf(code) == 404) {
                SaveData.SaveData(this.activity, "exists", "false");
                SharedPreferences preferences = this.activity.getSharedPreferences("appData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();

                Intent intent = new Intent(activity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
