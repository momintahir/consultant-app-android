package com.softech.ccconsultant.apiconnections;


import com.softech.ccconsultant.data.models.models.GeneralResponse;
import com.softech.ccconsultant.data.models.models.Login.LoginResponse;
import com.softech.ccconsultant.utils.Const;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitService {

    @FormUrlEncoded
    @POST(Const.LOGIN)
    Call<LoginResponse> login(
            @Field("encryptObject") String encryptObject
    );


    @Multipart
    @POST(Const.UPLOAD_PRESCRIPTION)
    Call<GeneralResponse> uploadPrescription(
            @Part("encryptObj") RequestBody requestEncryptedString,
            @Part MultipartBody.Part imageFile
    );

    @Multipart
    @POST(Const.UPLOAD_DOCUMENT)
    Call<GeneralResponse> uploadDocument(
            @Part("encryptObj") RequestBody requestEncryptedString,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST(Const.UPDATEPROFILEIMAGE)
    Call<GeneralResponse> updateProfileImage(
            @Part("action") RequestBody action,
            @Part("patientId") RequestBody patientId,
            @Part MultipartBody.Part imageFile
    );

}


