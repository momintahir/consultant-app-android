package com.softech.ccconsultant.interfaces;

import com.softech.ccconsultant.data.models.models.CalendarByDate.SlotsDatum;

/**
 * Created by saimshafqat on 09/08/2018.
 */

public interface SendCalendarData
{
    void sendCalandarData(  SlotsDatum slotsDatum);
}
